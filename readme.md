### Как поднять проект

`composer install`

`npm i`

`cp .env.example .env`

затем прописать настройки в .env

`php artisan key:generate`

`php artisan migrate`

`php artisan db:seed`

`php artisan jwt:secret`
### База vidal
Установить mdbtools:

`sudo apt-get install mdbtools`

Запустить 

`php artisan vidal:install path-to-mdb`
### Sphinx Search

Установить Sphinx Search:

`sudo apt-get install sphinxsearch`

Скопировать и изменить настройки Sphinx Search:

`sudo cp sphinx.conf.example /etc/sphinx/sphinx.conf`

Создать индексы

`indexer --all`

Запустить daemon searchd:

`sudo searchd`

Построить триграммы:

`indexer --config=/etc/sphinx/sphinx.conf drugs --buildstops dict.txt 100000 --buildfreqs`

Запустить импорт триграмм в базу данных:

`php artisan sphinx:trigrams`

Запустить переиндексацию поиска:

`indexer --config=/etc/sphinx/sphinx.conf --all --rotate`

### Cron

* Laravel Shedule
    * `indexer --config=/etc/sphinx/sphinx.conf drugs --buildstops dict.txt 100000 --buildfreqs`
    * `php artisan sphinx:trigrams`
    * `indexer --config=/etc/sphinx/sphinx.conf --all --rotate`
    * `php artisan sms-codes:delete`
    * генерация промокодов `GeneratePromocodeJob`

### Список иконок

[resources/assets/fonts/tvoya-apteka/icons-reference.html](resources/assets/fonts/tvoya-apteka/icons-reference.html)