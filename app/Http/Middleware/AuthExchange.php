<?php

namespace App\Http\Middleware;

use Closure;

class AuthExchange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Request::getUser() != config('exchange.user') || \Request::getPassword() != config('exchange.password')) {
            return \Response::make('Invalid credentials.', 401, ['WWW-Authenticate' => 'Basic']);
        }
        return $next($request);
    }
}
