<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && session()->has('cart')) {
            $itemsInCart = Auth::user()->cartDrugs->keyBy('id');
            $refreshedItemsInCart = collect(session()->get('cart', []))->mapWithKeys(function ($item) use ($itemsInCart) {
                return [
                    $item['drug_id'] => [
                        'count' => $item['count'] + ($itemsInCart[$item['drug_id']]->pivot->count ?? 0),
                        'drugstore_id' => $item['drugstore_id']
                    ]
                ];
            })->toArray();
            Auth::user()->cartDrugs()->syncWithoutDetaching($refreshedItemsInCart);
            session()->forget('cart');
        }
        return $next($request);
    }
}
