<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class ApiRefresh extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $this->auth->parseToken()->authenticate();
        } catch (TokenExpiredException $e) {
            return $next($request);
        } catch (JWTException $e) {
            $errors = ['token' => [$e->getMessage()]];
            return response()->json(['errors' => $errors], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json(['message' => 'Token is valid yet.'], Response::HTTP_ACCEPTED);
    }
}
