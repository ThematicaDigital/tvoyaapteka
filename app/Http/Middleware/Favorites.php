<?php

namespace App\Http\Middleware;

use App\Models\Drug;
use Closure;
use Cookie;
use Auth;

class Favorites
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $favorites = unserialize(Cookie::get(Drug::FAVORITES_COOKIE));
            if ($favorites) {
                Auth::user()
                    ->favouriteDrugs()
                    ->syncWithoutDetaching($favorites);
            }
            $favoriteDrugsId = Auth::user()
                ->favouriteDrugs->pluck('id')->toArray();
            Cookie::queue(
                Drug::FAVORITES_COOKIE,
                serialize(array_combine($favoriteDrugsId, $favoriteDrugsId)),
                Drug::FAVORITES_EXPIRES
            );
        }

        return $next($request);
    }
}
