<?php

namespace App\Http\Middleware;

use App\Models\SmsCode;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class VerifySmsCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            SmsCode::where([
                'phone' => $request->phone,
                'code' => $request->sms_code
            ])->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $errors = ['errors' => ['sms_code' => ['Неверный код']]];
            return response()->json($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $next($request);
    }
}
