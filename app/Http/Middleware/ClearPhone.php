<?php

namespace App\Http\Middleware;

use Closure;

class ClearPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('phone')) {
            $request->merge(['phone' => clearPhone($request->phone)]);
        }
        return $next($request);
    }
}
