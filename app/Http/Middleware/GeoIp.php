<?php

namespace App\Http\Middleware;

use App\Models\Drugstore;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use View;
use App\Models\City;

class GeoIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $appCity = env('DEFAULT_CITY', 'moskva');
        $host = request()->server()['HTTP_HOST'];
        $scheme = request()->server()['REQUEST_SCHEME'] ?? 'http';
        $domain = config('session.domain', '.semeynayaapteka.ru');

        if (app()->environment() !== 'production') {
            if (!session()->exists('city_id')) {
                $geo = \GeoIP::getLocation('89.109.153.142');
                $city = City::where('slug', strtolower($geo->city))->first();
                if ($city) {
                    session()->put('city_id', $city->id);
                }
            }
            $city = City::find(session()->get('city_id'));
            $firstDrugstore = Drugstore::where('city_id', $city->id)->rememberCache(60*24)->first();
            View::share('city', $city);
            View::share('firstDrugstore', $firstDrugstore);
            $request->city_id = $city->id;
            $request->attributes->add(['city' => $city]);
            return $next($request);
        }

        if (!session()->has('user_city') && !session()->has('city_id')) {
            if (ends_with($host, $domain)) {
                $subdomain = str_before($host, $domain);
                try {
                    $city = City::where('slug', $subdomain)->firstOrFail();
                    $firstDrugstore = Drugstore::where('city_id', $city->id)->rememberCache(60*24)->first();
                    View::share('city', $city);
                    View::share('firstDrugstore', $firstDrugstore);
                    $request->city_id = $city->id;
                    $request->attributes->add(['city' => $city]);
                    session()->put('city_id', $city->id);
                    return $next($request);
                } catch (ModelNotFoundException $e) {
                    \Log::info('User input subdomain not found');
                }
            }

            $geo = \GeoIP::getLocation(request()->server()['REMOTE_ADDR']);
            try {
                $city = City::where('slug', strtolower($geo->city))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $city = City::where('slug', $appCity)->first();
            } finally {
                session()->put('city_id', $city->id);
            }

        }

        if (session()->has('user_city')) {
            $userCity = session()->get('user_city');
            $city = City::where('slug', $userCity)->first();
            session()->put('city_id', $city->id);
        } elseif (session()->has('city_id')) {
            $city = City::where('id', session()->get('city_id'))->first();
        }
        $firstDrugstore = Drugstore::where('city_id', $city->id)->rememberCache(60*24)->first();
        View::share('city', $city);
        View::share('firstDrugstore', $firstDrugstore);
        $request->city_id = $city->id;
        $request->attributes->add(['city' => $city]);

        if (request()->server()['HTTP_HOST'] !== "{$city->slug}{$domain}") {
            return redirect("{$scheme}://{$city->slug}{$domain}/{$request->path()}");
        }

        return $next($request);
    }
}
