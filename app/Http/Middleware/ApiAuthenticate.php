<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class ApiAuthenticate extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$this->auth->parseToken()->authenticate()) {
                return response()->json([], Response::HTTP_UNAUTHORIZED);
            }
        } catch (TokenExpiredException $e) {
            $errors = ['token' => [$e->getMessage()]];
            return response()->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        } catch (JWTException $e) {
            $errors = ['token' => [$e->getMessage()]];
            return response()->json(['errors' => $errors], Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
