<?php

namespace App\Http\Resources\Exchange;


use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => 's-' . $this->id,
            'drugstore' => new DrugstoreResource($this->whenLoaded('drugstore')),
            'status' => $this->status,
            'promocode' => new PromocodeResource($this->whenLoaded('promocode')),
            'created_at' => $this->created_at,
            'user' => new UserResource($this->whenLoaded('user')),
            'total_price' => $this->totalPrice,
            'total_price_with_promocode' => $this->totalPriceWithPromocode,
            'drugs' => DrugResource::collection($this->whenLoaded('drugs')),

        ];
    }

}
