<?php

namespace App\Http\Resources\Exchange;


use Illuminate\Http\Resources\Json\JsonResource;

class DrugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'title' => $this->title,
            'type' => $this->type,
            'price' => $this->pivot->price,
            'discount' => $this->pivot->discount,
            'discount_is_percent' => $this->pivot->discount_is_percent,
            'count' => $this->pivot->count
        ];
    }

}
