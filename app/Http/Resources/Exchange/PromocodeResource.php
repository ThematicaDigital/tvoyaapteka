<?php

namespace App\Http\Resources\Exchange;


use Illuminate\Http\Resources\Json\JsonResource;

class PromocodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->title,
            'type' => $this->type,
            'amount' => $this->amount,
            'is_percent' => $this->is_percent,
        ];

    }

}
