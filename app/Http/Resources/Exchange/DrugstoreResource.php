<?php

namespace App\Http\Resources\Exchange;


use Illuminate\Http\Resources\Json\JsonResource;

class DrugstoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'city_id' => $this->city_id,
            'title' => $this->title,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'worktime' => $this->worktime,
            'is_24h' => $this->is_24h
        ];
    }

}
