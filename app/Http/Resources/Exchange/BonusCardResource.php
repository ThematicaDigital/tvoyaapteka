<?php

namespace App\Http\Resources\Exchange;


use Illuminate\Http\Resources\Json\JsonResource;

class BonusCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'phone' => $this->user->phone ?? null,
            'number' => $this->number,
            'is_active' => $this->is_active,
            'activation_date' => $this->created_at,
        ];
    }

}
