<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        dd($this->whenLoaded('drugs'));
        return [
            'id' => $this->id,
            'drugstore' => new DrugstoreResource($this->whenLoaded('drugstore')),
            'status' => $this->status,
            'promocode' => new PromocodeResource($this->whenLoaded('promocode')),
            'created_at' => $this->created_at,
            'pay_datetime' => $this->pay_datetime,
            'total_price' => $this->totalPrice,
            'total_price_with_promocode' => $this->totalPriceWithPromocode,
            'drugs' => DrugOrderResource::collection($this->whenLoaded('drugs')),
        ];
    }
}
