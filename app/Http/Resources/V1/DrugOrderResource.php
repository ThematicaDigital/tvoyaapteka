<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class DrugOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'images' => getAbsolutePath($this->images),
            'image' => getAbsolutePath($this->image),
            'type' => $this->type,
            'default_price' => $this->default_price,
            'prices' => [
                'price' => $this->pivot->price,
                'discount' => $this->pivot->discount,
                'discount_is_percent' => $this->pivot->discount_is_percent,
            ],
            'count' => $this->pivot->count,
        ];
    }
}
