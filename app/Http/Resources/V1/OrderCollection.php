<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($order) {
                return [
                    'id' => $order->id,
                    'drugstore' => new DrugstoreResource($order->drugstore),
                    'status' => $order->status,
                    'promocode' => $this->when(!is_null($order->promocode), function () use ($order) {
                        return new PromocodeResource($order->promocode);
                    }),
                    'created_at' => $order->created_at,
                    'pay_datetime' => $order->pay_datetime,
                    'total_price' => $order->totalPrice,
                    'total_price_with_promocode' => $order->totalPriceWithPromocode
                ];
            }),
        ];
    }
}
