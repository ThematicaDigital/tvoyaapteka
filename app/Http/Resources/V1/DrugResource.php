<?php

namespace App\Http\Resources\V1;

use App\Models\Drug;
use Illuminate\Http\Resources\Json\JsonResource;

class DrugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $text = $this->text;
        if (!is_null($this->text)) {
            $text = str_ireplace('/local/images/rls.png', getAbsolutePath('/local/images/rls.png'), htmlspecialchars_decode($this->text));
        }
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'title' => $this->title,
            'text' => $text,
            'images' => getAbsolutePath($this->images),
            'image' => getAbsolutePath($this->image),
            'manufacturer' => new ManufacturerResource($this->whenLoaded('manufacturer')),
            'molecular_composition' => $this->molecular_composition,
            'type' => $this->type,
            'default_price' => $this->default_price,
            'drugstores' => DrugInStockResource::collection($this->whenLoaded('drugstores')),
            'prices' => $this->when($this->cities->isNotEmpty(), function () {
                return [
                    'price' => (double)$this->price,
                    'discount' => (double)$this->discount,
                    'discount_is_percent' => (boolean)$this->discount_is_percent,
                ];
            }),
            'analogs' => DrugAnalogResource::collection(Drug::getAnalogs($this->resource, $request->city_id)),
            'too_buy' => DrugAnalogResource::collection(Drug::getIsTooBuy($this->resource, $request->city_id)),
        ];
    }
}
