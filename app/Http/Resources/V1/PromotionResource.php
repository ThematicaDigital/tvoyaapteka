<?php

namespace App\Http\Resources\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class PromotionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'images' => getAbsolutePath($this->images),
            'url' => $this->url,
            'city_id' => $this->city_id,
            'drugs' => DrugWithoutAnalogsResource::collection($this->whenLoaded('drugs')),
        ];
    }

}
