<?php

namespace App\Http\Resources\V1;

use App\Models\DrugDrugstore;
use Illuminate\Http\Resources\Json\JsonResource;

class DrugstoreDrugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->drug_id,
            'in_stock' => $this->in_stock
        ];
    }
}
