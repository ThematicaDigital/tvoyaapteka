<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class DrugAnalogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'title' => $this->title,
            'text' => $this->description,
            'images' => getAbsolutePath($this->images),
            'image' => getAbsolutePath($this->image),
            'manufacturer' => new ManufacturerResource($this->whenLoaded('manufacturer')),
            'molecular_composition' => $this->molecular_composition,
            'type' => $this->type,
            'default_price' => $this->default_price,
            'drugstores' => DrugInStockResource::collection($this->whenLoaded('drugstores')),
            'prices' => [
                'price' => (double)$this->price,
                'discount' => (double)$this->discount,
                'discount_is_percent' => (boolean)$this->discount_is_percent,
            ],
        ];
    }
}
