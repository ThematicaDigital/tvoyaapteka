<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DrugSearchCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'drugs' => $this->collection->transform(function($drug){
                    return [
                        'id' => $drug->id,
                        'title' => $drug->title,
                        'images' => getAbsolutePath($drug->images),
                        'image' => getAbsolutePath($drug->image),
                        'type' => $drug->type,
                        'default_price' => $drug->default_price,
                        'prices' => $this->when($drug->cities->isNotEmpty(), function () use($drug) {
                            return [
                                'price' => (double)$drug->price,
                                'discount' => (double)$drug->discount,
                                'discount_is_percent' => (boolean)$drug->discount_is_percent,
                            ];
                        }),
                        'drugstores' => DrugInStockResource::collection($drug->drugstores),
                    ];
                }),
            ]
        ];
    }
}
