<?php

namespace App\Http\Resources\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'created_at' => $this->created_at,
            'announce' => $this->announce,
            'content' => $this->content,
            'images' => getAbsolutePath($this->images),
            'drugs' => DrugWithoutAnalogsResource::collection($this->whenLoaded('drugs')),
        ];
    }

}
