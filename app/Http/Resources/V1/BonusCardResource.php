<?php

namespace App\Http\Resources\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class BonusCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'amount' => round($this->amount, 2),
            'is_active' => $this->is_active,
            'activation_date' => $this->activation_date,
            'bonus_logs' => BonusLogResource::collection($this->whenLoaded('bonusLogs')),
        ];
    }

}
