<?php

namespace App\Http\Resources\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class BonusLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'drugstore_id' => $this->drugstore_id,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
        ];
    }

}
