<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class DrugCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'title' => $this->title,
            'images' => getAbsolutePath($this->images),
            'image' => $this->image,
            'type' => $this->type,
            'default_price' => $this->default_price,
            'drugstores' => DrugInStockResource::collection($this->whenLoaded('drugstores'))
        ];
    }
}
