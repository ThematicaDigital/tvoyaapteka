<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (!empty($this->drug->cities->keyBy('id')[$this->drugstore->city_id])) {
            $itemPrices = $this->drug->cities->keyBy('id')[$this->drugstore->city_id]->price;
            return [
                'drug' => new DrugCartResource($this->drug),
                'count' => $this->count,
                'drugstore_id' => $this->drugstore_id,
                'drugstores' => $this->drug->drugstores,
                'prices' => [
                    'price' => round($itemPrices->price, 2),
                    'discount' => $itemPrices->discount,
                    'discount_is_percent' => $itemPrices->discount_is_percent,
                    'price_with_discount' => $this->price_with_discount,
                    'total_price_with_discount' => $this->total_price_with_discount,
                    'item_price_with_discount_and_promocode' => $this->item_price_with_discount_and_promocode ?? $this->price_with_discount,
                    'total_item_price_with_discount_and_promocode' => $this->total_item_price_with_discount_and_promocode ?? $this->total_price_with_discount,
                ]
            ];
        }
        return [
            'drug' => new DrugCartResource($this->drug),
            'count' => $this->count,
            'drugstore_id' => $this->drugstore_id,
            'drugstores' => $this->drug->drugstores,
            'prices' => [
                'price' => round($this->drug->default_price, 2),
                'discount' => 0,
                'discount_is_percent' => false,
                'price_with_discount' => $this->price_with_discount,
                'total_price_with_discount' => $this->total_price_with_discount,
                'item_price_with_discount_and_promocode' => $this->item_price_with_discount_and_promocode ?? $this->price_with_discount,
                'total_item_price_with_discount_and_promocode' => $this->total_item_price_with_discount_and_promocode ?? $this->total_price_with_discount,
            ]
        ];
    }
}
