<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequestForApi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:255',
            'text' => 'required|string|min:2',
            'file' => 'mimes:pdf,docx,doc,jpg,png,jpeg',
            'phone' => 'required|regex:/[\d\+\-\(\)\s]{5,}/',
        ];
    }
}
