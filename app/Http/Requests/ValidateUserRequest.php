<?php

namespace App\Http\Requests;

use App\Models\Group;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ValidateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required',
                'digits:10',
                Rule::unique('users', 'phone')->ignore(Group::GROUP_ID['UNREG_USERS'] ,'group_id'),
            ],
            'password' => 'required|confirmed|min:6',
        ];
    }
}
