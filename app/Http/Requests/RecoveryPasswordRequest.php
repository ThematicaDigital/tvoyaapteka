<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class RecoveryPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'phone' => 'required|digits:10|exists:users,phone',
            'code' => ['required',
                Rule::exists('sms_codes')->where(function ($query) use($request) {
                    $query->where('phone',$request->phone);
                }),
            ],
            'password' => 'required|confirmed|min:6',
        ];
    }
}
