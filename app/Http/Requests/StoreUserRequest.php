<?php

namespace App\Http\Requests;

use App\Models\Group;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   

        return [
            'phone' => [
                'required',
                'digits:10',
                Rule::unique('users', 'phone')->ignore(Group::GROUP_ID['UNREG_USERS'] ,'group_id'),
            ],
            'sms_code' => ['required',
                Rule::exists('sms_codes', 'code')->where(function ($query) use($request) {
                    $query->where('phone',$request->phone);
                }),
            ],
            'password' => 'required|confirmed|min:6',
        ];
    }
}
