<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DrugRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'drug' => 'required|string|min:2|max:191',
            'name' => 'required|string|min:2|max:191',
            'phone' => 'required|digits_between:5,10|regex:/[\d\+\-\(\)\s]{5,}/',
            'count' => 'required|numeric',
        ];
    }
}
