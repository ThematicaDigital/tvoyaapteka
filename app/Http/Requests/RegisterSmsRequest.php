<?php

namespace App\Http\Requests;

use App\Models\Group;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RegisterSmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'phone' => [
                'required',
                'digits:10',
                Rule::unique('users', 'phone')->ignore(Group::GROUP_ID['UNREG_USERS'] ,'group_id'),
            ]
        ];
    }


    public function messages()
    {
        return [
            'phone.required' => 'Поле не заполнено',
            'phone.digits'   => 'Минимальная длина 10 символов',
            'phone.unique'   => 'Пользователь с таким номером зарегистрирован'
        ];
    }
}
