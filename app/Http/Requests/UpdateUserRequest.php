<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = \Auth::user();
        return [
            'firstname' => 'nullable|min:2',
            'lastname' => 'nullable|min:2',
            'surname' => 'nullable|min:2',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore($user->id, 'id'),
            ],
            'is_notify' => 'boolean',
            'old_password' => 'required_with:password|old_password:' . $user->password,
            'password' => 'confirmed|min:6',
        ];
    }
}
