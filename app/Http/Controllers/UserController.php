<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function forSelect(Request $request)
    {
        $phrase = $request->input('q');

        if ($phrase) {
            return User::where('phone', 'LIKE', '%' . $phrase . '%')->paginate(10);
        }

        return User::paginate(10);
    }
}
