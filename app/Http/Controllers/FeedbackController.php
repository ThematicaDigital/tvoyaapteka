<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Models\Feedback;
use App\Models\Drugstore;

class FeedbackController extends Controller
{

    /**
     * feedback index
     */
    public function index()
    {
        $drugstores = Drugstore::withCityId(session('city_id'))
            ->sortTitle()
            ->get();
        return view('feedback.index', compact('drugstores'));
    }

    /**
     * Save feedback
     *
     * @param \App\Http\Requests\FeedbackRequest $request
     * @return \Illuminate\Http\Response
     */
    public function send(FeedbackRequest $request)
    {
    	$fields = $request->except(['file', '_token']);
        if ($request->hasFile('file')) {
            $fields['file'] = 'uploads/' . $request->file->store('feedbacks', 'uploads');
        }
        Feedback::create($fields);
        $request->session()
            ->flash('flash_notification.level', 'success');
        $request->session()
            ->flash('flash_notification.message', 'Ваше отзыв успешно отправлен!');
        return redirect()->back();
    }
}