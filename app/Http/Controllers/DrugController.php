<?php

namespace App\Http\Controllers;

use App\Http\Requests\AutocompleteRequest;
use App\Http\Requests\DrugsSearchRequest;
use App\Models\Trigram;
use App\Models\Drug;
use App\Services\DrugService;
use Illuminate\Http\Request;

class DrugController extends Controller
{
    private $drugService;

    public function __construct(DrugService $drugService)
    {
        $this->drugService = $drugService;
    }
    /**
     * Autocomplete drugs for search
     *
     * @todo Chosen of city
     *
     * @param AutocompleteRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(AutocompleteRequest $request)
    {
        $results = (new Trigram)->searchSuggests($request->q, 100, 0);

        if (empty($results)) {
            $q = keyboardLayoutCorrection($request->q);
            $results = (new Trigram)->searchSuggests($q, 100, 0);
        }

        $suggestions = [];
        if ($results) {
            $cityId = $request->session()->get('city_id', false);
            $suggestions = Drug::select('drugs.*')
                ->withPrice($cityId)
                ->withCityId($cityId)
                ->withDrugstores($cityId)
                ->whereIn('drugs.id', collect($results['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values())
                ->orderBy(\DB::raw('FIELD(drugs.id, ' . collect($results['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values()->implode(',') . ')'))
                ->limit($request->count)
                ->get();
        }
        return response()->json([
            'suggestions' => $suggestions,
            'choosed_drugstore_id' => session('drugstore_id', null),
        ]);
    }

    /**
     * Result of drugs search
     *
     * @todo Chosen of city
     *
     * @param DrugsSearchRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(DrugsSearchRequest $request)
    {
        $drugs = (new Trigram)->searchSuggests($request->q, 12);
        $suggest = (new Trigram)->searchSuggest($request->q);
        $q = $request->q;
        return view('drugs/search', compact('suggest', 'drugs', 'q'));
    }

    public function forSelect(Request $request)
    {
        $phrase = $request->input('q');

        if ($phrase)
        {
            return Drug::where('title', 'LIKE', '%'.$phrase.'%')->orWhere('id', 'LIKE', '%'.$phrase.'%')->paginate(10);
        }

        return Drug::paginate(10);
    }

    public function getRecommendedDrugs(Drug $drug)
    {
        $drugs = $this->drugService->getRecommendedDrugsForCartPopup($drug);
        return response()->json([
            'drugs' => $drugs,
        ]);
    }
}
