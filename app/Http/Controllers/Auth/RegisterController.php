<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\V1\UserResource;
use App\Models\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Services\UserService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';
    private $userService;

    /**
     * Create a new controller instance.
     * @param UserService $userService
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param StoreUserRequest $request
     * @return UserResource
     */
    protected function create(StoreUserRequest $request)
    {
        $user = $this->userService->getOrCreateByPhone($request->phone, [
            'group_id' => Group::GROUP_ID['USERS'],
            'password' => $request->password,
        ]);
        Auth::login($user);

        return new UserResource($user);
    }
}
