<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecoveryPasswordRequest;
use Illuminate\Http\Response;
use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   


    public function forgetpassword(RecoveryPasswordRequest $request){

        $phone = $request->phone;

        $userobj = User::select('id')->where('phone',$phone)->first();

        $user = User::find($userobj->id);
        
        $user->password = $request->password;
        $user->save();

        return response()->json([]);
    }
}
