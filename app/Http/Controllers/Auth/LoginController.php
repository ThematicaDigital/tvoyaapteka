<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\AuthenticateRequest;
use Auth;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log in user
     * @param AuthenticateRequest $request
     * @return void
     */
    public function login(AuthenticateRequest $request)
    {
        $credentials = request(['phone', 'password']);
        $remember = $request->remember;
        if (Auth::attempt($credentials,$remember)) {
            if (!Auth::user()->isUnregistered) {
                return response()->json([], 200);
            }
        }
        return response()->json(['errors' => ['password' => ['Неверный телефон или пароль']]], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
