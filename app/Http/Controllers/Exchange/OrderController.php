<?php

namespace App\Http\Controllers\Exchange;


use App\Http\Resources\Exchange\OrderResource;
use App\Models\Order;
use App\Services\FirebaseService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    private $firebaseService;

    public function __construct(FirebaseService $firebase)
    {
        $this->firebaseService = $firebase;
    }

    /**
     * @api {get} exchange/orders/:id?[cancelled=1]Получение списка заказов после заказа с id
     * @apiName OrdersGet
     * @apiGroup Exchange
     * @apiParam :id id заказа
     * @apiParam cancelled Получить только отмененные заказы  1-получить отменные     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": [
     *        {
     *            "id": 32,
     *            "drugstore": {
     *                "id": 1008,
     *                "city_id": 8,
     *                "title": "Твояаптека.рф",
     *                "address": "Ул. Ленина, 87",
     *                "latitude": "50.25652379",
     *                "longitude": "127.5514545",
     *                "worktime": "с 8.00 до 20.00",
     *                "is_24h": false
     *            },
     *            "status": 1,
     *            "promocode": {
     *                "code": "81416810",
     *                "type": "2",
     *                "amount": 50,
     *                "is_percent": null
     *            },
     *            "created_at": {
     *                "date": "2018-06-21 15:00:30.000000",
     *                "timezone_type": 3,
     *                "timezone": "Asia/Yakutsk"
     *            },
     *            "user": {
     *                "firstname": null,
     *                "lastname": "",
     *                "surname": "",
     *                "phone": "9098848294",
     *                "email": "admin@site.com"
     *            },
     *            "total_price": 728.1,
     *            "total_price_with_promocode": 693.6,
     *            "drugs": [
     *                {
     *                    "id": 330,
     *                    "category_id": 37,
     *                    "title": "Алтея сироп 125мл",
     *                    "type": 1,
     *                    "price": 41.1,
     *                    "discount": null,
     *                    "discount_is_percent": false,
     *                    "count": 1
     *                },
     *                {
     *                    "id": 331,
     *                    "category_id": 37,
     *                    "title": "Алтея сироп 125мл",
     *                    "type": 1,
     *                    "price": 43.4,
     *                    "discount": null,
     *                    "discount_is_percent": false,
     *                    "count": 4
     *                },
     *                {
     *                    "id": 332,
     *                    "category_id": 156,
     *                    "title": "Алфавит Классик тбл 60 БАД",
     *                    "type": 1,
     *                    "price": 256.7,
     *                    "discount": null,
     *                    "discount_is_percent": false,
     *                    "count": 2
     *                }
     *            ]
     *        },
     * ...
     *    ]
     */

    public function get(Request $request, $id)
    {
        $orders = Order::where('id', '>', $id)
            ->when($request->cancelled, function ($query) {
                return $query->where('status', Order::STATUS['CANCELED']);
            }, function ($query) {
                return $query->where('status', Order::STATUS['IN_PROGRESS']);
            })
            ->with(['drugstore', 'user', 'promocode', 'drugs'])->get();
        return OrderResource::collection($orders);
    }

    /**
     * @api {post} exchange/orders/:order_id/:status Изменить статус заказа order_id на статус  status
     * @apiName OrdersChangeStatus
     * @apiGroup Exchange
     * @apiParam :order_id id заказа
     * @apiParam :status status статус: 1 - в работе(новый заказ создается с этим статусом), 3 - собран, 4-отменен, 5 - оплачен
     * @apiError Error404
     * @apiErrorExample (Error 404) {Object[]}
     *  HTTP/1.1 404 NOT FOUND
     * {
     *     "error": "Статус не найден"
     * }
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": {
     *        "id": 32,
     *        "status": 4,
     *        "promocode": {
     *            "code": "81416810",
     *            "type": "2",
     *            "amount": 50,
     *            "is_percent": null
     *        },
     *        "created_at": {
     *            "date": "2018-06-21 15:00:30.000000",
     *            "timezone_type": 3,
     *            "timezone": "Asia/Yakutsk"
     *        },
     *        "total_price": 728.1,
     *        "total_price_with_promocode": 693.6,
     *        "drugs": [
     *            {
     *                "id": 330,
     *                "category_id": 37,
     *                "title": "Алтея сироп 125мл",
     *                "type": 1,
     *                "price": 41.1,
     *                "discount": null,
     *                "discount_is_percent": false,
     *                "count": 1
     *            },
     *            {
     *                "id": 331,
     *                "category_id": 37,
     *                "title": "Алтея сироп 125мл",
     *                "type": 1,
     *                "price": 43.4,
     *                "discount": null,
     *                "discount_is_percent": false,
     *                "count": 4
     *            },
     *            {
     *                "id": 332,
     *                "category_id": 156,
     *                "title": "Алфавит Классик тбл 60 БАД",
     *                "type": 1,
     *                "price": 256.7,
     *                "discount": null,
     *                "discount_is_percent": false,
     *                "count": 2
     *            }
     *        ]
     *    }
     *}
     *
     */
    public function changeStatus(Order $order, Request $request, $status)
    {

        if (!in_array($status, Order::STATUS)) {
            return response()->json(['error' => 'Статус не найден'], Response::HTTP_NOT_FOUND);
        }
        $order->status = $status;
        $order->save();

        try {
            $this->firebaseService->notifyChangeOrderStatus($order);
        } catch (\Exception $e) {
            $errors = ['errors' => ['server' => [$e->getMessage()]]];
            return response()->json($errors, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new OrderResource($order->load('promocode'));
    }
}