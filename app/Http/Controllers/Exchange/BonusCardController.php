<?php

namespace App\Http\Controllers\Exchange;

use App\Http\Resources\Exchange\BonusCardResource;
use App\Models\BonusCard;
use App\Models\BonusLog;
use App\Models\User;
use App\Notifications\BonusCardCreated;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

class BonusCardController extends Controller
{

    /**
     * @api {get} exchange/bonus-cards/:search Бонусная карта
     * @apiName BonusCard
     * @apiGroup Exchange
     * @apiParam :search номер бонусной карты или номер телефона
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": {
     *        "id": 1,
     *        "phone": "9098848294",
     *        "number": "12345656",
     *        "amount": 0,
     *        "is_active": true,
     *        "activation_date": {
     *            "date": "2018-06-22 02:25:19.000000",
     *            "timezone_type": 3,
     *            "timezone": "Asia/Yakutsk"
     *        }
     *    }
     *}
     *
     */
    public function show(Request $request, $search)
    {
        try {
            $bonusCard = BonusCard::whereHas('user', function ($query) use ($search) {
                $query->where('phone', $search);
            })
                ->orWhere('number', $search)
                ->firstOrFail();
        } catch (\Throwable $e) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return new BonusCardResource($bonusCard);
    }

    /**
     * @api {post} exchange/bonus-cards/:search Добавление / списание бонусов
     * @apiName BonusCardStoreBonus
     * @apiGroup Exchange
     * @apiParam :search номер бонусной карты или номер телефона
     * @apiParam {String}   amount    Величина бонуса (прим: 11.5 , 10, -100)
     * @apiParam {String}   drugstore     id Аптеки в которой произошла транзакция     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": {
     *        "id": 1,
     *        "phone": "9098848294",
     *        "number": "12345656",
     *        "amount": 20,
     *        "is_active": true,
     *        "activation_date": {
     *            "date": "2018-06-22 02:25:19.000000",
     *            "timezone_type": 3,
     *            "timezone": "Asia/Yakutsk"
     *        }
     *    }
     *}
     *
     */
    public function storeBonusLog(Request $request, $search)
    {
        $bonusCard = BonusCard::whereHas('user', function ($query) use ($search) {
            $query->where('phone', $search);
        })
            ->orWhere('number', $search)->firstOrFail();
        $bonusLog = new BonusLog([
            'drugstore_id' => $request->drugstore,
            'amount' => $request->amount,
        ]);
        $bonusCard->amount += $request->amount;
        $bonusCard->bonusLogs()->save($bonusLog);
        $bonusCard->save();
        return new BonusCardResource($bonusCard);
    }


    /**
     * @api {post} exchange/bonus-cards/create Создание бонусной карты
     * @apiDescription Создание новой бонусной карты и пользователя с телефоном phone, если его не существует
     * @apiName BonusCardCreate
     * @apiGroup Exchange
     * @apiParam {String}   card_number    Номер карты
     * @apiParam {String}   phone          Телефон
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "message": "Карта успешно добавлена"
     *}
     * @apiError Error503 Ошибки смс сервиса (могут возникать при создании пользователя)
     * @apiError Error500 Внутренние ошибки сервера
     */
    public function create(Request $request)
    {
        $bonusCard = BonusCard::firstOrCreate(['number' => $request->card_number]);
        $user = User::firstOrNew(['phone' => $request->phone]);

        if (!$user->exists) {

            $password = rand(10000000, 99999999);
            $user->password = $password;
            $user->save();

            try {
                $user->notify(new BonusCardCreated($password));
            } catch (\Throwable $e) {
                return response()->json(['message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        if (!is_null($bonusCard->user) && $bonusCard->user->id != $user->id) {
            $errors = ['server' => 'Пользователь с такой картой уже существует'];
            return response()->json($errors, Response::HTTP_CONFLICT);
        }

        if (is_null($user->bonusCard)) {
            $bonusCard->user()->associate($user)->save();
            $data = ['message' => 'Карта успешно добавлена!'];
            return response()->json($data, Response::HTTP_CREATED);
        }

        if ($bonusCard->wasRecentlyCreated) {
            $bonusCard->forceDelete();
        }

        $data = [ 'message' => 'У пользователя уже есть дисконтная карта' ];
        return response()->json($data, Response::HTTP_OK);
    }
}