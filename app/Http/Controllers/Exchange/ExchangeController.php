<?php

namespace App\Http\Controllers\Exchange;

use App\Jobs\ExchangeJob;
use App\Exchange\Data\ExchangeDataParser;
use App\Exchange\ExchangeFileHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use App\Models\Import;

class ExchangeController extends Controller
{
    /**
     * Exchange mode
     * @var array
     */
    const MODE = [
        'INIT' => 'init',
        'CHECK_AUTH' => 'checkauth',
        'FILE' => 'file',
        'IMPORT' => 'import',
        'STATUS' => 'status',
    ];

    /**
     * Exchange type
     * @var array
     */
    const TYPE = [
        'CATALOG' => 'catalog',
        'SALE' => 'sale',
    ];

    /**
     * @var ExchangeFileHelper
     */
    private $exchangeFileHelper;

    public function __construct(ExchangeFileHelper $exchangeFileHelper)
    {
        \Debugbar::disable();
        $this->exchangeFileHelper = $exchangeFileHelper;
    }
    /**
     * Exchange with 1C
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index(Request $request)
    {
        if ($request->input('type') == self::TYPE['CATALOG']) {
            switch ($request->input('mode')) {
                case self::MODE['CHECK_AUTH']:
                    return $this->checkAuth($request);
                case self::MODE['INIT']:
                    return $this->init();
                case self::MODE['FILE']:
                    return $this->file($request);
                case self::MODE['IMPORT']:
                    return $this->import($request);
                case self::MODE['STATUS']:
                    return $this->status($request);
            }
        }
        return response("failure\nUnknown command type.");
    }

    /**
     * Check exchange auth
     * @param Request $request
     * @return Response
     */
    protected function checkAuth(Request $request)
    {
        return response(sprintf(
            "success\n%s\n%s\ntimestamp=%s\n",
            \Session::getName(),
            \Session::getId(),
            time()
        ));
    }

    /**
     * Initialization exchange
     * @return Response
     */
    protected function init()
    {
        Storage::deleteDirectory(config('exchange.storagePath'));
        Storage::makeDirectory(config('exchange.storagePath'));
        return response(sprintf("zip=%s\nfile_limit=%s\n", config('exchange.zip'), config('exchange.fileLimit')));
    }

    /**
     * Exсhange file
     * @param Request $request
     * @return Response
     */
    protected function file(Request $request)
    {
        $filename = str_replace('..', '', $request->get('filename'));
        // $filename = str_replace('..', '', $request->filename->getClientOriginalName());
        $data = file_get_contents('php://input');
        if (!empty($data))
            file_put_contents(storage_path('app/' . config('exchange.storagePath')) . "/$filename", $data, FILE_APPEND);
        else
            return response("failure\n");

        return response("success\n");
    }

    /**
     * Import from 1C
     * @param  Request $request
     * @return Response
     */
    protected function import(Request $request)
    {
        // ini_set('max_execution_time', 0);
        // set_time_limit(0);
        if (config('exchange.zip') === 'yes')
            $this->exchangeFileHelper->unzipAll(config('exchange.storagePath'));

        $absolutePath = storage_path('app/' . config('exchange.storagePath') . '/');
        $mapListenersToJson = [
            \App\Exchange\Listeners\CatalogListener::class => config('exchange.drugsFilename'),
            \App\Exchange\Listeners\DrugstoresListener::class => config('exchange.drugstoresFilename'),
            \App\Exchange\Listeners\OffersListener::class => config('exchange.priceFilename'),
            \App\Exchange\Listeners\DiscountListener::class => config('exchange.discountsFilename'),
            \App\Exchange\Listeners\InStockListener::class => config('exchange.inStockFilename'),
            \App\Exchange\Listeners\ForusesListener::class => config('exchange.recommendationsFilename'),
        ];
        $ids = '';
        $data = collect();
        foreach ($mapListenersToJson as $listener => $file) {
            $filepath = $absolutePath . $file;
            if (file_exists($filepath)) {
                $id = md5(time() . $filepath);
                $ids .= $file . "=${id}\n";
                Import::create(['import_id' => $id]);
                $data->push(
                    new ExchangeDataParser($filepath, new $listener(), $id)
                );
            }
        }
        ExchangeJob::dispatch($data)->onQueue('exchange');

        return response("success\n" . $ids);
    }

    /**
     * Check import status
     * @param  Request $request
     * @return Response
     */
    protected function status(Request $request)
    {
        $import = Import::where('import_id', $request->id)->first();
        if ($import) {
            if ($import->progress == 100) {
                return response("finished\nprogress=100%\n");
            }
            return response("processing\nprogress={$import->progress}%\n");
        }
        return response("not found\n");
    }
}
