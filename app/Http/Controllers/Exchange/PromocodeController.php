<?php

namespace App\Http\Controllers\Exchange;


use App\Jobs\GeneratePromocodeJob;
use App\Models\Promocode;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PromocodeController extends Controller
{

    /**
     * @api {post} exchange/promocodes Сгенерировать промокоды
     * @apiName PromocodesGenerate
     * @apiGroup Exchange
     * @apiParam {Number}   amount    Размер скидки промокода
     * @apiParam {Number}   amount_type     Тип скидки промокода: 1- в процентах, 2- в рублях, 3- фиксированная цена
     * @apiParam {Number}   count    Количество промокодов которое необходимо сгенерировать
     * @apiParam {Number}   type     Тип промокода: 1- Одноразовый, 2- Многоразовый, 3- Одноразовый для пользователя
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *[
     *    "81416810",
     *    "31366970",
     *    "50695165",
     *    "83734622",
     *    "79865290",
     *    "97253435",
     *    "67384484",
     *    "21485348",
     *    "20090787",
     *    "41251926",
     *    "44946348",
     *    "19598053",
     *    "13361192",
     *    "97046581",
     *    "20863774",
     *    "69613017",
     *    "22622731",
     *    "48483502",
     *    "42058051",
     *    "38289196"
     *]
     * @apiError Error422
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 UNPROCESSABLE ENTITY
     *{
     *    "error": "максимальное количество промокодов которые можно сгенерировать за один раз 1000"
     *}
     */
    public function generate(Request $request)
    {
        $availablePromocodesCount = config('promocodes.generateCount');

        if ($request->has('count')) {
            if ((int)$request->count > $availablePromocodesCount) {
                return response()->json(['error' => 'максимальное количество промокодов которые можно сгенерировать за один раз ' . $availablePromocodesCount], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $availablePromocodesCount = (int)$request->count;
        }

        $fields = [
            'type' => $request->type,
            'amount' => $request->amount,
            'amount_type' => $request->amount_type,
        ];
        $query = Promocode::select('title')->isEmpty()->orderBy('id')->limit($availablePromocodesCount);

        $emptyPromocodes = $query->get();
        $query->update($fields);

        GeneratePromocodeJob::dispatch();

        return response()->json($emptyPromocodes->pluck('title'));
    }
}