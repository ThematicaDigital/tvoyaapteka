<?php

namespace App\Http\Controllers;

use App\Services\SeoService;
use Illuminate\Http\Request;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleSlides;

class ArticleController extends Controller
{

    /**
     * View articles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::latest()->search()->paginate(6);
        $categories = ArticleCategory::get();
        SeoService::setMetaTags('Информация - Семейная аптека', 'семейная аптека информация', 'семейная аптека информация');
        return view('articles/index', compact('articles', 'categories'));
    }

    /**
     * View category articles
     *
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $category = ArticleCategory::where('slug', $slug)->firstOrFail();
        $articles = Article::latest()->where('category_id', $category->id)->paginate(6);
        $categories = ArticleCategory::get();
        SeoService::setMetaTags('Информация - Семейная аптека', 'семейная аптека информация', 'семейная аптека информация');
        return view('articles/category', compact('articles', 'categories', 'category'));
    }

    /**
     * View article
     *
     * @param string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function view($categorySlug, $slug)
    {
        $article = Article::where('slug', $slug)->with([
            'drugs' => function ($query) {
                $query->select('drugs.*')
                    ->inStockForCity(session('city_id'))
                    ->withPrice()
                    ->withDrugstores(session('city_id'))
                    ->with('manufacturer');
            }
        ])->firstOrFail();

        $article->views++;
        $article->save();
        
        SeoService::setTagsForArticle($article);
        return view('articles/view', compact('article'));
    }

    public static function slider()
    {
        $slides = ArticleSlides::inCity(session()->get('city_id'))
            ->where(function($query) {
                return $query->where(function($query) {
                    return $query->where('start_datetime', '<=', date("Y-m-d H:i:s"))
                        ->where('end_datetime', '>=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('start_datetime')
                        ->where('end_datetime', '>=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('end_datetime')
                        ->where('start_datetime', '<=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('end_datetime')
                        ->whereNull('start_datetime');
                });
            })
            ->orderBy(\DB::raw('ISNULL(`order`), `order`'), 'ASC')
            ->get();
        $query = request()->get('q');
        return view('articles/slider', compact('slides', 'query'));
    }
}
