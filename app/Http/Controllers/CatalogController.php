<?php

namespace App\Http\Controllers;

use App\Models\DosageForm;
use App\Services\DrugService;
use App\Services\SearchQueryService;
use App\Services\SeoService;
use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Drugstore;
use App\Models\Drug;
use App\Models\Trigram;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{

    /**
     * @var DrugService
     */
    private $drugService;
    /**
     * @var SearchQueryService
     */
    private $searchQueryService;

    public function __construct(DrugService $drugService, SearchQueryService $searchQueryService)
    {
        $this->drugService = $drugService;
        $this->searchQueryService = $searchQueryService;
    }
    /**
     * Catalog page
     * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        SeoService::setTagsForCatalogIndex($request->get('city'));

        $categories = Category::whereHas('drugs')->orWhereNull('parent_id')->orderBy('_lft')->get()->toTree();

        $categoryDrugsCount = Drug::select('category_id', DB::raw('count(*) as drugsCount'))
            ->inStockForCity($request->city_id)
            ->withPrice($request->city_id)
            ->groupBy('category_id')
            ->rememberCache(60)
            ->get()
            ->keyBy('category_id');

        $categories->map(function ($item) use($categoryDrugsCount) {
            $item['drugsCount'] = 0;
            if ($categoryDrugsCount->has($item->id)) {
                $item['drugsCount'] = $categoryDrugsCount[$item->id]['drugsCount'];
            }
            $item->children->map(function ($child) use($categoryDrugsCount) {
                $child['drugsCount'] = 0;
                if ($categoryDrugsCount->has($child->id)) {
                    $child['drugsCount'] = $categoryDrugsCount[$child->id]['drugsCount'];
                }
            });
        });
        return view('catalog/index', compact('categories', 'categoryDrugsCount'));
    }

    /**
     * @param $category_slug
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function category($category_slug, Request $request)
    {
        $category = Category::whereHas('drugs')->where('slug', $category_slug)->first();
        if (is_null($category)) {
            $category = Category::whereNull('parent_id')->where('slug', $category_slug)->firstOrFail();
        }

        SeoService::setTagsForCategory($request->get('city'), $category, $request->url());

        $drugstores = Drugstore::withCityId(session('city_id'))
            ->sortTitle()
            ->get();

        $request->merge(['category_id' => $category->id]);
        $items = collect($this->drugService->getSearchedDrugs($request, session('city_id')));
        return view('catalog/category', compact('items', 'category', 'drugstores'));
    }

    /**
     * Search drugs
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $cityId = $request->session()->get('city_id', false);

        if ($request->q) {
            $results = (new Trigram)->searchSuggests($request->q, 100, ($request->page - 1) * 100);
            if ($results) {
                return response()->json([
                    'drugs' => $this->drugService->getSearchedDrugs($request, $cityId, $results),
                    'pages' => (int)ceil($results['total']/$request->take),
                    'total' => $results['total']
                ]);
            }
            return response()->json([
                'drugs' => [],
                'pages' => 0,
                'total' => 0
            ]);
        } else {
            $totalQuery = Drug::inStockForCity($cityId);

            if ($request->sale) {
                $totalQuery->onlyWithDiscount($cityId);
            } else {
                $totalQuery->withPrice($cityId);
            }

            if ($request->is_24h) {
                $totalQuery->is24h();
            }
            if ($request->category_id) {
                $totalQuery->withCategoryId($request->category_id);
            }

            $total = $totalQuery->rememberCache(60)->get()->count();
            return response()->json([
                'drugs' =>  $this->drugService->getSearchedDrugs($request, $cityId),
                'pages' => (int)ceil($total/$request->take),
                'total' => $total
            ]);
        }
    }

    /**
     * [searchDrugs description]
     * @param  Request $request [description]
     * @return \Illuminate\Http\Response
     */
    public function searchDrugs(Request $request)
    {
        $drugstores = Drugstore::withCityId($request->city_id)
            ->sortTitle()
            ->rememberCache(60)
            ->get();

        $q = $request->q ?? false;
        if ($q !== false) {
            $this->searchQueryService->addOrIncrement($q);
        }
        $suggest = (new Trigram)->searchSuggest($q);

        $dosageForms = DosageForm::rememberCache(60)->get()->toJson();
        return view('catalog/category', compact('drugstores', 'suggest', 'q', 'dosageForms'));
    }

    /**
     * @param $category_slug
     * @param $drug_slug
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function view($category_slug, $drug_slug, Request $request)
    {
        $cityId = session()->get('city_id');

        $drug = Drug::select('drugs.*')
            ->withPrice($cityId)
            ->withDrugstores($cityId)
            ->where('slug', $drug_slug)
            ->firstOrFail();

        SeoService::setTagsForDrug($request->get('city'), $drug);

        $currentDrugstore = session()->get('drugstore_id', 0);
        $recommendedDrugs = $drug->isTooBuy;
        $recCount = $recommendedDrugs->count();
        if ($recCount < 9) {
            $limit = 9 - $recCount;
            $recommendedDrugs = $recommendedDrugs
                ->merge($this->drugService->getIndividualDrugs($limit))
                ->keyBy('id')->except($drug->id);
        }

        return view('catalog/view', compact('drug', 'currentDrugstore', 'recommendedDrugs'));
    }


    /**
     * Sidebar categories by category
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public static function sidebarCategories(Category $category)
    {
        $category->load([
            'children' => function($query){
                $query->whereHas('drugs')->orWhereNull('parent_id');
            }
        ]);
        $sidebarCategories = $category->children;
        if ($sidebarCategories->isEmpty()) {
            $sidebarCategories = $category->getSiblings();
        }
        return view('catalog/sidebar-categories', compact('sidebarCategories'));
    }

    /**
     * show drugs with discount
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function promotions(Request $request)
    {
        SeoService::setTagsForPromotions($request->url());
        $drugstores = Drugstore::withCityId(session('city_id'))
            ->sortTitle()
            ->get();

        $request->merge(['sale' => true]);
        $items = collect($this->drugService->getSearchedDrugs($request, session('city_id')));
        return view('promotions.catalog', compact('items', 'drugstores'));
    }

}
