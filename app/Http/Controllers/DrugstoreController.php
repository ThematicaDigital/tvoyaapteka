<?php

namespace App\Http\Controllers;

use App\Services\SeoService;
use Illuminate\Http\Request;

use App\Models\Drugstore;

class DrugstoreController extends Controller
{
    /**
     * Contacts page
     *
     * @return \Illuminate\Http\Response
     */
    public function contacts(Request $request)
    {
        $city_id = $request->session()->get('city_id');
        $drugstores = Drugstore::with('city')
            ->withActiveFreeday()
            ->WithCityId($city_id)
            ->orderBy('is_24h', 'DESC')
            ->orderBy('address')
            ->get();
        SeoService::setMetaTags('Адрес аптек  - Семейная аптека', 'семейная аптека адрес аптек', 'семейная аптека адрес аптек');
        return view('contacts/contacts', compact('drugstores'));
    }

    public function select(Request $request)
    {
        $city_id = $request->city;

        $drugstores = Drugstore::orderBy('title')
            ->with('city')
            ->withActiveFreeday()
            ->WithCityId($city_id)
            ->get();

        return response()->json($drugstores);
    }
}
