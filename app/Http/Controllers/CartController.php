<?php

namespace App\Http\Controllers;

use App\Http\Resources\Web\CartResource;
use App\Services\CartService;
use App\Services\DrugService;
use App\Services\SeoService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Exceptions\PromocodeException;
use App\Http\Resources\Web\CartCollection;
use App\Http\Resources\V1\PromocodeResource;
use App\Models\Promocode;
use App\Models\DrugDrugstore;

class CartController extends Controller
{
    const SESSION_KEY = 'cart';

    private $cartService;
    private $drugService;

    public function __construct(CartService $cartService, DrugService $drugService)
    {
        $this->cartService = $cartService;
        $this->drugService = $drugService;
    }


    /**
     * Add drug to cart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (Auth::check()) {
            $this->cartService->addItemsToUsersCart(collect($request->cart), Auth::user());
        } else {
            $drugs = collect($request->cart)->keyBy('drug_id');
            $cart = session()->get('cart', []);
            foreach ($drugs as $drug_id => $drug) {
                $cart[$drug_id] = $drug;
            }
            session()->put('cart', $cart);
            return response()->json($cart);
        }
        return response()->json([]);
    }

    /**
     * Delete drug from cart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->cartDrugs()->detach($request->drug_id);
        } else {
            $drugs = collect($request->cart)->keyBy('drug_id');
            $itemsInCart = session()->get('cart', []);
            unset($itemsInCart[$request->drug_id]);
            session()->put('cart', $itemsInCart);
        }
    	return response()->json([]);
    }

    public function get(Request $request)
    {
        /**
         * @todo Блок говнокода, в пизду пока что, ебал время тратить
         */

        if (!Auth::check()) {
            $cartItems = $this->cartService->getCartItemsForUnauthorizedUser(collect(session()->get('cart', [])), session()->get('city_id'));

            $totalOrderPriceWithoutPromocode = getTotalCartPriceGuest($cartItems);

            return (new CartCollection($cartItems))->additional([
                'data' => [
                    'promocode' =>  null,
                    'order_price' => $totalOrderPriceWithoutPromocode,
                    'order_price_with_promocode' => $totalOrderPriceWithoutPromocode,
                ]
            ]);
        }

        $promocodeResource = null;
        $user = Auth::user();

        $cartItems = $this->cartService->getUsersCartItemsByCityId($user, session()->get('city_id'));

        $totalOrderPriceWithoutPromocode = getTotalCartPriceGuest($cartItems);
        $totalOrderPrice = $totalOrderPriceWithoutPromocode;

        /**
         * @var Promocode $promocode
         */
        if (!empty($request->promocode)) {
            $promocode = Promocode::where('title', $request->promocode)->first();
            if (is_null($promocode)) {
                return response()->json(['errors' => ['promocode' => 'Промокод не найден']], Response::HTTP_NOT_FOUND);
            }
            if (!$promocode->validateByUser($user)) {
                return response()->json(['errors' => ['promocode' => 'Промокод уже был активирован этим пользователем']], Response::HTTP_FORBIDDEN);
            }
            $promocodeResource = new PromocodeResource($promocode);
            try {
                $totalOrderPrice = $promocode->getCartPriceWithPromocode($cartItems);
            } catch (PromocodeException $pe) {
                return response()->json(['errors' => ['promocode' => $pe->getMessage()]], Response::HTTP_FORBIDDEN);
            }
        }

        return (new CartCollection($cartItems))->additional([
            'data' => [
                'promocode' => $promocodeResource ?? null,
                'order_price' => $totalOrderPriceWithoutPromocode,
                'order_price_with_promocode' => $totalOrderPrice,
            ]
        ]);
    }

    public function index(Request $request)
    {
        SeoService::setMetaTags('Корзина - Семейная аптека', 'семейная аптека корзина', 'семейная аптека корзина');

        $makeOrder = false;
        $cityId = session()->get('city_id');
        if (!Auth::check()) {
            $cartItems = $this->cartService->getCartItemsForUnauthorizedUser(collect(session()->get('cart', [])), $cityId);
        } else {
            $user = Auth::user();
            $makeOrder = $request->has('make_order') && $request->make_order == $user->id;
            $cartItems = $this->cartService->getUsersCartItemsByCityId($user, $cityId);
        }

        $recommendedDrugs = collect([]);
        if ($cartItems->isNotEmpty()) {
            $recommendedDrugs = $this->drugService->getRecommendedDrugsByCartItems($cartItems);
        }

        $totalOrderPriceWithoutPromocode = getTotalCartPriceGuest($cartItems);

        $cartItems = (new CartCollection($cartItems))->additional([
            'data' => [
                'promocode' => $promocodeResource ?? null,
                'order_price' => $totalOrderPriceWithoutPromocode,
                'order_price_with_promocode' => $totalOrderPriceWithoutPromocode,
            ]
        ]);

        $cartItems->transform(function ($item) {
            return (new CartResource($item));
        });
        return view('cart/index', compact('cartItems', 'recommendedDrugs', 'makeOrder'));
    }

    public function clear(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->cartDrugs()->sync([]);
        } else {
            session()->put('cart', []);
        }
        return response()->json([]);
    }

}