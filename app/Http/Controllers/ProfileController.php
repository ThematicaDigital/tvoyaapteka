<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Services\FirebaseService;
use App\Services\SeoService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\Profile\BonusCardValidationRequest;
use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Models\BonusCard;
use Auth;

class ProfileController extends Controller
{
    private $firebase;
    public function __construct(FirebaseService $firebase)
    {
        $this->middleware('auth');
        $this->firebase = $firebase;
    }

    /**
     * Profile page
     * @return Illuminate\Http\Reponse
     */
    public function index()
    {
        SeoService::setMetaTags('Профиль - Семейная аптека', 'семейная аптека профиль', 'семейная аптека профиль');
        return view('profile.index');
    }

    /**
     * Edit profile
     *
     * @param UpdateUserRequest $request
     *
     * @return Illuminate\Http\Reponse
     */
    public function edit(UpdateUserRequest $request)
    {
        Auth::user()->update($request->all());
        return response()->json([]);
    }

    /**Change password page*/
    public function password()
    {
        return view('profile.changepswd');
    }

    /**
     * Change password
     *
     * @param ChangePasswordRequest $request
     *
     * @return Illuminate\Http\Reponse
     */
    public function changepswd(ChangePasswordRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $user->password = $request->newpswd;
        $user->save();
        return response()->json([]);
    }

    /**
     * Bonuscard
     *
     * @param ChangePasswordRequest $request
     *
     * @return Illuminate\Http\Reponse
     */
    public function bonus()
    {
        $bonuscard = Bonuscard::where('user_id',Auth::user()->id)->with(['bonusLogs' => function($q){
            $q->limit(10)->with('drugstore');
        }])->first();

        return view('profile.bonuscard',compact('bonuscard'));
    }

    public function createcard(BonusCardValidationRequest $request)
    {
        $user = Auth::user();
        $card = $request->card;

        if (is_null($user->bonusCard)) {
            $bonusCard = BonusCard::firstOrCreate(['number' => $card ]);
            $bonusCard->user()->associate($user)->save();
            return response()->json(['message' => 'Карта успешно добавлена!']);
        }
    }

    public function orders(Request $request)
    {
        SeoService::setMetaTags('Заказы - Семейная аптека', 'семейная аптека заказы', 'семейная аптека заказы');
        $orders = $this->getOrders($request);
        return view('profile.orders', compact('orders'));
    }

    public function getOrders(Request $request)
    {
        $ordering = $request->ordering ?? 'desc';
        return
            \Auth::user()->orders()
                ->when($request->has('status'), function ($query) use ($request) {
                    return $query->where('status', $request->status);
                })
                ->with(['drugs', 'drugs.manufacturer', 'drugstore', 'promocode'])
                ->orderBy('created_at', $ordering)
                ->get()
                ->toJson();
    }

    public function changeNotification(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $user->is_notify = $request->is_notify;
            $user->save();
            $this->firebase->subscribeOrUnsubscribePromotions($user);
        }
        return response()->json(['message' => 'Успешно изменено!']);
    }
}