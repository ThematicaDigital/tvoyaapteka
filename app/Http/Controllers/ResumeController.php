<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResumeRequest;
use App\Models\Resume;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    /**
     * Store a resume in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResumeRequest $request)
    {
        $fields = $request->except('file');
        if ($request->hasFile('file')) {
            $fields['file'] = 'uploads/' . $request->file->store('resumes', 'uploads');
        }
        Resume::create($fields);
        $request->session()
            ->flash('flash_notification.level', 'success');
        $request->session()
            ->flash('flash_notification.message', 'Ваше резюме успешно отправлено!');
        return redirect()->back();
    }

}
