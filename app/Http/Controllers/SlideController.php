<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Slide;

class SlideController extends Controller
{
    /**
     * Slider
     *
     * @param string $position
	 *
	 * @return \Illuminate\Http\Response
     */
    public static function slider($position)
    {
        $cityId = session()->get('city_id');
    	$slides = Slide::where('position', Slide::POSITION[strtoupper($position)])
            ->inCity($cityId)
            ->withDrug($cityId)
            ->where(function($query) {
                return $query->where(function($query) {
                    return $query->where('start_datetime', '<=', date("Y-m-d H:i:s"))
                        ->where('end_datetime', '>=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('start_datetime')
                        ->where('end_datetime', '>=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('end_datetime')
                        ->where('start_datetime', '<=', date("Y-m-d H:i:s"));
                })->orWhere(function($query) {
                    return $query->whereNull('end_datetime')
                        ->whereNull('start_datetime');
                });
            })
            ->orderBy(\DB::raw('ISNULL(`order`), `order`'), 'ASC')
    		->get();
        return view("slides/{$position}", compact('slides'));
    }
}