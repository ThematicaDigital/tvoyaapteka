<?php

namespace App\Http\Controllers;

use App\Http\Requests\DrugRequestRequest;
use App\Models\DrugRequest;

class DrugRequestController extends Controller
{

    /**
     * Save drug request
     *
     * @param \App\Http\Requests\DrugRequestRequest $request
     * @return \Illuminate\Http\Response
     */
    public function post(DrugRequestRequest $request)
    {
        try {
            DrugRequest::create($request->all());
        } catch (\Exception $e) {
            return response()->json([], 500);
        }

        return response()->json([], 200);
    }
}