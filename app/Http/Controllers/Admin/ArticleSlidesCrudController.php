<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\City;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticleSlidesRequest as StoreRequest;
use App\Http\Requests\ArticleSlidesRequest as UpdateRequest;

class ArticleSlidesCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ArticleSlides');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article_slides');
        $this->crud->setEntityNameStrings('слайд', 'слайды');

        $this->crud->addColumns([
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'image'
            ],
            [
                'name' => 'city_id',
                'label' => 'Город',
                'type' => 'select',
                'entity' => 'city',
                'attribute' => 'title',
                'model' => City::class
            ],
            [
                'name' => 'order',
                'label' => 'Очередность',
                'type' => 'text',
            ],
            [
                'name' => 'created_at',
                'label' => 'Дата создания',
                'type' => 'text'
            ]
        ]);

        $this->crud->addFields([
            [
                'name' => 'image',
                'label' => 'Изображение',
                'type' => 'browse'
            ],
            [
                'name' => 'mobile_image',
                'label' => 'Изображение для мобильной версии',
                'type' => 'browse'
            ],
            [
                'name' => 'url',
                'label' => 'Ссылка',
                'type' => 'url'
            ],
            [
                'name' => 'city_id',
                'label' => 'Город',
                'type' => 'select2',
                'entity' => 'city',
                'attribute' => 'title',
                'model' => City::class
            ],
            [
                'name' => 'start_datetime',
                'label' => 'Начало показа',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
                'allows_null' => true
            ],
            [
                'name' => 'end_datetime',
                'label' => 'Окончание показа',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
                'allows_null' => true
            ],
            [
                'name' => 'order',
                'label' => 'Очередность',
                'type' => 'number',
            ],
        ]);

        $this->crud->orderBy('order', 'DESC');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
