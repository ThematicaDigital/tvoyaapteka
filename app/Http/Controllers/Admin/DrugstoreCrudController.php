<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Drugstore;
use App\Models\Freeday;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\DrugstoreRequest as StoreRequest;
use App\Http\Requests\Admin\DrugstoreRequest as UpdateRequest;

class DrugstoreCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Drugstore::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/drugstores');
        $this->crud->setEntityNameStrings('Аптека', 'Аптеки');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $baseFields = [
            [
                'name' => 'title',
                'label' => 'Название аптеки'
            ],
            [
                'name' => 'address',
                'label' => 'Адрес аптеки'
            ],
            [
                'name' => 'city_id',
                'label' => 'Город',
                'type' => 'select',
                'entity' => 'city',
                'attribute' => 'title',
                'model' => City::class
            ],
            [
                'name' => 'worktime',
                'label' => 'Рабочее время'
            ],
            [
                'name' => 'is_24h',
                'label' => 'Круглосуточно',
                'type' => 'checkbox'
            ],
            [
                'name' => 'freeday_id',
                'label' => 'Нерабочий день',
                'type' => 'select',
                'entity' => 'freeday',
                'attribute' => 'admin_title',
                'model' => Freeday::class,
                'searchLogic' => false,
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->addFields([
            [
                'name' => 'latitude',
                'label' => 'Широта'
            ],
            [
                'name' => 'longitude',
                'label' => 'Долгота'
            ],
            [
                'name' => 'images',
                'label' => 'Изображения',
                'type' => 'browse_multiple',
                'multiple' => true,
            ],
//            [
//                'name' => 'is_24h',
//                'label' => 'Круглосуточно',
//                'type' => 'checkbox'
//            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
