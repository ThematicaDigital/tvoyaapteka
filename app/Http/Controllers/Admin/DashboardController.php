<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Order;

class DashboardController
{
	public function index()
	{
		return view('vendor.backpack.base.dashboard');
	}

	public function statistic(Request $request)
	{	

		$begin = $request->begin;

		$end = $request->end;

		$orders = Order::select('status','created_at')->whereBetween('created_at',[$begin,$end])->orderBy('created_at', 'asc')->get();

		return response()->json(['orders' => $orders]);
	}
}
