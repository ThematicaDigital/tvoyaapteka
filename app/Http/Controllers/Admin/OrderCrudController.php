<?php

namespace App\Http\Controllers\Admin;

use App\Models\Drug;
use App\Models\Drugstore;
use App\Models\Order;
use App\Models\Promocode;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\OrderRequest as StoreRequest;
use App\Http\Requests\Admin\OrderRequest as UpdateRequest;

class OrderCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Order::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/orders');
        $this->crud->setEntityNameStrings('заказ', 'заказы');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $baseFields = [
            [
                'name' => 'id',
                'id' => 'Id',
                'type' => 'number'
            ],
            [
                'name' => 'user_id',
                'label' => 'Пользователь',
                'type' => 'text',
                'entity' => 'user',
                'attribute' => 'phone',
                'model' => User::class,
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereHas('user', function ($q) use ($column, $searchTerm) {
                        $q->where('phone', 'like', '%' . $searchTerm . '%');
                    });
                }
            ],
            [
                'name' => 'status',
                'label' => 'Статус заказа',
                'type' => 'select_from_array',
                'options' => [
                    Order::STATUS['IN_PROGRESS'] => 'В работе',
                    Order::STATUS['APPROVED'] => 'Одобрен',
                    Order::STATUS['DONE'] => 'Выполнен',
                    Order::STATUS['CANCELED'] => 'Отменен',
                    Order::STATUS['PAID'] => 'Оплачен',
                ]
            ],
            [
                'name' => 'drugstore_id',
                'label' => 'Аптека',
                'type' => 'select',
                'entity' => 'drugstore',
                'attribute' => 'address',
                'model' => Drugstore::class
            ],
            [
                'name' => 'promocode_id',
                'label' => 'Промокод',
                'type' => 'select',
                'entity' => 'promocode',
                'attribute' => 'title',
                'model' => Promocode::class
            ],
            [
                'name' => 'pay_datetime',
                'label' => 'Время выкупа',
                'type' => 'datetime',
                'searchLogic' => false
            ],
            [
                'name' => 'created_at',
                'label' => 'Дата оформления заказа',
                'type' => 'datetime',
                'searchLogic' => false
            ],
        ];
        $this->crud->addColumns($baseFields);
        $this->crud->addFields([
            [
                'name' => 'id',
                'id' => 'Id',
                'type' => 'number'
            ],
            [
                'name' => 'status',
                'label' => 'Статус заказа',
                'type' => 'select_from_array',
                'options' => [
                    Order::STATUS['IN_PROGRESS'] => 'В работе',
                    Order::STATUS['APPROVED'] => 'Одобрен',
                    Order::STATUS['DONE'] => 'Выполнен',
                    Order::STATUS['CANCELED'] => 'Отменен',
                    Order::STATUS['PAID'] => 'Оплачен',
                ]
            ],
            [
                'name' => 'drugstore_id',
                'label' => 'Аптека',
                'type' => 'select',
                'entity' => 'drugstore',
                'attribute' => 'address',
                'model' => Drugstore::class
            ],
            [
                'label' => 'Товары в заказе',
                'type' => 'select2_from_ajax_multiple',
                'name' => 'drugs',
                'entity' => 'drugs',
                'attribute' => 'title_admin',
                'model' => Drug::class,
                'data_source' => route('crud.drugs.for_select'),
                'placeholder' => 'Выберите товар',
                'minimum_input_length' => 3,
                'pivot' => true,
            ],
            [
                'name' => 'pay_datetime',
                'label' => 'Время выкупа',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ],
            [
                'name' => 'created_datetime',
                'label' => 'Дата оформления заказа',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ]
        ]);
//        TODO: pivot table drug_order не заполняется

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
