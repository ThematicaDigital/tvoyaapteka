<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticleCategoryRequest as StoreRequest;
use App\Http\Requests\ArticleCategoryRequest as UpdateRequest;

class ArticleCategoryCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ArticleCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article_categories');
        $this->crud->setEntityNameStrings('категория', 'категории');

        $this->crud->addColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас',
            ],
            [
                'name' => 'created_at',
                'label' => 'Дата создания',
                'type' => 'text'
            ]
        ]);
        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Заголовок',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'meta_title',
                'label' => 'meta title',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'meta keywords',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'meta_description',
                'label' => 'meta description',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
