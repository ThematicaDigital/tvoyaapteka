<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promotion;
use App\Models\City;
use App\Models\Drug;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\PromotionRequest as StoreRequest;
use App\Http\Requests\Admin\PromotionRequest as UpdateRequest;

class PromotionCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Promotion::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/promotions');
        $this->crud->setEntityNameStrings('акция', 'акции');

        $baseFields = [
            [
                'name' => 'title',
                'label' => 'Заголовок акции'
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);

        $this->crud->addFields([
            [
                'name' => 'text',
                'label' => 'Содержимое акции',
                'type' => 'textarea'
            ],
            [
                'name' => 'images',
                'label' => 'Изображения',
                'type' => 'browse_multiple',
                'multiple' => true,
            ],
            [
                'name' => 'url',
                'label' => 'Ссылка на страницу',
                'type' => 'text',
            ],
            [
                'name' => 'start_datetime',
                'label' => 'Начало акции',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ],
            [
                'name' => 'end_datetime',
                'label' => 'Окончание акции',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ],
            [
                'name' => 'city_id',
                'label' => 'Город',
                'type' => 'select',
                'entity' => 'city',
                'attribute' => 'title',
                'model' => City::class
            ],
            // [
            //     'label' => 'Товары в акции',
            //     'type' => 'select2_from_ajax_multiple',
            //     'name' => 'drugs',
            //     'entity' => 'drugs',
            //     'attribute' => 'title_admin',
            //     'model' => Drug::class,
            //     'data_source' => route('crud.drugs.for_select'),
            //     'placeholder' => 'Выберите товар',
            //     'minimum_input_length' => 3,
            //     'pivot' => true,
            // ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud($request);
        return $redirect_location;
    }
}
