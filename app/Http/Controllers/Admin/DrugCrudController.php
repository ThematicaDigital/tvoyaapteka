<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Drug;
use App\Models\Manufacturer;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\DrugRequest as StoreRequest;
use App\Http\Requests\Admin\DrugRequest as UpdateRequest;
use Illuminate\Http\Request;

class DrugCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Drug::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/drugs');
        $this->crud->setEntityNameStrings('товар', 'товары');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

//        $this->crud->setFromDb();
        $baseFields = [
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'title',
                'label' => 'Название'
            ],
            [
                'name' => 'category_id',
                'label' => 'Категория',
                'type' => 'select',
                'entity' => 'category',
                'attribute' => 'title',
                'model' => Category::class
            ],
            [
                'name' => 'created_at',
                'label' => 'Дата добавления',
                'type' => 'date',
                'searchLogic' => false,
            ],
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Дата создания',
            'type' => 'text'
        ]);

        $this->crud->addFields([
            [
                'name' => 'title',
                'label' => 'Название',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'category_id',
                'label' => 'Категория',
                'type' => 'select',
                'entity' => 'category',
                'attribute' => 'title',
                'model' => Category::class,
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'manufacturer_id',
                'label' => 'Производитель',
                'type' => 'select2',
                'entity' => 'manufacturer',
                'attribute' => 'title',
                'model' => Manufacturer::class,
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'molecular_composition',
                'label' => 'Молекулярный состав',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'too_buy',
                'label' => 'С этим товаром покупают',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'registration_number',
                'label' => 'Регистрационный номер',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'type',
                'label' => 'Рецептурный препарат',
                'type' => 'select_from_array',
                'options' => [
                    Drug::PRESCRIPTION['NO'] => 'Нет',
                    Drug::PRESCRIPTION['NORMAL'] => 'Рецептурный',
                    Drug::PRESCRIPTION['HIGH'] => 'Строго рецептурный',
                ],
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'publish',
                'label' => 'Публиковать?',
                'type' => 'checkbox',
                'default' => true,
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'is_hit',
                'label' => 'Хит продаж',
                'type' => 'checkbox',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'text',
                'label' => 'Описание препарата',
                'type' => 'ckeditor',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'images',
                'label' => 'Изображения',
                'type' => 'browse_multiple',
                'multiple' => true,
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'search_ranking',
                'label' => 'Приоритет поиска из 1с',
                'type' => 'number',
                'tab' => 'Настройки поиска'
            ],
            [
                'name' => 'search_ranking_manual',
                'label' => 'Приоритет поиска',
                'type' => 'number',
                'tab' => 'Настройки поиска'
            ],
            [
                'name' => 'additional_search',
                'label' => 'Дополнительный поиск',
                'tab' => 'Настройки поиска'
            ],
            [
                'name' => 'default_price',
                'label' => 'Цена по умолчанию',
                'type' => 'number',
                'tab' => 'Основные настройки'
            ],
            [
                'name' => 'meta_title',
                'label' => 'meta title',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'meta_keywords',
                'label' => 'meta keywords',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'meta_description',
                'label' => 'meta description',
                'type' => 'text',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'seo_text',
                'label' => 'SEO текстовый блок',
                'type' => 'wysiwyg',
                'tab' => 'Настройки СЕО'
            ],
            [
                'name' => 'slug',
                'label' => 'Алиас',
                'tab' => 'Основные настройки'
            ],
        ]);

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        $this->crud->denyAccess(['create']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}