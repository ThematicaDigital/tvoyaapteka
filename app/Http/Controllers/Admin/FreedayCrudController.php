<?php

namespace App\Http\Controllers\Admin;

use App\Models\Freeday;
use App\Models\FreedayStatus;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Admin\FreedayRequest as StoreRequest;
use App\Http\Requests\Admin\FreedayRequest as UpdateRequest;

class FreedayCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Freeday::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/freedays');
        $this->crud->setEntityNameStrings('Нерабочий день', 'Нерабочие дни');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $baseFields = [
            [
                'name' => 'start_datetime',
                'label' => 'Начало',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ],
            [
                'name' => 'end_datetime',
                'label' => 'Окончание',
                'type' => 'datetime_picker',
                'datetime_picker_options' => [
                    'format' => 'YYYY-MM-DD HH:mm'
                ],
            ],
            [
                'name' => 'freeday_status_id',
                'label' => 'Статус',
                'type' => 'select',
                'entity' => 'freedayStatus',
                'attribute' => 'title',
                'model' => FreedayStatus::class
            ]
        ];

        $this->crud->addColumns($baseFields);
        $this->crud->addFields($baseFields);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
