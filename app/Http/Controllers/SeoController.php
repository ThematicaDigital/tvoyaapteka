<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Services\SitemapService;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    private $sitemapService;
    /**
     * Create a new command instance.
     * @param SitemapService $sitemapService
     * @return void
     */
    public function __construct(SitemapService $sitemapService)
    {
        $this->sitemapService = $sitemapService;
    }

    /**
     * sitemap for city
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function sitemap(Request $request)
    {
        $city = City::findOrFail($request->city_id);
        $sitemap = $this->sitemapService->getSitemapForCity($city);
        return response()->file($sitemap, ['Content-Type' => 'text\xml']);
    }

    /**
     * robots
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function robots(Request $request)
    {
        $host = url('');
        $sitemap = route('seo.sitemap');
        return response()
            ->view('robots', compact('host', 'sitemap'))
            ->header('Content-Type','text');
    }
}
