<?php

namespace App\Http\Controllers;

use App\Services\SeoService;
use Auth;
use Cookie;
use App\Models\Drug;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    /**
     * Favorites page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        SeoService::setMetaTags('Избранное - Семейная аптека', 'семейная аптека избранное', 'семейная аптека избранное');
        return view('favorites/index');
    }

    /**
     * Add drug to favorites
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $favorites = unserialize(Cookie::get(Drug::FAVORITES_COOKIE));
        $favorites[$request->drug_id] = $request->drug_id;
        Cookie::queue(
            Drug::FAVORITES_COOKIE,
            serialize($favorites),
            Drug::FAVORITES_EXPIRES
        );
        return response()->json([]);
    }

    /**
     * Delete drug from favorites
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (Auth::check()) {
            Auth::user()
                ->favouriteDrugs()
                ->detach($request->drug_id);
        }
        $favorites = unserialize(Cookie::get(Drug::FAVORITES_COOKIE));
        unset($favorites[$request->drug_id]);
        Cookie::queue(
            Drug::FAVORITES_COOKIE,
            serialize($favorites),
            Drug::FAVORITES_EXPIRES
        );
    	return response()->json([]);
    }

    /**
     * Search favorites
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $favorites = unserialize(Cookie::get(Drug::FAVORITES_COOKIE));
        $drugs = Drug::select('drugs.*')->whereIn('drugs.id', (array)$favorites)->withPrice()
            ->withDrugstores($request->city_id)->paginate(6);
        return response()->json([
            'drugs' => $drugs->items(),
            'pages' => $drugs->lastPage()
        ]);
    }
}