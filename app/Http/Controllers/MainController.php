<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Services\SeoService;
use Illuminate\Http\Request;
use App\Models\City;

class MainController extends Controller
{
    /**
     * Main page
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function main(Request $request)
    {
        SeoService::setTagsForMainPage($request->get('city'));
        return view('index');
    }

    /**
     * Choose city
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function chooseCity(Request $request)
    {
        $scheme = request()->server()['REQUEST_SCHEME'] ?? 'http';
        $domain = env('SESSION_DOMAIN', '.tvoyaapteka.ru');
        $city = City::where('id', $request->selectedCityId)->first();
        session()->put('city_id', $request->selectedCityId);
        session()->put('user_city', $city->slug);
        session()->forget('drugstore_id');
        session()->forget('cart');
        if (\Auth::check()) {
            Cart::where('user_id', \Auth::user()->id)->delete();
        }
        return response()->json([
            'url' => "{$scheme}://{$city->slug}{$domain}"
        ]);
    }

    /**
     * Choose drugstore
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function chooseDrugstore(Request $request)
    {
        $request->session()->put('drugstore_id', $request->drugstore_id);
    }

    /**
     * Load drugstores for header
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function drugstores(Request $request)
    {
        $drugstores = \App\Models\Drugstore::orderBy('title')
            ->withActiveFreeday()
            ->where('city_id', $request->session()->get('city_id'))
            ->get();
        return response()->json([
            'drugstores' => $drugstores
        ]);
    }
}
