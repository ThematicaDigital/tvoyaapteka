<?php

namespace App\Http\Controllers;

use App\Models\Promotion;

class PromotionController extends Controller
{
	/**
	 * List of promotions
     * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $promotions = Promotion::inCity(session()->get('city_id'))
            ->active()
            ->orderBy('start_datetime')
            ->get()
            ->toJson();
        return view('promotions.index', compact('promotions'));
    }
}