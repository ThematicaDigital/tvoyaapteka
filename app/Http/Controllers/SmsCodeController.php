<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterSmsRequest;
use App\Services\SmsCodeService;
use App\Http\Requests\StoreSmsPasswordRequest;
use App\Models\SmsCode;
use Illuminate\Http\Request;

class SmsCodeController extends Controller
{   
    
    private $smsCodeService;
    /**
     * SmsCodeController constructor.
     * @param $smsCodeService
     */
    public function __construct(SmsCodeService $smsCodeService)
    {
        $this->smsCodeService = $smsCodeService;
    }
    /**
     * @api {post} sms/send-code Запрос смс кода
     * @apiName SmsCode
     * @apiGroup Sms
     * @apiUse AcceptHeader
     * @apiParam {String}   phone     Телефон
     *
     * @apiError (Error 422) {String[]} phone Ошибки валидации
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *  {
     *      "message": "The given data was invalid.",
     *      "errors": {
     *          "phone": [
     *              "The phone must be 10 digits."
     *          ]
     *      }
     *  }
     * @apiSuccess (Success 200) {String} message Смс с кодом отправлено
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *  {
     *      "message": "Sms код отправлен"
     *  }
     */

    /**
     * @param StoreSmsPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendCodeForExistingUser(StoreSmsPasswordRequest $request)
    {   
        return $this->smsCodeService->sendCode($request->phone);
    }
    
    public function sendCodeForFoggotPassword(StoreSmsPasswordRequest $request)
    {
//        $code = SmsCode::where('phone',$request->phone)->first();
//        if($code){
//            SmsCode::where('phone',$request->phone)->delete();
//        }
        return $this->smsCodeService->sendCode($request->phone);
    }

    public function sendCodeForRegister(RegisterSmsRequest $request)
    {   
//        $code = SmsCode::where('phone',$request->phone)->first();
//        if($code){
//            SmsCode::where('phone',$request->phone)->delete();
//        }
        return $this->smsCodeService->sendCode($request->phone);
    }

}
