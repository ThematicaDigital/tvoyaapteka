<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\RegionResource;
use App\Models\Region;

class RegionController extends Controller
{
    /**
     * @api {get} api/regions Список регионов и городов
     * @apiName allRegionsAndCities
     * @apiGroup Regions
     * @apiUse AcceptHeader
     *
     * @apiSuccess (Success 200) {Object} data  Регион
     * @apiSuccess (Success 200) {Number} data.id    id региона
     * @apiSuccess (Success 200) {String} data.title Название региона
     * @apiSuccess (Success 200) {Object[]} data.cities Города региона
     * @apiSuccess (Success 200) {Number} data.cities.id Id города
     * @apiSuccess (Success 200) {String} data.cities.title Название города
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *"data": [
     *        {
     *            "id": 14,
     *            "title": "Республика Саха (Якутия)",
     *            "cities": [
     *                {
     *                    "id": 23,
     *                    "region_id": 14,
     *                    "title": "Нерюнгри"
     *                },
     *                {
     *                    "id": 24,
     *                    "region_id": 14,
     *                    "title": "Якутск"
     *                }
     *            ]
     *        },
     *         ...
     *      ]
     *}
     */



    /**
     * Display a listing of regions with cities.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $regions = Region::with(['cities' => function($query){
            return $query->orderBy('title', 'ASC');
        }])->get();
        return RegionResource::collection($regions);
    }
}
