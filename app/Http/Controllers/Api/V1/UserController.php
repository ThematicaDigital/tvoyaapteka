<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\ValidateUserRequest;
use App\Http\Resources\V1\DrugWithoutAnalogsResource;
use App\Http\Resources\V1\UserResource;
use App\Models\Drug;
use App\Models\Group;
use App\Models\User;
use App\Services\FirebaseService;
use App\Services\SmsCodeService;
use App\Services\UserService;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


/**
 * @resource Auth
 *
 * Auth
 */
class UserController extends Controller
{

    /**
     * @var SmsCodeService
     */
    private $smsCodeService;

    /**
     * @var FirebaseService
     */
    private $firebase;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param $smsCodeService
     * @param $firebase
     * @param $userService
     */
    public function __construct(SmsCodeService $smsCodeService, FirebaseService $firebase, UserService $userService)
    {
        $this->smsCodeService = $smsCodeService;
        $this->userService = $userService;
        $this->firebase = $firebase;
    }

    /**
     * @apiDefine HeaderWithTokenAndFormUrlEncoded
     *
     * @apiHeader Authorization Bearer token
     * @apiHeader Accept application/vnd.tvoyaapteka.[version].api+json
     * @apiHeader Content-Type application/x-www-form-urlencoded
     * @apiHeaderExample {json} Header:
     *  {
     *      "Authorization" : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcHRla2EuaHNcL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1MjA5OTM2MTUsImV4cCI6MTUyMTA4MDAxNSwibmJmIjoxNTIwOTkzNjE1LCJqdGkiOiIwcm5zbFFQQ0UzRHpFTVZVIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.tvb961bzN4j7ksEY3bF_hae6rozxztuI5VHFDEP1gIU",
     *      "Accept" : "application/vnd.tvoyaapteka.v1.api+json",
     *      "Content-Type" : "application/x-www-form-urlencoded"
     * }
     */

    /**
     * @apiDefine SuccessUserResource
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 201 CREATED
     *  HTTP/1.1 200 OK
     * {
     *     "data": {
     *         "id": 1,
     *         "firstname": "Антон",
     *         "lastname": "Титов",
     *         "surname": "Иванович",
     *         "phone": "9098848295",
     *         "email": "admin@site.com",
     *         "is_notify": false
     *      }
     * }
     */

    /**
     * @api {post} api/user Регистрация пользователя
     * @apiName RegisterUser
     * @apiGroup User
     * @apiUse AcceptHeader
     * @apiUse SuccessToken
     * @apiParam {String}   phone     Телефон пользователя
     * @apiParam {String}   password  Пароль
     * @apiParam {String}   password_confirmation  Подтверждение пароля
     * @apiParam {String}   sms_code  Код из смс для подтверждения регистрации
     *
     * @apiError (Error 422) {String[]} sms_code Неверный код
     * @apiError (Error 422) {String[]} phone Пользователь с таким телефоном уже существует
     * @apiError (Error 500) {String[]} server ошибки сервера
     *
     */

    /**
     * Store a newly created user in storage.
     * @param StoreUserRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function store(StoreUserRequest $request)
    {
        try {
            $user = $this->userService->getOrCreateByPhone($request->phone, [
                'group_id' => Group::GROUP_ID['USERS'],
                'password' => $request->password,
            ]);
            $this->smsCodeService->delete([
                'phone' => $request->phone,
                'code' => $request->sms_code
            ]);
        } catch (\Exception $e) {
            $errors = ['errors' => ['server' => [$e->getMessage()]]];
            return response()->json($errors, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        //        TODO: что то сделать, подобное есть в App\Http\Controllers\Api\V1\AuthController
        $token = auth('api')->login($user);

        $data = [
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60
            ]
        ];
        return response()->json($data, Response::HTTP_CREATED);
    }

    /**
     * @api {patch} api/user/ Редактирование профиля пользователя
     * @apiName EditUser
     * @apiGroup User
     * @apiUse HeaderWithTokenAndFormUrlEncoded
     * @apiUse SuccessUserResource
     * @apiParam {String}   [firstname]    Имя пользователя
     * @apiParam {String}   [lastname]     Фамилия пользователя
     * @apiParam {String}   [surname]     Отчество пользователя
     * @apiParam {Boolean}   [is_notify]  Уведомлять о новых акциях
     * @apiParam {String}   [password]  Пароль
     * @apiParam {String}   [password_confirmation]  Подтверждение пароля
     * @apiParam {String}   [old_password]  Старый пароль(поле обязательно если в запросе есть поле password)
     * @apiParam {String}   [email]  Email
     *
     * @apiError (Error 500) {String[]} server ошибки сервера
     * @apiError (Error 404) {String[]} NotFound
     *
     */

    /**
     * @param UpdateUserRequest $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request)
    {
        try {
            $user = \Auth::user();
            $user->fill($request->except('phone'));
            $user->save();
            $this->firebase->subscribeOrUnsubscribePromotions($user);
        } catch (\Exception $e) {
            $errors = ['errors' => ['server' => [$e->getMessage()]]];
            return response()->json($errors, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new UserResource($user);
    }


    /**
     * @api {post} api/user/validate Валидация пользователя до регистрации, запрос смс кода
     * @apiName ValidateUser
     * @apiGroup User
     * @apiUse AcceptHeader
     * @apiParam {String}   phone     Телефон пользователя
     * @apiParam {String}   password  Пароль
     * @apiParam {String}   password_confirmation  Подтверждение пароля
     *
     * @apiError (Error 422) {String[]} phone ошибки валидации телефона
     * @apiError (Error 422) {String[]} password ошибки валидации пароля
     * @apiError (Error 500) {String[]} server ошибки сервера
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *  {
     *      "message": "The given data was invalid.",
     *      "errors": {
     *          "password": [
     *              "The password field is required."
     *          ]
     *      }
     *  }
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * {
     *     "message": "Sms код отправлен",
     * }
     */

    /**
     *
     * @param ValidateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateBeforeRegister(ValidateUserRequest $request)
    {
        return $this->smsCodeService->sendCode($request->phone);
    }
    /**
     * @api {get} api/user/favourites/?city_id=:city_id Получить избранные товары пользователя
     * @apiName UserFavouritesDrugsGet
     * @apiGroup User
     * @apiParam city_id id города для которого будет подгружаться цена
     * @apiUse HeaderWithToken
     * @apiUse OffsetLimit
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * {
     *     "data": [
     *         {
     *             "id": 330,
     *             "category_id": 37,
     *             "title": "Алтея сироп 125мл",
     *             "text": null,
     *             "images": [
     *                 "/uploads/drugs/ta_330.jpg"
     *             ],
     *             "molecular_composition": "Алтея лекарственного корней экстракт",
     *             "type": 1,
     *             "default_price": 44.4,
     *             "drugstores": [
     *                 {
     *                     "id": 1030,
     *                     "in_stock": 1
     *                 },
     *                  ....
     *             ],
     *             "prices": {
     *                 "price": 41.1,
     *                 "discount": 0,
     *                 "discount_is_percent": false
     *             }
     *         },
     *          ....
     *     ]
     * }
     */

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getFavourites(Request $request)
    {
        $drugs = \Auth::user()->favouriteDrugs()
            ->select('drugs.*')
            ->withDrugstores()
            ->withPrice($request->city_id)
            ->when($request->limit, function ($query) use ($request) {
                return $query->limit($request->limit);
            })
            ->when($request->offset, function ($query) use ($request) {
                return $query->offset($request->offset);
            })
            ->get();
        return DrugWithoutAnalogsResource::collection($drugs);
    }

    /**
     * @api {post} api/user/favourites/:id Добавить в избраное пользователя товар
     * @apiName UserFavouritesDrugsAdd
     * @apiGroup User
     * @apiUse HeaderWithToken
     * @apiParam {Number} id id товара
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * {
     *    "data":
     *        {
     *            "id": 1,
     *            "category_id": 146,
     *            "title": "Autem nulla maxime sit ducimus saepe laudantium voluptatem magnam.",
     *            "text": null,
     *            "images": null,
     *            "manufacturer_id": 1,
     *            "molecular_composition": null,
     *            "type": "1",
     *            "default_price": "300.00"
     *        }
     * }
     */
    public function putFavourites(Drug $drug)
    {
        $user = \Auth::user();
        $user->favouriteDrugs()->syncWithoutDetaching($drug->id);
        return response()->json([], Response::HTTP_OK);
    }

    /**
     * @api {delete} api/user/favourites/:id Удалить товар из избранного
     * @apiName UserFavouritesDrugsDelete
     * @apiGroup User
     * @apiUse HeaderWithToken
     * @apiParam {Number} id id товара
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * {
     *    "data":
     *        {
     *            "id": 1,
     *            "category_id": 146,
     *            "title": "Autem nulla maxime sit ducimus saepe laudantium voluptatem magnam.",
     *            "text": null,
     *            "images": null,
     *            "manufacturer_id": 1,
     *            "molecular_composition": null,
     *            "type": "1",
     *            "default_price": "300.00"
     *        }
     * }
     */
    /**
     * @param Drug $drug
     * @return DrugWithoutAnalogsResource
     */
    public function detachFavourites(Drug $drug)
    {
        $user = \Auth::user();
        $user->favouriteDrugs()->detach($drug->id);
        return response()->json([], Response::HTTP_OK);
    }

    /**
     * @api {post} api/user/restore-password Восстановление пароля
     * @apiName UserPasswordRestore
     * @apiGroup User
     * @apiUse AcceptHeader
     * @apiUse SuccessUserResource
     * @apiParam {String}   phone     Телефон пользователя
     * @apiParam {String}   password  Пароль
     * @apiParam {String}   password_confirmation  Подтверждение пароля
     * @apiParam {String}   sms_code  Код из смс для подтверждения смены пароля
     *
     * @apiError (Error 422) {String[]} sms_code Неверный код
     * @apiError (Error 422) {String[]} phone Пользователь с таким телефоном уже существует
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *  {
     *      "message": "The given data was invalid.",
     *      "errors": {
     *          "sms_code": [
     *              "Неверный код"
     *          ]
     *      }
     *  }
     *
     */

    /**
     * @param Request $request
     * @return UserResource
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|digits:10|exists:users,phone',
            'password' => 'required|confirmed|min:6',
            'sms_code' => 'required',
        ]);
        $user = User::getByPhone($request->phone);
        $user->password = $request->password;
        $user->save();
        return response()->json([], Response::HTTP_OK);
    }


    /**
     * @api {get} api/user/ Получить текущего пользователя
     * @apiName UserShow
     * @apiGroup User
     * @apiUse HeaderWithToken
     * @apiUse SuccessUserResource
     */
    /**
     * @return UserResource
     */
    public function show()
    {
        return new UserResource(\Auth::user());
    }
}