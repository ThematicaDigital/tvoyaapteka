<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\AuthenticateRequest;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;


/**
 * @resource Auth
 *
 * Auth
 */
class AuthController extends Controller
{
    /**
     * @apiDefine AcceptHeader
     * @apiHeader Accept application/vnd.tvoyaapteka.[version].api+json
     * @apiHeaderExample {json} Header:
     *  {
     *      "Accept" : "application/vnd.tvoyaapteka.v1.api+json"
     * }
     */

    /**
     * @apiDefine TokenErrors
     * @apiError (Error 401) {String[]} token ошибки токена
     * @apiError (Error 400) {String[]} Token has expired
     * @apiErrorExample {json} 401:
     *  HTTP/1.1 401 Unauthorized
     * {
     * "errors":
     *     {
     *         "token": [
     *              "The token has been blacklisted"
     *          ]
     *     }
     * }
     * @apiErrorExample {json} 400:
     *  HTTP/1.1 400 Bad request
     * {
     * "errors":
     *     {
     *         "token": [
     *              "Token has expired"
     *          ]
     *     }
     * }
     */

    /**
     * @apiDefine HeaderWithToken
     *
     * @apiHeader Authorization Bearer token
     * @apiHeader Accept application/vnd.tvoyaapteka.[version].api+json
     * @apiHeaderExample {json} Header:
     *  {
     *      "Authorization" : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcHRla2EuaHNcL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1MjA5OTM2MTUsImV4cCI6MTUyMTA4MDAxNSwibmJmIjoxNTIwOTkzNjE1LCJqdGkiOiIwcm5zbFFQQ0UzRHpFTVZVIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.tvb961bzN4j7ksEY3bF_hae6rozxztuI5VHFDEP1gIU",
     *      "Accept" : "application/vnd.tvoyaapteka.v1.api+json"
     * }
     */

    /**
     * @apiDefine SuccessToken
     * @apiSuccess (Success 200) {String} access_token  Токен доступа для последующих запросов к ресурсам, требующим аутентификацию
     * @apiSuccess (Success 200) {String} token_type    Тип токена
     * @apiSuccess (Success 200) {Number} expires_in    Время действия токена в секундах
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *  {
     *      data {
     *          "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcHRla2EuaHNcL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1MjA5OTM2MTUsImV4cCI6MTUyMTA4MDAxNSwibmJmIjoxNTIwOTkzNjE1LCJqdGkiOiIwcm5zbFFQQ0UzRHpFTVZVIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.tvb961bzN4j7ksEY3bF_hae6rozxztuI5VHFDEP1gIU",
     *          "token_type": "bearer",
     *          "expires_in": 86400
     *      }
     *  }
     *
     */

    /**
     * @api {post} api/auth/login Аутентификация
     * @apiName LoginUser
     * @apiGroup Auth
     * @apiUse AcceptHeader
     * @apiUse SuccessToken
     *
     * @apiParam {String}   phone     Телефон пользователя в формате 9141234567
     * @apiParam {String}   password  Пароль
     *
     * @apiError (Error 422) {String[]} phone ошибки валидации телефона
     * @apiError (Error 422) {String[]} password ошибки валидации пароля
     * @apiError (Error 401) {String[]} AuthenticationFailed неверный логин или пароль
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *  {
     *      "message": "The given data was invalid.",
     *      "errors": {
     *          "phone": [
     *              "The phone must be at least 10 characters."
     *          ],
     *          "password": [
     *              "The password field is required."
     *          ]
     *      }
     *  }
     * @apiErrorExample {json} 401:
     *  HTTP/1.1 401 Unauthorized
     *{
     *    "errors": {
     *        "Authentication failed": [
     *            "Incorrect phone or password"
     *        ]
     *    }
     *}
     */

     /**
     * Handle a login request to the application.
     *
     * @param  AuthenticateRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(AuthenticateRequest $request)
    {
        $credentials = request(['phone', 'password']);

        if ($token = auth('api')->attempt($credentials)) {
            if (!auth('api')->user()->isUnregistered) {
                return $this->respondWithToken($token);
            }
        }
        $errors = ['Authentication failed' => ['Incorrect phone or password']];
        return response()->json(['errors' => $errors], Response::HTTP_UNAUTHORIZED);

    }

    /**
     * @api {post} api/auth/logout Logout
     * @apiName LogoutUser
     * @apiGroup Auth
     * @apiUse HeaderWithToken
     * @apiUse TokenErrors
     * @apiSuccess (Success 200) {String} message Successfully logged out
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *  {
     *      "message": "Successfully logged out"
     *  }
     */

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out'], Response::HTTP_OK);
    }


    /**
     * @api {post} api/auth/refresh-token Обновление токена
     * @apiName RefreshToken
     * @apiGroup Auth
     * @apiDescription Обновить токен если срок его действия истек
     * @apiUse SuccessToken
     * @apiUse HeaderWithToken
     * @apiUse TokenErrors
     *
     * @apiSuccess (Success 202) {String} message Token is valid yet.
     * @apiSuccessExample {json} 202:
     *  HTTP/1.1 202 ACCEPTED
     *  {
     *      "message": "Token is valid yet."
     *  }
     */

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        try {
            $token = auth('api')->refresh();
        } catch (JWTException $e) {
            $errors = ['token' => [$e->getMessage()]];
            return response()->json(['errors' => $errors], Response::HTTP_UNAUTHORIZED);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function respondWithToken($token)
    {
        $data = [
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60
            ]
        ];
        return response()->json($data);
    }
}