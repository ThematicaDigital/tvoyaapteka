<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\PromocodeException;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\CartCollection;
use App\Http\Resources\V1\PromocodeResource;
use App\Models\Cart;
use App\Models\Promocode;
use App\Services\CartService;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CartController extends Controller
{
    private $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }
    /**
     * @api {get} api/cart?city_id=:city_id&[promocode=:promocode] Получить корзину пользователя
     * @apiName GetCart
     * @apiGroup Cart
     * @apiUse HeaderWithToken
     * @apiParam {String} promocode Промокод
     * @apiParam {Number} city_id ид города
     * @apiError Error403 Условия промокода не выполнены
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Условия промокода не выполнены"
     *          ]
     *     }
     * }
     * @apiError Error403 Промокод уже был активирован этим пользователем
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Промокод уже был активирован этим пользователем"
     *          ]
     *     }
     * }
     * @apiError Error404 Промокод не найден
     * @apiErrorExample {json} 404:
     *  HTTP/1.1 404 NOT FOUND
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Промокод не найден"
     *          ]
     *     }
     * }
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *{
     *     "data": {
     *          "items": [
     *               {
     *                   "drug": {
     *                      "id": 334,
     *                       "title": "Алфавит 50+ тбл 60 БАД",
     *                      "images": [
     *                           "/uploads/drugs/ta_334.jpg"
     *                       ],
     *                      "type": 1,
     *                       "default_price": 300.1,
     *                       "drugstores": [
     *                           {
     *                              "id": 1015,
     *                               "in_stock": 1
     *                           },
     *                           ...
     *                       ]
     *                  },
     *                   "count": 1,
     *                   "drugstore_id": 1001,
     *                   "prices": {
     *                         "price": 180.4,
     *                         "discount": 10,
     *                         "discount_is_percent": true,
     *                         "price_with_discount": 162.36,
     *                         "total_price_with_discount": 324.72
     *                   }
     *                },
     *                    ...
     *           ],
     *          "promocode": {
     *             "code": "12345678"
     *          },
     *          "order_price": 630.28,
     *          "order_price_with_promocode": 598.77
     *      }
     * }
     */

    /**
     * get user cart
     * @param Request $request
     * @return CartCollection
     */
    public function index(Request $request)
    {
        $promocodeResource = null;
        $user = Auth::user();

        $cartItems = $this->cartService->getUsersCartItemsByCityId($user, $request->city_id);

        $totalOrderPriceWithoutPromocode = getTotalCartPrice($cartItems);
        $totalOrderPrice = $totalOrderPriceWithoutPromocode;
        /**
         * @var Promocode $promocode
         */
        if ($request->has('promocode')) {
            $promocode = Promocode::where('title', $request->promocode)->first();
            if (is_null($promocode)) {
                return response()->json(['errors' => ['promocode' => 'Промокод не найден']], Response::HTTP_NOT_FOUND);
            }
            if (!$promocode->validateByUser($user)) {
                return response()->json(['errors' => ['promocode' => 'Промокод уже был активирован этим пользователем']], Response::HTTP_FORBIDDEN);
            }
            $promocodeResource = new PromocodeResource($promocode);
            try {
                $totalOrderPrice = $promocode->getCartPriceWithPromocode($cartItems);
            } catch (PromocodeException $pe) {
                return response()->json(['errors' => ['promocode' => $pe->getMessage()]], Response::HTTP_FORBIDDEN);
            }
        }

        return (new CartCollection($cartItems))->additional([
            'data' => [
                'promocode' => $promocodeResource ?? null,
                'order_price' => $totalOrderPriceWithoutPromocode,
                'order_price_with_promocode' => $totalOrderPrice,
            ]
        ]);
    }

    /**
     * @api {post} api/cart Добавить товар(ы) в корзину
     * @apiName PostCart
     * @apiGroup Cart
     * @apiUse HeaderWithToken
     * @apiParam {Object[]} cart
     * @apiParam {Number} cart.drug_id id товара
     * @apiParam {Number} cart.count количество товара
     * @apiParam {Number} cart.drugstore_id id аптеки
     * @apiParamExample {json} Request-Example:
     * {
     *  "cart":
     *      [
     *          {
     *              "drug_id":20387,
     *              "count":1,
     *              "drugstore_id":1001
     *          },
     *          ...
     *      ]
     * }
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * []
     */

    /**
     * Add drug to cart
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $this->cartService->addItemsToUsersCart(collect($request->cart), Auth::user());
        return response()->json([]);
    }

    /**
     * @api {delete} api/cart удалить товар(ы) из корзины
     * @apiName DeleteCart
     * @apiGroup Cart
     * @apiUse HeaderWithToken
     * @apiParam {Number[]} drugs  Массив id товаров которые необходимо удалить, если не передавать, очистится вся корзина
     * @apiParamExample {json} Request-Example:
     * {
     *  "drugs":
     *      [
     *          1234,
     *          56,
     *          78
     *      ]
     * }
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     * []
     */

    /**
     * Delete drug from cart, if drugs is empty all it will be deleted
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Auth::user()->cartDrugs()->detach($request->drugs);
        return response()->json([]);
    }

}