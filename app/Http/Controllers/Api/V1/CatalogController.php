<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\CategoryResource;
use App\Http\Resources\V1\DrugCollection;
use App\Http\Resources\V1\DrugSearchCollection;
use App\Http\Resources\V1\DrugResource;
use App\Models\Category;
use App\Models\Drug;
use App\Services\DrugService;
use App\Services\SearchQueryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Trigram;

class CatalogController extends Controller
{
    private $drugService;
    private $searchQueryService;

    public function __construct(DrugService $drugService, SearchQueryService $searchQueryService)
    {
        $this->drugService = $drugService;
        $this->searchQueryService = $searchQueryService;
    }

    /**
     * @apiDefine SuccessCategory
     *
     * @apiSuccess (Success 200) {Object} data  Категория
     * @apiSuccess (Success 200) {Number} data.id    id категории
     * @apiSuccess (Success 200) {String} data.title Заголовок категории
     * @apiSuccess (Success 200) {Number} data.parent_id    id родительской категории
     */

    /**
     * @apiDefine SuccessDrugCollection
     *
     * @apiSuccess (Success 200) {Object[]} data Товар
     * @apiSuccess (Success 200) {Number} data.id    id товара
     * @apiSuccess (Success 200) {String} data.title Название товара
     * @apiSuccess (Success 200) {String[]} data.images Массив путей к изображениям
     * @apiSuccess (Success 200) {Number} data.type Тип препарата: 1- Без рецепта, 2- рецептурный, 3- строго рецептурный
     * @apiSuccess (Success 200) {Number} data.default_price Цена по умолчанию
     * @apiSuccess (Success 200) {Object} data.prices
     * @apiSuccess (Success 200) {Number} data.prices.price Цена товара в указанном городе
     * @apiSuccess (Success 200) {Number} data.prices.discount Скидка в указанном городе
     * @apiSuccess (Success 200) {Boolean} data.prices.discount_is_percent Скидка в процентах?
     * @apiSuccess (Success 200) {Object[]} data.drugstores Список аптек в которых товар в наличии
     * @apiSuccess (Success 200) {Number} data.drugstores.id Id аптеки
     * @apiSuccess (Success 200) {Number} data.drugstores.in_stock Количество товара
     */

    /**
     * @apiDefine SuccessSearchDrugCollection
     *
     * @apiSuccess (Success 200) {Object[]} data Товары
     * @apiSuccess (Success 200) {Object[]} data.drugs Товар
     * @apiSuccess (Success 200) {Number} data.drugs.id    id товара
     * @apiSuccess (Success 200) {String} data.drugs.title Название товара
     * @apiSuccess (Success 200) {String[]} data.drugs.images Массив путей к изображениям
     * @apiSuccess (Success 200) {Number} data.drugs.type Тип препарата: 1- Без рецепта, 2- рецептурный, 3- строго рецептурный
     * @apiSuccess (Success 200) {Number} data.drugs.default_price Цена по умолчанию
     * @apiSuccess (Success 200) {Object} data.drugs.prices
     * @apiSuccess (Success 200) {Number} data.drugs.prices.price Цена товара в указанном городе
     * @apiSuccess (Success 200) {Number} data.drugs.prices.discount Скидка в указанном городе
     * @apiSuccess (Success 200) {Boolean} data.drugs.prices.discount_is_percent Скидка в процентах?
     * @apiSuccess (Success 200) {Object[]} data.drugs.drugstores Список аптек в которых товар в наличии
     * @apiSuccess (Success 200) {Number} data.drugs.drugstores.id Id аптеки
     * @apiSuccess (Success 200) {Number} data.drugs.drugstores.in_stock Количество товара
     * @apiSuccess (Success 200) {String} data.suggest Возможно вы имели ввиду
     */

    /**
     * @api {get} api/catalog/ Список категорий
     * @apiName AllCategories
     * @apiGroup Catalog
     * @apiUse AcceptHeader
     * @apiUse SuccessCategory
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *  "data": [
     *        {
     *            "id": 4,
     *            "title": "Бад и лечебная косметика",
     *            "parent_id": null
     *        },
     *        {
     *            "id": 156,
     *            "title": "БАДы Витамины",
     *            "parent_id": 4
     *        },
     *      ...
     *  ]
     *}
     */
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $categories = Category::orderBy('_lft')->rememberCache(60)->get();
        return CategoryResource::collection($categories);
    }

    /**
     * @api {get} api/catalog/:category_id/?city_id=:city_id&[price_min=:price_min]&[price_max=:price_max]&[order=:order]&[sort=:sort]&[sales=:sales]&[hit=:hit]&[manufacturers=:manufacturers]  Список товаров категории, которые есть в наличии в городе
     * @apiName CategoryDrugs
     * @apiGroup Catalog
     * @apiUse AcceptHeader
     * @apiUse OffsetLimit
     * @apiUse SuccessDrugCollection
     * @apiParam {Number} category_id id категории или 0
     * @apiParam {Number} city_id id города
     * @apiParam {Number=0,1} [sales=0] товары по акции (товары у которых есть скидка)
     * @apiParam {Number=0,1} [hit=0] хиты продаж
     * @apiParam {String} [manufacturers=] производители, id через ,
     * @apiParam {Number} [price_min=0] минимальная цена
     * @apiParam {Number} [price_max=0] максимальная цена
     * @apiParam {String="asc","desc"} [order=asc] порядок сортировки
     * @apiParam {String="price","title"} [sort=price] сортировка
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": [
     *         {
     *             "id": 171,
     *             "title": "АД Минус тбл 40БАД",
     *             "type": 1,
     *             "default_price": 390.1,
     *             "images": [
     *                 "/uploads/drugs/ta_171.jpg"
     *             ],
     *             "prices": {
     *                 "price": 213.70,
     *                 "discount": null,
     *                "discount_is_percent": false
     *            },
     *            "drugstores": [
     *                {
     *                    "id": 112696,
     *                    "in_stock": 1
     *                },
     *                ...
     *            ]
     *        },
     *         {
     *            "id": 172,
     *            "title": "АД Норма капс 60БАД",
     *            "images": [
     *                "/uploads/drugs/ta_172.jpg"
     *            ],
     *            "prices": {},
     *            "drugstores": [
     *                {
     *                    "id": 112798,
     *                     "in_stock": 1
     *                },
     *                ...
     *            ]
     *        },
     *          ...
     *      ]
     *}
     */

    /**
     * @param $categoryId
     * @param Request $request
     * @return DrugCollection
     */
    public function category($categoryId, Request $request)
    {
        $drugs = $this->drugService->getDrugsFromCategory($categoryId, $request);
        return new DrugCollection($drugs);
    }

    /**
     * @api {get} api/drugs/:drug_id/?city_id=:city_id Товар, с аптеками и ценами
     * @apiName Drugs
     * @apiGroup Drugs
     * @apiUse AcceptHeader
     * @apiParam {Number} drug_id id товара
     * @apiParam {Number} city_id id города
     * @apiUse SuccessDrugCollection
     * @apiSuccess (Success 200) {Object} data Товар
     * @apiSuccess (Success 200) {Number} data.category_id id категории товара
     * @apiSuccess (Success 200) {Object} data.document Данные из базы Видаль
     * @apiSuccess (Success 200) {String} data.document.CompiledComposition Состав и форма выпуска
     * @apiSuccess (Success 200) {String} data.document.ClPhGrDescription КФГ
     * @apiSuccess (Success 200) {String} data.document.PhInfluence Фармакологическое действие
     * @apiSuccess (Success 200) {String} data.document.PhKinetics Фармакокинетика
     * @apiSuccess (Success 200) {String} data.document.Dosage Способ применения и дозы
     * @apiSuccess (Success 200) {String} data.document.OverDosage Передозировка
     * @apiSuccess (Success 200) {String} data.document.Interaction Лекарственное взаимодействие
     * @apiSuccess (Success 200) {String} data.document.Lactation Применение при беременности и кормлении грудью
     * @apiSuccess (Success 200) {String} data.document.SideEffects Побочное действие
     * @apiSuccess (Success 200) {String} data.document.StorageCondition Условия и сроки хранения
     * @apiSuccess (Success 200) {String} data.document.Indication Показания к применению
     * @apiSuccess (Success 200) {String} data.document.ContraIndication Противопоказания к применению
     * @apiSuccess (Success 200) {String} data.document.SpecialInstruction Особые указания
     * @apiSuccess (Success 200) {String} data.document.PregnancyUsing Противопоказания при беременности
     * @apiSuccess (Success 200) {String} data.document.NursingUsing Противопоказания при уходе
     * @apiSuccess (Success 200) {String} data.document.RenalInsuf Применение при нарушениях функции почек
     * @apiSuccess (Success 200) {String} data.document.HepatoInsuf Применение при нарушениях функции печени
     * @apiSuccess (Success 200) {String} data.document.ElderlyInsuf Применение у пожилых пациентов
     * @apiSuccess (Success 200) {String} data.document.ChildInsuf Применение у детей
     * @apiSuccess (Success 200) {Object} data.manufacturer
     * @apiSuccess (Success 200) {Number} data.manufacturer.id id производителя
     * @apiSuccess (Success 200) {String} data.manufacturer.title Название производителя
     * @apiSuccess (Success 200) {String} data.molecular_composition Молекулярный состав
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *{
     *    "data": {
     *        "id": 171,
     *        "category_id": 107,
     *        "title": "АД Минус тбл 40БАД",
     *        "text": "<B>Строка</B>",
     *        "images": [
     *            "/uploads/drugs/ta_171.jpg"
     *        ],
     *        "manufacturer": {
     *            "id": 655,
     *            "title": "Эвалар ЗАО"
     *        },
     *        "molecular_composition": "Эпинефрин",
     *        "type": 1,
     *        "default_price": 1,
     *        "drugstores": [
     *            {
     *                "id": 112696,
     *                "in_stock": 1
     *            },
     *            ...
     *        ],
     *        "prices": {
     *            "price": 213.7,
     *            "discount": null,
     *            "discount_is_percent": false
     *        },
     *        "analogs": [
     *             {
     *                 "id": 190,
     *                 "category_id": 22,
     *                 "title": "Адреналин 0,1% амп 1мл 5",
     *                 "text": null,
     *                 "images": null,
     *                 "image": "/images/no_photo.png",
     *                 "manufacturer": {
     *                     "id": 366,
     *                     "title": "Московский эндокринный завод ФГУП"
     *                 },
     *                 "molecular_composition": "Эпинефрин",
     *                 "type": 1,
     *                 "default_price": 73.6,
     *                 "prices": {
     *                     "price": "73.80",
     *                     "discount": null,
     *                     "discount_is_percent": 0
     *                 }
     *             }
     *         ],
     *         "too_buy": [
     *             {
     *                 "id": 424,
     *                 "category_id": 38,
     *                 "title": "Амоксиклав  375мг тбл пп об 15",
     *                 "text": null,
     *                 "images": null,
     *                 "image": "/images/no_photo.png",
     *                 "manufacturer": {
     *                     "id": 1815,
     *                     "title": "Лек д.д."
     *                 },
     *                 "molecular_composition": "Амоксициллин + Клавулановая кислота",
     *                 "type": 1,
     *                 "default_price": 229.4,
     *                 "prices": {
     *                     "price": "223.10",
     *                     "discount": null,
     *                     "discount_is_percent": 0
     *                 }
     *             },
     *              ....
     *          ]
     *    }
     *}
     */

    /**
     * @param int $drugId
     * @param Request $request
     * @return DrugResource
     */
    public function getDrug(int $drugId, Request $request)
    {
        return new DrugResource($this->drugService->getDrugByIdWithDataForCity($drugId, $request->city_id));
    }

    /**
     * @api {get} api/catalog/search?q=:q&city_id=:city_id&[price_min=:price_min]&[price_max=:price_max]&[order=:order]&[sort=:sort]&[sales=:sales]&[hit=:hit]&[manufacturers=:manufacturers]&[isVoiced=:isVoiced] Поиск товаров
     * @apiName SearchDrugs
     * @apiGroup Catalog
     * @apiUse AcceptHeader
     * @apiUse OffsetLimit
     * @apiUse SuccessSearchDrugCollection
     * @apiParam {String} q поисковый запрос
     * @apiParam {Number} city_id id города
     * @apiParam {Boolean} [sales=false] товары по акции (товары у которых есть скидка)
     * @apiParam {Boolean} [isVoiced=false] Запрос набран голосом
     * @apiParam {Boolean} [hit=false] хиты продаж
     * @apiParam {String} [manufacturers=] производители, id через ,
     * @apiParam {Number} [price_min=0] минимальная цена
     * @apiParam {Number} [price_max=0] максимальная цена
     * @apiParam {String="asc","desc"} [order=asc] порядок сортировки
     * @apiParam {String="price","title"} [sort] сортировка
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": [
     *         "drugs": [
     *             {
     *                 "id": 171,
     *                 "title": "АД Минус тбл 40БАД",
     *                 "type": 1,
     *                 "default_price": 390.1,
     *                 "images": [
     *                     "/uploads/drugs/ta_171.jpg"
     *                 ],
     *                 "prices": {
     *                     "price": 213.70,
     *                     "discount": null,
     *                    "discount_is_percent": false
     *                },
     *                "drugstores": [
     *                    {
     *                        "id": 112696,
     *                        "in_stock": 1
     *                    },
     *                    ...
     *                ]
     *             },
     *             {
     *                "id": 172,
     *                "title": "АД Норма капс 60БАД",
     *                "images": [
     *                    "/uploads/drugs/ta_172.jpg"
     *                ],
     *                "prices": {},
     *                "drugstores": [
     *                    {
     *                        "id": 112798,
     *                         "in_stock": 1
     *                    },
     *                    ...
     *                ]
     *             },
     *              ...
     *         ],
     *         "suggest": "suggest"
     *      ]
     *}
     */

    /**
     * @param $categoryId
     * @param Request $request
     * @return DrugSearchCollection
     */
    public function search(Request $request)
    {

        $q = $request->q ?? false;
        $isVoiced = $request->isVoiced ?? false;
        if ($q !== false) {
            $this->searchQueryService->addOrIncrement($q, (bool)$isVoiced);
        }
        $drugs = $this->drugService->getDrugsFromSearch($request);
        if ($drugs->isEmpty()) {
            $suggest = (new Trigram)->searchSuggest($request->q);
            return (new DrugSearchCollection($drugs))->additional(['data' => [
                'suggest' => $suggest,
            ]]);
        }
        return (new DrugSearchCollection($drugs));
    }
}
