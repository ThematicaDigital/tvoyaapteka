<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\ActualResource;
use App\Http\Resources\V1\DrugCollection;
use App\Models\Actual;
use App\Services\DrugService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActualController extends Controller
{
    private $drugService;

    public function __construct(DrugService $drugService)
    {
        $this->drugService = $drugService;
    }
    /**
     * @api {get} api/actuals/ Список актуального
     * @apiName Actuals
     * @apiGroup Actuals
     * @apiUse AcceptHeader
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": [
     *        {
     *            "id": 2,
     *            "title": "От всего"
     *        },
     *        {
     *            "id": 1,
     *            "title": "От кашля"
     *        }
     *    ]
     *}
     */
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $actuals = Actual::orderBy('title')->get();
        return ActualResource::collection($actuals);
    }

    /**
     * @api {get} api/actuals/:actual_id/?city_id=:city_id Список товаров категории актуального, которые есть в наличии в городе
     * @apiName ActualDrugs
     * @apiGroup Actuals
     * @apiUse AcceptHeader
     * @apiUse OffsetLimit
     * @apiUse SuccessDrugCollection
     * @apiParam {Number} actual_id id категории актуального
     * @apiParam {Number} city_id id города
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": [
     *         {
     *             "id": 171,
     *             "title": "АД Минус тбл 40БАД",
     *             "type": 1,
     *             "default_price": 390.1,
     *             "images": [
     *                 "/uploads/drugs/ta_171.jpg"
     *             ],
     *             "prices": {
     *                 "price": 213.70,
     *                 "discount": null,
     *                "discount_is_percent": false
     *            },
     *            "drugstores": [
     *                {
     *                    "id": 112696,
     *                    "in_stock": 1
     *                },
     *                ...
     *            ]
     *        },
     *         {
     *            "id": 172,
     *            "title": "АД Норма капс 60БАД",
     *            "images": [
     *                "/uploads/drugs/ta_172.jpg"
     *            ],
     *            "prices": {},
     *            "drugstores": [
     *                {
     *                    "id": 112798,
     *                     "in_stock": 1
     *                },
     *                ...
     *            ]
     *        },
     *          ...
     *      ]
     *}
     */

    /**
     * @param $actualId
     * @param Request $request
     * @return DrugCollection
     */
    public function actual($actualId, Request $request)
    {
        $drugs = $this->drugService->getDrugsByActualIdForCity($actualId, $request->city_id,
            $request->offset ?? 0, $request->limit ?? 10);
        return new DrugCollection($drugs);
    }
}
