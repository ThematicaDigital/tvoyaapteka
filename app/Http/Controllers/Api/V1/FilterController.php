<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Drug;
use App\Services\ManufacturerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    private $manufacturerService;

    public function __construct(ManufacturerService $manufacturerService)
    {
        $this->manufacturerService = $manufacturerService;
    }
    /**
     * @api {get} api/filters/?city_id=:city_id[&category_id=:category_id] Список производителей и макс мин цена для фильтра
     * @apiName Filters
     * @apiGroup Filters
     * @apiUse AcceptHeader
     * @apiParam {Number} city_id id города
     * @apiParam {Number} category_id id категории для которой будет возвращен min-max price
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *    "data": {
     *        "price": {
     *            "max": "50182.30",
     *            "min": "0.20"
     *        },
     *        "manufacturers": [
     *            {
     *                "id": 4,
     *                "title": "Колгейт-Палмолив"
     *            },
     *            {
     *                "id": 10,
     *                "title": "Марбиофарм ОАО"
     *            },
     *            ...
     *        ]
     *    }
     *}
     */

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Drug::select('drugs.*')
            ->withPrice($request->city_id)
            ->inStockForCity($request->city_id);

        if ($request->category_id) {
            $query->withCategoryId($request->category_id);
        }

        if (!empty($request->q)) {
            $query->where('title', 'like', '%' . $request->q . '%');
        }

        $query->rememberCache(30);

        $minPrice = $query->min('price');
        $maxPrice = $query->max('price');

        $manufacturers = $this->manufacturerService->getManufacturersForInStockDrugsInCity($request);

        $data = [
            'data' => [
                'price' => [
                    'max' => $maxPrice,
                    'min' => $minPrice
                ],
                'manufacturers' => $manufacturers,
            ]
        ];
        return response()->json($data, 200);
    }

}
