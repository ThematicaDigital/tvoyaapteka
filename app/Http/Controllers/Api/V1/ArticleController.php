<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\ArticleCollection;
use App\Http\Resources\V1\ArticleResource;
use App\Models\Article;
use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }
    /**
     * @apiDefine SuccessArticle
     *
     * @apiSuccess (Success 200) {Object} data  Статья
     * @apiSuccess (Success 200) {Number} data.id    id статьи
     * @apiSuccess (Success 200) {String} data.title Заголовок статьи
     * @apiSuccess (Success 200) {String} data.created_at Дата создания статьи
     * @apiSuccess (Success 200) {String} data.announce Анонс статьи
     * @apiSuccess (Success 200) {String} data.content Содержание статьи
     * @apiSuccess (Success 200) {String[]} data.images Массив путей к изображениям
     */

    /**
     * @apiDefine OffsetLimit
     *
     * @apiParam {Number} [offset=0] пропустить элементов
     * @apiParam {Number} [limit=10] лимит по количеству элементов
     */

    /**
     * @api {get} api/articles Список статей
     * @apiName AllArticles
     * @apiGroup Articles
     * @apiUse AcceptHeader
     * @apiUse OffsetLimit
     * @apiUse SuccessArticle
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *  "data": [
     *      {
     *          "id": 1,
     *          "title": "Perferendis repellat eius cumque id.",
     *          "created_at": {
     *              "date": "2018-03-19 16:37:41.000000",
     *              "timezone_type": 3,
     *              "timezone": "Asia/Yakutsk"
     *          },
     *          "announce": "Sequi amet minima minima est magni. Omnis laborum non doloribus quod eligendi dolorum asperiores. Molestiae incidunt voluptas quidem dolores. Nemo voluptate quis eveniet.",
     *          "images": [
     *              "uploads/articles/52ba3c99df6d2420ed3e6ef5d1d66cd7.jpg"
     *          ]
     *      },
     *      ...
     *  ]
     *}
     */


    /**
     * @param Request $request
     * @return ArticleCollection
     */
    public function index(Request $request)
    {
        $articles = $this->articleService->getArticlesForApi($request->offset ?? 0, $request->limit ?? 10);
        return new ArticleCollection($articles);
    }


    /**
     * @api {get} api/articles/:id/?city_id=:city_id Статья
     * @apiName Article
     * @apiGroup Articles
     * @apiParam id id статьи
     * @apiParam city_id id города для которого будет подгружаться цена
     * @apiUse AcceptHeader
     * @apiUse SuccessArticle
     * @apiSuccess (Success 200) {Object[]} data.drugs Массив товаров привязанных к статье
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *     "data": {
     *         "id": 1,
     *         "title": "Totam repellat et voluptas labore qui perspiciatis.",
     *         "created_at": {
     *             "date": "2018-05-28 10:13:31.000000",
     *             "timezone_type": 3,
     *             "timezone": "Asia/Yakutsk"
     *         },
     *        "announce": "Tenetur molestiae qui corporis amet exercitationem eveniet architecto. Velit exercitationem ducimus natus deleniti. Nam repellat consequatur porro illum ea nemo animi iusto. Magnam cumque perferendis aut qui vel.",
     *         "content": "<p>Fugit facilis odio voluptatibus labore incidunt. Repudiandae ea animi expedita et non. Qui sapiente quia placeat labore. Molestiae dolor et non cum aliquid.</p>",
     *         "images": [
     *             "uploads/articles/640bc22b2a943a5b35a25e95c833ec0a.jpg"
     *         ],
     *         "drugs": [
     *             {
     *                 "id": 190,
     *                 "category_id": 22,
     *                "title": "Адреналин 0,1% амп 1мл 5",
     *                 "text": null,
     *                 "images": [
     *                     "/uploads/drugs/ta_190.jpg"
     *                 ],
     *                 "molecular_composition": "Эпинефрин",
     *                 "type": 1,
     *                 "default_price": 74.5,
     *                 "drugstores": [
     *                    {
     *                         "id": 1001,
     *                         "in_stock": 3
     *                     },
     *                      ...
     *                 ],
     *                "prices": {
     *                     "price": 73.8,
     *                     "discount": 0,
     *                     "discount_is_percent": false
     *                 }
     *             },
     *              ...
     *         ]
     *     }
     * }
     *
     *
     *
     *
     *
     *
     *
     *
     *
     */

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @param  Request $request
     * @return ArticleResource
     */
    public function show(Article $article, Request $request)
    {
        return new ArticleResource($article->load([
            'drugs' => function ($query) use ($request) {
                $query->select('drugs.*')
                    ->withDrugstores()
                    ->withPrice($request->city_id);
            },
        ]));
    }
}
