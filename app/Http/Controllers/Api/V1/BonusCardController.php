<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\BonusCardResource;
use App\Models\BonusCard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

class BonusCardController extends Controller
{
    /**
     * @api {get} api/user/bonus-card Получить бонусную карту с историей бонусов
     * @apiName UserBonusCardGet
     * @apiGroup User
     * @apiUse HeaderWithToken
     * @apiUse OffsetLimit
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *{
     *    "data": {
     *        "id": 1,
     *        "number": "123456",
     *        "amount": 205.85,
     *        "is_active": true,
     *        "activation_date": {
     *            "date": "2018-04-01 00:00:00.000000",
     *            "timezone_type": 3,
     *            "timezone": "Asia/Yakutsk"
     *        },
     *        "bonus_logs": [
     *            {
     *                "id": 5,
     *                "drugstore_id": 1001,
     *                "amount": -20,
     *                "created_at": {
     *                    "date": "2018-05-05 00:59:28.000000",
     *                    "timezone_type": 3,
     *                    "timezone": "Asia/Yakutsk"
     *                }
     *            },
     *              ...
     *        ]
     *    }
     *}
     * @apiErrorExample {json} Error 404:
     *  HTTP/1.1 404 Not Found
     *[]
     *
     */

    /**
     * @return BonusCardResource
     */
    public function show(Request $request)
    {
        try {
            $bonusCard = \Auth::user()->bonusCard()->with(['bonusLogs' => function ($query) use ($request) {
                $query->latest()->offset($request->offset ?? 0)
                    ->limit($request->limit ?? 3);
            }])->firstOrFail();
        } catch (\Throwable $e) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return new BonusCardResource($bonusCard);
    }

//    TODO: Дублирование с App\Http\Controllers\Exchange\BonusCardController

    /**
     * @api {post} api/user/bonus-card/:bonus_card Активировать бонусную карту
     * @apiName UserBonusCardCreate
     * @apiGroup User
     * @apiDescription Активировать бонусную карту
     * @apiParam {Number} bonus_card номер бонусной карты
     * @apiUse HeaderWithToken
     * @apiSuccess (Success 200) {String} data.message У пользователя уже есть бонусный счет, теперь он привязан к новой бонусной карте
     * @apiSuccess (Success 201) {String} data.message Карта успешно добавлена!
     * @apiError Error422 Пользователь с такой картой уже существует
     * @apiErrorExample {json} 409:
     *  HTTP/1.1 401 Unauthorized
     * {
     *     "message": "The given data was invalid.",
     *     "errors": {
     *         "card": [
     *             "Такое значение поля номер карты уже существует."
     *         ]
     *     }
     * }
     */
    public function create(Request $request, $cardNumber)
    {
        $validationFields = [
          'card' => $cardNumber,
        ];
        \Validator::make($validationFields, [
            'card' => 'required|integer|unique:bonus_cards,number|discount_card',
        ])->validate();

        $bonusCard = BonusCard::firstOrCreate(['number' => $cardNumber]);
        $user = \Auth::user();

        if (is_null($user->bonusCard)) {
            $bonusCard->user()->associate($user)->save();
            $data = [
                'data' => [
                    'message' => 'Карта успешно добавлена!',
                ]
            ];
            return response()->json($data, Response::HTTP_CREATED);
        }

        if ($bonusCard->wasRecentlyCreated) {
            $bonusCard->forceDelete();
        }
        $user->bonusCard->number = $cardNumber;
        $user->bonusCard->save();

        $data = [
            'data' => [
                'message' => 'У пользователя уже есть бонусный счет, теперь он привязан к новой бонусной карте',
            ]
        ];
        return response()->json($data, Response::HTTP_OK);
    }

}