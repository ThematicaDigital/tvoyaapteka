<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequestForApi;
use App\Models\Feedback;
use Illuminate\Http\Response;


class FeedbackController extends Controller
{
    /**
     * @api {post} api/feedback Обратная связь
     * @apiName feedback
     * @apiGroup feedback
     * @apiUse AcceptHeader
     * @apiParam {String}   phone Телефон
     * @apiParam {String}   text  Текст сообщения
     * @apiParam {String}   name  Имя пользователя
     * @apiParam {File}   file  Файл pdf,docx,doc,jpg,png,jpeg
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 201 CREATED
     *     []
     */

    /**
     * @param FeedbackRequestForApi $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FeedbackRequestForApi $request)
    {
        $fields = $request->except(['file']);
        if ($request->hasFile('file')) {
            $fields['file'] = 'uploads/' . $request->file->store('feedbacks', 'uploads');
        }
        Feedback::create($fields);

        return response()->json([], Response::HTTP_CREATED);
    }
}