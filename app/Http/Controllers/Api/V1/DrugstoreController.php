<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\DrugstoreResource;
use App\Http\Resources\V1\DrugstoreDrugResource;
use App\Http\Controllers\Controller;
use App\Models\Drugstore;
use App\Models\DrugDrugstore;
use Illuminate\Http\Request;

class DrugstoreController extends Controller
{
    /**
     * @api {get} api/drugstores/?city_id=:city_id Список аптек в городе
     * @apiName AllDrugstores
     * @apiGroup Drugstores
     * @apiUse AcceptHeader
     * @apiParam {Number} city_id id города
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *  "data": [
     *      {
     *           "id": 210564,
     *           "city_id": 8,
     *           "title": "Аптека КПП Здоровье",
     *           "address": "Ул. 50 лет Октября, 202",
     *           "latitude": "50.29839729",
     *           "longitude": "127.543361",
     *           "images": null,
     *           "freeday": {
     *               "id": 3,
     *               "start_datetime": {
     *                   "date": "2018-04-22 10:55:21.000000",
     *                   "timezone_type": 3,
     *                    "timezone": "Asia/Yakutsk"
     *                },
     *                "end_datetime": {
     *                    "date": "2018-04-23 10:55:21.000000",
     *                    "timezone_type": 3,
     *                    "timezone": "Asia/Yakutsk"
     *                },
     *                "freeday_status": "Ревизия"
     *            },
     *            "worktime": "с 8.00 до 22.00",
     *            "is_24h": false
     *       },
     *      ...
     *  ]
     *}
     */


    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $drugstores = Drugstore::where('city_id', $request->city_id)
            ->with(['freeday' => function ($query) {
                $query->active();
            }])
            ->orderBy('is_24h', 'DESC')
            ->orderBy('address')
            ->get();
        return DrugstoreResource::collection($drugstores);
    }


    /**
     * @api {get} api/drugstores/:id Аптека
     * @apiName Drugstore
     * @apiGroup Drugstores
     * @apiUse AcceptHeader
     * @apiParam {Number} id id аптеки
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *  "data":
     *      {
     *           "id": 210564,
     *           "city_id": 8,
     *           "title": "Аптека КПП Здоровье",
     *           "address": "Ул. 50 лет Октября, 202",
     *           "latitude": "50.29839729",
     *           "longitude": "127.543361",
     *           "images": null,
     *           "freeday": {
     *               "id": 3,
     *               "start_datetime": {
     *                   "date": "2018-04-22 10:55:21.000000",
     *                   "timezone_type": 3,
     *                    "timezone": "Asia/Yakutsk"
     *                },
     *                "end_datetime": {
     *                    "date": "2018-04-23 10:55:21.000000",
     *                    "timezone_type": 3,
     *                    "timezone": "Asia/Yakutsk"
     *                },
     *                "freeday_status": "Ревизия"
     *            },
     *            "worktime": "с 8.00 до 22.00",
     *            "is_24h": false
     *       }
     *}
     */

    /**
     * Display the Drugstore.
     *
     * @param  \App\Models\Drugstore  $drugstore
     * @return DrugstoreResource
     */
    public function show(Drugstore $drugstore)
    {
        return new DrugstoreResource($drugstore->load(['freeday' => function ($query) {
            $query->active();
        }]));
    }

    /**
     * @api {get} api/drugstores/:id/drugs Список остатков товаров в аптеке
     * @apiName DrugsInStock
     * @apiGroup Drugstores
     * @apiUse AcceptHeader
     * @apiParam {Number} id id аптеки
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *  "data": [
     *      {
     *           "id": 1,
     *           "in_stock": 2,
     *       },
     *      ...
     *  ]
     *}
     */

    /**
     * Display the drugs in stock for drugstore.
     *
     * @param  \App\DrugstoreDrugResource\Drugstore  $drugstore
     * @return DrugstoreResource
     */
    public function drugs(Drugstore $drugstore)
    {
        return DrugstoreDrugResource::collection(
            DrugDrugstore::where('drugstore_id', $drugstore->id)->where('in_stock', '>', 0)->get()
        );
    }
}
