<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\StoreDeviceTokenRequest;
use App\Models\DeviceToken;
use Illuminate\Http\Response;

class DeviceTokenController
{
    /**
     * @apiDefine SuccessDeviceToken
     * @apiSuccess (Success 204) 204 No Content
     */

    /**
     * @api {post} api/device-token Добавить токен устройства
     * @apiName PostDeviceToken
     * @apiGroup DeviceToken
     * @apiUse HeaderWithToken
     * @apiUse AcceptHeader
     * @apiUse SuccessDeviceToken
     * @apiParam {String{..200}} value Token string value
     * @apiParam {String="android","ios"} platform Platform name
     */

    /**
     * @param StoreDeviceTokenRequest $request
     * @param DeviceToken $model
     * @return mixed
     */
    public function store(StoreDeviceTokenRequest $request, DeviceToken $model)
    {
        $device_token = $model->where('value', $request->get('value'))
            ->where('platform', $request->get('platform'))
            ->first();

        if ($device_token) {

            if ($device_token->user_id != $request->user()->id) {
                $device_token->user_id = $request->user()->id;
                $device_token->save();
            }

        } else {
            $model->create(array_merge($request->validated(), [
                'user_id' => $request->user()->id
            ]));
        }

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}