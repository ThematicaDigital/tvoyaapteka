<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\OrderException;
use App\Http\Controllers\Controller;
use App\Http\Requests\MakeOrderRequest;
use App\Http\Resources\V1\OrderCollection;
use App\Http\Resources\V1\OrderResource;
use App\Models\Cart;
use App\Models\DrugDrugstore;
use App\Models\Drugstore;
use App\Models\Group;
use App\Models\Order;
use App\Services\CartService;
use App\Services\OrderService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    private $cartService;
    private $userService;
    private $orderService;

    public function __construct(CartService $cartService, UserService $userService, OrderService $orderService)
    {
        $this->cartService = $cartService;
        $this->userService = $userService;
        $this->orderService = $orderService;
    }
    /**
     * @apiDefine SuccessOrder
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *
     *{
     *     "data": [
     *        {
     *             "id": 1,
     *             "drugstore": {
     *                 "id": 1008,
     *                 "city_id": 8,
     *                 "title": "Твояаптека.рф",
     *                 "address": "Ул. Ленина, 87",
     *                 "latitude": "50.25652379",
     *                 "longitude": "127.5514545",
     *                 "images": null,
     *                 "worktime": "с 8.00 до 20.00",
     *                 "is_24h": false
     *             },
     *             "status": 5,
     *             "promocode": {
     *                 "code": "12345678"
     *             },
     *             "created_datetime": {
     *                "date": "2018-04-23 11:04:21.000000",
     *                 "timezone_type": 3,
     *                "timezone": "Asia/Yakutsk"
     *             },
     *            "pay_datetime": {
     *                 "date": "2018-04-23 12:04:21.000000",
     *                "timezone_type": 3,
     *                 "timezone": "Asia/Yakutsk"
     *            },
     *            "total_price": 15654.89,
     *            "total_price_with_promocode": 15654.89,
     *             "drugs": [
     *                {
     *                     "id": 17236,
     *                    "title": "Актрапид НМ Пенфилл 100МЕмл 3,0мл 5",
     *                     "images": null,
     *                    "type": 1,
     *                     "default_price": 925.7,
     *                    "prices": {
     *                         "price": 7103.44,
     *                        "discount": 3972.46,
     *                         "discount_is_percent": false
     *                    },
     *                     "count": 5
     *                }
     *             ]
     *        },
     *        ...
     *  ]
     *}
     */

    /**
     * @api {get} api/orders/?status=:status Список заказов пользователя
     * @apiName Orders
     * @apiGroup Orders
     * @apiUse HeaderWithToken
     * @apiUse OffsetLimit
     * @apiUse SuccessOrder
     * @apiParam {Number=1,2,3,4,5} status статус заказа 'IN_PROGRESS' => 1, 'APPROVED' => 2, 'DONE' => 3, 'CANCELED' => 4, 'PAID' => 5
     *
     */


    /**
     * @param Request $request
     * @return OrderCollection
     */
    public function index(Request $request)
    {
        $orders = \Auth::user()->orders()
            ->when($request->has('status'), function ($query) use ($request) {
                return $query->where('status', $request->status);
            })
            ->with(['drugstore', 'promocode'])
            ->latest()
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? 10)
            ->get();

        if ($orders->isEmpty()) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return new OrderCollection($orders);
    }

    /**
     * @api {post} api/orders/ Создать заказ из корзины
     * @apiName OrderCreate
     * @apiGroup Orders
     * @apiDescription Если в корзине присутствуют товары из разных аптек, то создается число заказов равное числу аптек
     * @apiParam {String} promocode Промокод к заказу
     * @apiUse HeaderWithToken
     * @apiUse SuccessOrder
     * @apiError Error403 Пустая корзина
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "cart": [
     *              "Пустая корзина"
     *          ]
     *     }
     * }
     * @apiError Error403 Условия промокода не выполнены
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Условия промокода не выполнены"
     *          ]
     *     }
     * }
     * @apiError Error403 Промокод уже был активирован этим пользователем
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Промокод уже был активирован этим пользователем"
     *          ]
     *     }
     * }
     * @apiError Error404 Промокод не найден
     * @apiErrorExample {json} 404:
     *  HTTP/1.1 404 NOT FOUND
     * {
     * "errors":
     *     {
     *         "promocode": [
     *              "Промокод не найден"
     *          ]
     *     }
     * }
     * @apiError Error422 ошибки при оформлении заказа
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *   {
     *      "errors": {
     *          "drugs": {
     *              "330": "Доступное количество товара на складе: 0"
     *          },
     *          "drugstores": {
     *              "1001": "Аптека закрыта по причине: ремонт",
     *              "1008": "Аптека закрыта по причине: ремонт"
     *          }
     *      }
     */

    /**
     * @param MakeOrderRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function add(MakeOrderRequest $request)
    {
//        TODO: ОСТОРОЖНО, ГОВНОКОД: Refactor this
        $user = \Auth::user();
        $orderVisible = true;
        if (is_null($user)) {
            if ($request->has('phone')) {
                $user = $this->userService->getOrCreateByPhone($request->phone, [
                    'password' => rand(10000000, 99999999),
                    'group_id' => Group::GROUP_ID['UNREG_USERS'],
                    'firstname' => $request->name,
                ]);
            }
            $cartItems = $this->cartService->getCartItemsForUnauthorizedUser(collect(session()->get('cart', [])), session()->get('city_id'));
            $orderVisible = false;
        }
        else {
            $cartItems = $this->cartService->getUsersCartItemsByCityId($user);
        }

        if ($cartItems->isEmpty()) {
            return response()->json(['errors' => ['cart' => 'Пустая корзина']], Response::HTTP_FORBIDDEN);
        }

        try {
            $orders = $this->orderService->createOrdersByCartItems($cartItems, $user, $orderVisible);
        } catch (OrderException $exception) {
            return response()->json(['errors' => $exception->getOrdersError()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        Cart::where('user_id', $user->id)->delete();
        session()->put('cart', []);

        return OrderResource::collection($orders);
    }


    /**
     * @api {post} api/orders/:order_id Отменить заказ
     * @apiName OrderCancel
     * @apiGroup Orders
     * @apiDescription Отменить заказ со статусом "в обработке"
     * @apiParam {Number} order_id id заказа
     * @apiUse HeaderWithToken
     * @apiError Error404 У пользователя нет заказа с таким ид и статусом "в обработке"
     */
    public function cancel(Request $request, $orderId)
    {
        $user = \Auth::user();

        if (is_null($user)) {
            $order = Order::where([
                ['visible', false],
                ['id', $orderId],
                ['status', Order::STATUS['IN_PROGRESS']]
            ])->firstOrFail();
        } else {
            $order = $user->orders()->with('drugs')->where([
                ['id', $orderId],
                ['status', Order::STATUS['IN_PROGRESS']]
            ])->firstOrFail();
        }
        $this->orderService->cancelOrder($order);
        return response()->json([]);
    }

    /**
     * @api {post} api/orders/:order_id/unauthorized-cancel Отменить заказ для неавторизованного пользователя
     * @apiName UnauthorizedOrderCancel
     * @apiGroup Orders
     * @apiDescription Отменить заказ со статусом "в обработке" неавторизованным пользователем
     * @apiParam {Number} order_id id заказа
     * @apiError Error404 У пользователя нет заказа с таким ид, статусом "в обработке"
     */
    public function cancelOrderByUnauthorizedUser(Request $request, $orderId)
    {
        $order = Order::where([
            ['visible', false],
            ['id', $orderId],
            ['status', Order::STATUS['IN_PROGRESS']]
        ])->firstOrFail();
        $this->orderService->cancelOrder($order);
        return response()->json([]);
    }

    /**
     * @api {get} api/orders/:order_id Просмотр заказа
     * @apiName OrderShow
     * @apiGroup Orders
     * @apiUse HeaderWithToken
     * @apiUse SuccessOrder
     * @apiParam {Number} order_id id заказа
     *
     */
    public function show(Request $request, $orderId)
    {
        $user = \Auth::user();
        $order = $user->orders()->with('drugs', 'drugstore')
            ->where('id', $orderId)
            ->firstOrFail();
        return new OrderResource($order);
    }


    /**
     * @api {post} api/orders/unauthorized-user Создать заказ от неавторизованного пользователя
     * @apiName UnauthorizedOrderCreate
     * @apiGroup Orders
     * @apiDescription Если присутствуют товары из разных аптек, то создается число заказов равное числу аптек
     * @apiParam {Object[]} cart
     * @apiParam {Number} cart.drug_id id товара
     * @apiParam {Number} cart.count количество товара
     * @apiParam {Number} cart.drugstore_id id аптеки
     * @apiParam {String} phone телефон пользователя
     * @apiParam {String} name имя пользователя
     * @apiUse SuccessOrder
     * @apiError Error403 Пустая корзина
     * @apiErrorExample {json} 403:
     *  HTTP/1.1 403 FORBIDDEN
     * {
     * "errors":
     *     {
     *         "cart": [
     *              "Пустая корзина"
     *          ]
     *     }
     * }
     * @apiError Error422 ошибки при оформлении заказаи ошибки валидации
     * @apiErrorExample {json} 422:
     *  HTTP/1.1 422 Unprocessable Entity
     *   {
     *      "errors": {
     *          "drugs": {
     *              "330": "Доступное количество товара на складе: 0"
     *          },
     *          "drugstores": {
     *              "1001": "Аптека закрыта по причине: ремонт",
     *              "1008": "Аптека закрыта по причине: ремонт"
     *          }
     *         "phone": [
     *             "Телефон должен содержать 10 цифр",
     *             "Неверный формат номера телефона"
     *         ]
     *      }
     */

    /**
     * @param MakeOrderRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */

    public function makeOrderByUnauthorizedUser(MakeOrderRequest $request)
    {
//        TODO: ОСТОРОЖНО, ГОВНОКОД: Refactor this
        $user = $this->userService->getOrCreateByPhone($request->phone, [
            'password' => rand(10000000, 99999999),
            'group_id' => Group::GROUP_ID['UNREG_USERS'],
            'firstname' => $request->name,
        ]);

        $cartItems = $this->cartService->getCartItemsForUnauthorizedUser(collect($request->cart));

        if ($cartItems->isEmpty()) {
            return response()->json(['errors' => ['cart' => 'Пустая корзина']], Response::HTTP_FORBIDDEN);
        }

        try {
            $orders = $this->orderService->createOrdersByCartItems($cartItems, $user, false);
        } catch (OrderException $exception) {
            return response()->json(['errors' => $exception->getOrdersError()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        Cart::where('user_id', $user->id)->delete();
        session()->put('cart', []);

        return OrderResource::collection($orders);
    }
}
