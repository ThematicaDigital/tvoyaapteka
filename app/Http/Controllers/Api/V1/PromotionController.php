<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\PromotionResource;
use App\Http\Controllers\Controller;
use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * @apiDefine SuccessPromotion
     *
     * @apiSuccess (Success 200) {Object} data  Акция
     * @apiSuccess (Success 200) {Number} data.id    id акции
     * @apiSuccess (Success 200) {String} data.title Заголовок акции
     * @apiSuccess (Success 200) {String} data.text Описание акции
     * @apiSuccess (Success 200) {String} data.start_datetime Дата начала акции
     * @apiSuccess (Success 200) {String} data.end_datetime Дата окончания акции
     * @apiSuccess (Success 200) {String[]} data.images Массив путей к изображениям
     * @apiSuccess (Success 200) {String} data.url Ссылка
     */

    /**
     * @api {get} api/promotions/?city_id=:city_id Список действующих акций для города
     * @apiName Promotions
     * @apiGroup Promotions
     * @apiUse AcceptHeader
     * @apiUse OffsetLimit
     * @apiUse SuccessPromotion
     * @apiParam {Number} city_id id города
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     *{
     *"data": [
     *        {
     *            "id": 2,
     *            "title": "Sunt et in dolor officiis maxime rerum blanditiis.",
     *            "text": "Occaecati laudantium quia est et possimus. Officiis minus nulla voluptatibus enim voluptatem nam. Et et cupiditate nam a.",
     *            "start_datetime": "2018-03-20 14:50:15",
     *            "end_datetime": "2018-05-14 00:50:15",
     *            "images": [
     *                "uploads/promotions/7fd02d25d1147bd5356f7066ae07fbe5.jpg"
     *            ],
     *            "url": "http://zuravlev.ru/omnis-et-laudantium-fugiat-praesentium-eveniet-dignissimos.html",
     *        },
     *          ...
     *    ]
     *
     *}
     */

    /**
     * Display a listing of the promotion for city.
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        //TODO: offset & limit в каждом запросе требующем пагинацию это плохо, надо что-то делать
        $promotions = Promotion::inCity($request->city_id)->active()
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? 10)
            ->orderBy('start_datetime')->get();
        return PromotionResource::collection($promotions);
    }


    /**
     * @api {get} api/promotions/:id/?city_id=:city_id Акция
     * @apiName Promotion
     * @apiGroup Promotions
     * @apiParam id id акции
     * @apiParam city_id id города для которого будет подгружаться цена
     * @apiUse AcceptHeader
     * @apiUse SuccessPromotion
     * @apiSuccess (Success 200) {String} data.drug товар из акции
     *
     * @apiSuccessExample {json} 200:
     *  HTTP/1.1 200 OK
     * {
     *     "data": {
     *         "id": 1,
     *         "title": "Nobis commodi ut tenetur suscipit quis incidunt.",
     *         "text": "Et sit provident rerum quos provident. Fugiat officia quo voluptates quia dolores. Pariatur libero et velit quas optio earum nostrum. Neque sed quis voluptatem est nesciunt ullam impedit minima.",
     *         "start_datetime": "2018-05-28 10:28:05",
     *         "end_datetime": "2018-06-19 08:28:05",
     *         "images": [
     *             "uploads/promotions/0b346408b08ff303ed3300f16c571b5a.jpg"
     *         ],
     *         "url": "http://karpov.com/magnam-repellat-numquam-commodi-rem-qui-sint",
     *         "city_id": 8,
     *         "drugs": [
     *             {
     *                 "id": 190,
     *                 "category_id": 22,
     *                 "title": "Адреналин 0,1% амп 1мл 5",
     *                 "text": null,
     *                 "images": [
     *                     "/uploads/drugs/ta_190.jpg"
     *                 ],
     *                 "molecular_composition": "Эпинефрин",
     *                 "type": 1,
     *                 "default_price": 74.5,
     *                 "drugstores": [
     *                     {
     *                         "id": 1001,
     *                         "in_stock": 3
     *                     },
     *                      ...
     *                 ],
     *                 "prices": {
     *                     "price": 73.8,
     *                     "discount": 0,
     *                     "discount_is_percent": false
     *                 }
     *             },
     *              ....
     *         ]
     *     }
     * }
     */

    /**
     * Display the specified promotion.
     *
     * @param  Promotion $promotion
     * @param  Request $request
     * @return PromotionResource
     */
    public function show(Promotion $promotion, Request $request)
    {
        return new PromotionResource($promotion->load([
            'drugs' => function ($query) use ($request) {
                $query->select('drugs.*')
                    ->withDrugstores()
                    ->withPrice($request->city_id);
            },
        ]));
    }
}
