<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Services\SeoService;

class PageController extends Controller
{
	/**
	 * View static page
	 *
     * @param string $slug
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function view($slug)
    {
        $page = Page::where('slug', $slug)->with([
            'drugs' => function ($query) {
                $query->select('drugs.*')->inStockForCity(session('city_id'))->withPrice()->with('manufacturer');
            }
        ])->firstOrFail();
        SeoService::setTagsForPage($page);
        return view('pages/view', compact('page'));
    }

    /**
     * Preview static page
     *
     * @param string $slug
	 *
	 * @return \Illuminate\Http\Response
     */
    public static function preview($slug)
    {
    	$page = Page::where('slug', $slug)->first();
        return view('pages/preview', compact('page'));
    }
}