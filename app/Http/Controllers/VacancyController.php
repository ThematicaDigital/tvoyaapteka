<?php

namespace App\Http\Controllers;

use App\Models\Vacancy;
use App\Services\SeoService;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::with('city')->get()
            ->sortBy('city.title')
            ->groupBy('city.title');
        SeoService::setMetaTags('Вакансии - Семейная аптека', 'семейная аптека вакансии', 'семейная аптека вакансии');
        return view('vacancies.index', compact('vacancies'));

    }
}
