<?php

namespace App\Http\ViewComposers;

use App\Models\Region;
use Illuminate\View\View;

class HeaderRegionsViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        try {
            $userCity = request()->session()->get('user_city', false);
        } catch(\Exception $e) {
            $userCity = !empty(env('APP_CITY')) ? env('APP_CITY') : env('DEFAULT_CITY', 'moskva');
        }

        $regions = Region::with('cities')
            ->orderBy('title')
            ->get();
        $view->with('regions', $regions);
        $view->with('userCity', $userCity);
    }
}