<?php

namespace App\Http\ViewComposers;

use App\Models\Drugstore;
use App\Models\City;
use Illuminate\View\View;

class HeaderDrugstoreViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $drugstore = 'false';
        try {
            if (request()->session()->has('drugstore_id')) {
                $drugstore = Drugstore::find(request()->session()->get('drugstore_id'))->toJson();
            }
        } catch (\Exception $e) {
            if (!empty($view->userCity)) {
                $appCity = !empty(env('APP_CITY')) ? env('APP_CITY') : env('DEFAULT_CITY', 'moskva');
                $city = City::where('slug', $appCity)->first();
                $view->city = $city;
            }
        }
        $view->with('drugstore', $drugstore);
    }
}