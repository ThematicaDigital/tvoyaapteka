<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\View\View;

class HeaderCatalogViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $categories = Category::whereHas('drugs')->orWhereNull('parent_id')->orderBy('_lft')->get()->toTree();
        $view->with('categories', $categories);
    }
}