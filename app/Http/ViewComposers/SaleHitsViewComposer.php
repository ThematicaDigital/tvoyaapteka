<?php

namespace App\Http\ViewComposers;

use App\Services\DrugService;
use Illuminate\View\View;

class SaleHitsViewComposer
{
    private $drugService;

    public function __construct(DrugService $drugService)
    {
        $this->drugService = $drugService;
    }
    /**
     * Bind data to the view.
     *
     * @todo Chosen of city
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $drugs = $this->drugService->getSaleHits(12);
        $view->with('drugs', $drugs);
    }
}