<?php

namespace App\Http\ViewComposers;

use App\Models\Article;
use Illuminate\View\View;

class LastArticlesViewComposer
{
    const LIMIT_LAST = 4;
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $articles = Article::latest()->limit(self::LIMIT_LAST)->get();
        $view->with('articles', $articles);
    }
}