<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class ConfirmCityViewComposer
{
    /**
     * Bind data to the view.
     *
     * @todo Chosen of city
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $userCity = request()->session()->get('user_city', false);
        $view->with('userCity', $userCity);
    }
}