<?php

namespace App\Http\ViewComposers;

use App\Models\Actual;
use Illuminate\View\View;

class ActualDrugViewComposer
{
    /**
     * Bind data to the view.
     *
     * @todo Chosen of city
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $actuals = Actual::with(['drugs' => function($query) {
                $cityId = session()->get('city_id', false);
                $query->select('drugs.*')
                    ->withPrice($cityId)
                    ->withDrugstores($cityId)
                    ->with('manufacturer')
                    ->limit(30);
            }])
            ->get();
        $actuals = $actuals->reject(function ($value, $key) {
            return $value->drugs->isEmpty();
        });
        $view->with('actuals', $actuals);
    }
}