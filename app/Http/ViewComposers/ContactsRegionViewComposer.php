<?php

namespace App\Http\ViewComposers;

use App\Models\Region;
use Illuminate\View\View;

class ContactsRegionViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {

        $regions = Region::with('cities')->orderBy('title')
            ->get();
        $view->with('regions', $regions);
    }
}