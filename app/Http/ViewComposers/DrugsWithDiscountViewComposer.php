<?php

namespace App\Http\ViewComposers;

use App\Models\Drug;
use Illuminate\View\View;

class DrugsWithDiscountViewComposer
{
    /**
     * Bind data to the view.
     *
     * @todo Chosen of city
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $cityId = session()->get('city_id', false);
        $drugs = Drug::select('drugs.*')
            ->onlyWithDiscount($cityId)
            ->withDrugstores($cityId)
            ->with('manufacturer')
            ->limit(15)
            ->get();
        $view->with('drugs', $drugs);
    }
}