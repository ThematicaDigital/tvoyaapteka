<?php

namespace App\Http\ViewComposers;

use App\Services\DrugService;
use Illuminate\View\View;

class IndividualsViewComposer
{
    private $drugService;

    public function __construct(DrugService $drugService)
    {
        $this->drugService = $drugService;
    }
    /**
     * Bind data to the view.
     *
     * @todo Chosen of city
     *
     * @param  View  $view
     * @return void
     */

    public function compose(View $view)
    {
        $route = request()->route();

        switch ($route->getName()) {
            case 'catalog.category':
                $recommendedDrugs = $this->drugService->getRecommendedDrugsForCategory(8, $route->parameter('category'));
                break;
//            case 'catalog.view':
//                $recommendedDrugs = $this->drugService->getSaleHits(8, $route->parameter('category'));
//                break;
            default:
                $recommendedDrugs = $this->drugService->getIndividualDrugs();
//            case '':
//                break;
        }
        $view->with('drugs', $recommendedDrugs);
    }
}