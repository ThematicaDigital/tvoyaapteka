<?php

namespace App\Http\ViewComposers;

use App\Models\Partner;
use Illuminate\View\View;

class FooterPartnersViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $partners = Partner::all()->random(4);
        $view->with('partners', $partners);
    }
}