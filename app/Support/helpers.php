<?php
if (!function_exists('keyboardLayoutCorrection')) {
    /**
     * return phone string as last 10 numbers
     *
     * @param  string $str
     * @return string
     */
    function keyboardLayoutCorrection(string $str)
    {

        $symbolMap = [
            "q"=>"й", "w"=>"ц", "e"=>"у", "r"=>"к", "t"=>"е", "y"=>"н", "u"=>"г", "i"=>"ш", "o"=>"щ", "p"=>"з", "["=>"х", "]"=>"ъ",
            "a"=>"ф", "s"=>"ы", "d"=>"в", "f"=>"а", "g"=>"п", "h"=>"р", "j"=>"о", "k"=>"л", "l"=>"д", ";"=>"ж", "'"=>"э",
            "z"=>"я", "x"=>"ч", "c"=>"с", "v"=>"м", "b"=>"и", "n"=>"т", "m"=>"ь", ","=>"б", "."=>"ю",
            "й"=>"q", "ц"=>"w", "у"=>"e", "к"=>"r", "е"=>"t", "н"=>"y", "г"=>"u", "ш"=>"i", "щ"=>"o", "з"=>"p", "х"=>"[", "ъ"=>"]",
            "ф"=>"a", "ы"=>"s", "в"=>"d", "а"=>"f", "п"=>"g", "р"=>"h", "о"=>"j", "л"=>"k", "д"=>"l", "ж"=>";", "э"=>"'",
            "я"=>"z", "ч"=>"x", "с"=>"c", "м"=>"v", "и"=>"b", "т"=>"n", "ь"=>"m", "б"=>",", "ю"=>".",
        ];
        return strtr(mb_strtolower($str), $symbolMap);
    }
}
if (!function_exists('clearPhone')) {
    /**
     * return phone string as last 10 numbers
     *
     * @param  string $phone
     * @return string
     */
    function clearPhone(string $phone = null)
    {
        if ($phone) {
            $clearedPhone = preg_replace("/[^\d]/", '', $phone);
            return substr($clearedPhone, -10);
        }
        return '';
    }
}
if (!function_exists('getAbsolutePath')) {
    /**
     * return absolute path
     *
     * @param  mixed $path
     * @return mixed
     */
    function getAbsolutePath($path)
    {
        if (is_null($path)) {
            return $path;
        }
        if (!is_array($path)) {
            return url($path);
        }

        $path = (array)$path;

        return array_map(function ($item) {
            return url($item);
        }, $path);
    }
}
if (!function_exists('getTotalCartPrice')) {
    /**
     * return absolute path
     *
     * @param  \Illuminate\Database\Eloquent\Collection $cartItems
     * @return mixed
     */
    function getTotalCartPrice($cartItems)
    {
        return $cartItems->reduce(function ($carry, $item) {
            return $carry + $item->totalPriceWithDiscount;
        });
    }
}

if (!function_exists('getTotalCartPriceGuest')) {
    /**
     * return absolute path
     *
     * @param  \Illuminate\Support\Collection $cartItems
     * @return mixed
     */
    function getTotalCartPriceGuest($cartItems)
    {
        return $cartItems->reduce(function ($carry, $item) {
            $priceWithDiscount = round($item->drug->default_price, 2);

            if (!empty($item->drug->cities->keyBy('id')[$item->drugstore->city_id])) {
                $itemPrices = $item->drug->cities->keyBy('id')[$item->drugstore->city_id]->price;
                $priceWithDiscount = $itemPrices->priceWithDiscount;
            }

            $totalPriceWithDiscount = round($item->count * $priceWithDiscount, 2);
            return $carry + $totalPriceWithDiscount;
        });
    }
}
