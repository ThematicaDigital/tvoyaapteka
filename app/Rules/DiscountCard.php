<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DiscountCard implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (mb_substr($value, 0, 5) == '55500') {
            $last = mb_substr($value, -1); // 13й символ
            $array = str_split(mb_substr($value, 0, 12)); // строка из 12 символов
            $odd = array_sum(array_filter($array, function($var) { return !($var & 1); }, ARRAY_FILTER_USE_KEY)); // сумма нечетных символов
            $even = array_sum(array_filter($array, function($var) { return $var & 1; }, ARRAY_FILTER_USE_KEY)); // сумма четных символов
            $control = $even*3+$odd;
            $control = 10 - ($control - ($control - $control%10)); // контрольное число
            if ($control == 10) $control = 0;
            if ($control == $last) { // проверка контрольного числа и 13го символа строки
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Невалидный номер дисконтной карты';
    }
}