<?php

namespace App\Exceptions;

use Exception;

class SmsException extends Exception
{
    public static function smsCodeWasSended()
    {
        return new static(
            "Код уже выслан"
        );
    }
}
