<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class OrderException extends Exception
{
    private $orderErrors = [];

    public function __construct($orderErrors, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->orderErrors = $orderErrors;
    }

    public function getOrdersError()
    {
        return $this->orderErrors;
    }
}
