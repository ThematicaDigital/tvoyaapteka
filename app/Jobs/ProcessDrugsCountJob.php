<?php

namespace App\Jobs;


use App\Models\Drugstore;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDrugsCountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $drugsCount;

    /**
     * Create a new job instance.
     * @param array $drugsCount
     */
    public function __construct(array $drugsCount)
    {
        $this->drugsCount = $drugsCount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $drugstore = Drugstore::findOrFail($this->drugsCount['drugstoreId']);
            $drugstore->drugs()->syncWithoutDetaching($this->drugsCount['count']);
        } catch (\Exception $e) {
            \Log::error('Exchange error: ' . self::class . ' ' . $e->getMessage());
        }
    }
}
