<?php

namespace App\Jobs;

use App\Models\Drug;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDrugPricesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productPrices;

    /**
     * Create a new job instance.
     * @param array $productPrices
     */
    public function __construct(array $productPrices)
    {
        $this->productPrices = $productPrices;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $drug = Drug::findOrFail($this->productPrices['drugId']);
            $drug->cities()->sync($this->productPrices['prices']);
        } catch (\Exception $e) {
            \Log::error('Exchange error: ' . self::class . ' ' . $e->getMessage());
        }
    }
}
