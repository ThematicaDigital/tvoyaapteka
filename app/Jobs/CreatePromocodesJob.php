<?php

namespace App\Jobs;

use App\Models\Drug;
use App\Models\Promocode;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreatePromocodesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $promocodesFields;

    /**
     * Create a new job instance.
     * @param array $promocodesFields
     */
    public function __construct(array $promocodesFields)
    {
        $this->promocodesFields = $promocodesFields;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Promocode::insert($this->promocodesFields);
        } catch (\Throwable $e) {
//            \Log::error('Generate promocode error: ' . $e->getMessage());
//
        }
    }
}
