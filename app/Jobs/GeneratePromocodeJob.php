<?php

namespace App\Jobs;

use App\Models\Promocode;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GeneratePromocodeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $generateCount;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emptyPromocodesCount = Promocode::isEmpty()->count();
        $neededPromocodesCount = (int)config('promocodes.generateCount');
        $generateCount = $neededPromocodesCount - $emptyPromocodesCount;

        $promocodes = [];

        if ($emptyPromocodesCount < $neededPromocodesCount) {

            while (count($promocodes) < $generateCount) {
                $count = $generateCount - count($promocodes);
                for ($i = 0; $i < $count; $i++) {
                    $title = random_int(10000000, 99999999);
                    $promocodes[$title] = [
                        'title' => $title,
                        'created_at' => Carbon::now()->toDateTimeString(),
                    ];
                }
            }

            $chunked = array_chunk($promocodes, 100);
            foreach ($chunked as $chunk) {
                CreatePromocodesJob::dispatch($chunk);
            }
            GeneratePromocodeJob::dispatch($generateCount);
        }
    }
}
