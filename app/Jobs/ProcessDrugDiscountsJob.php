<?php

namespace App\Jobs;

use App\Models\CityDrug;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDrugDiscountsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productDiscount;
    protected $drugId;

    /**
     * Create a new job instance.
     * @param int $drugId
     * @param array $productDiscount
     */
    public function __construct(int $drugId, array $productDiscount)
    {
        $this->productDiscount = $productDiscount;
        $this->drugId = $drugId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!is_null($this->productDiscount['start_at'])) {
            $this->productDiscount['start_at'] = (new Carbon($this->productDiscount['start_at']))->toDateTimeString();
        }
        if (!is_null($this->productDiscount['end_at'])) {
            $this->productDiscount['end_at'] = (new Carbon($this->productDiscount['end_at']))->toDateTimeString();
        }
        try {
            CityDrug::where('drug_id', $this->drugId)->update($this->productDiscount);
        } catch (\Exception $e) {
            \Log::error('Exchange error: ' . self::class . ' ' . $e->getMessage());
        }
    }
}
