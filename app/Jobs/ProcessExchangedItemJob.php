<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Intervention\Image\Facades\Image;

class ProcessExchangedItemJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $objectClassName;
    protected $itemFields;
    protected $publicPathToSaveImages;

    /**
     * Create a new job instance.
     * @param string $objectClassName
     * @param array $itemFields
     * @param string $publicPathToSaveImages
     */
    public function __construct(string $objectClassName, array $itemFields, string $publicPathToSaveImages)
    {
        $this->objectClassName = $objectClassName;
        $this->itemFields = $itemFields;
        $this->publicPathToSaveImages = $publicPathToSaveImages;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (isset($this->itemFields['images'])) {
            $imagesPaths = $this->getSavedImagesPaths($this->itemFields['images']);

            if (is_null($imagesPaths)) {
                unset($this->itemFields['images']);
            } else {
                $this->itemFields['images'] = $imagesPaths;
            }
        }

        if (array_key_exists('actual', $this->itemFields)) {
            $this->itemFields['too_buy'] = $this->itemFields['actual'];
            unset($this->itemFields['actual']);
        }
        if (array_key_exists('freeday', $this->itemFields)) {
            $this->freedayToFreedayId();
        }
        $id = $this->itemFields['id'];
        unset($this->itemFields['id']);

        try {
            $this->objectClassName::updateOrCreate(['id' => $id], $this->itemFields);
        } catch (\Exception $e) {
            \Log::error('Exchange error: ' . self::class . ' ' . $e->getMessage());
        }
    }

    protected function getSavedImagesPaths(array $imagesPaths)
    {
        $drugImages = collect([]);
        collect($imagesPaths)->each(function ($imageSourcePath) use ($drugImages) {
            try {
                $image = Image::make(storage_path('app/' . config('exchange.storagePath') . '/') . $imageSourcePath);
                $imageSavePath = public_path($this->publicPathToSaveImages) . "{$image->filename}.{$image->extension}";
                $image->save($imageSavePath)->destroy();
                $drugImages->push(str_replace(public_path('/'), '', $imageSavePath));
            } catch (\Exception $exception) {
                \Log::warning($exception->getMessage() . ' ' . 'image: ' . $imageSourcePath);
            }
        });
        return $drugImages->isNotEmpty() ? $drugImages->toArray() : null;
    }

    private function freedayToFreedayId()
    {
        list($start, $end, $status) = $this->itemFields['freeday'];
        $freedayStatus = \App\Models\FreedayStatus::firstOrCreate(['title' => $status]);
        $freeday = \App\Models\Freeday::firstOrCreate([
            'start_datetime' => (new Carbon($start))->toDateTimeString(),
            'end_datetime' => (new Carbon($end))->toDateTimeString(),
            'freeday_status_id' => $freedayStatus->id
        ]);

        $this->itemFields['freeday_id'] = $freeday->id;
        unset($this->itemFields['freeday']);
    }
}
