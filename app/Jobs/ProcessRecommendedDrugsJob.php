<?php

namespace App\Jobs;

use App\Models\Drug;
use App\Models\ForUse;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessRecommendedDrugsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fields;

    /**
     * Create a new job instance.
     * @param array $fields
     */
    public function __construct(array $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $drugs = $this->fields['drugs'];
            unset($this->fields['drugs']);
            $forUse = ForUse::firstOrCreate($this->fields);
            $forUse->drugs()->sync($drugs);
        } catch (\Exception $e) {
            \Log::error('Exchange error: ' . self::class . ' ' . $e->getMessage());
        }
    }
}
