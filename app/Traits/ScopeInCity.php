<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Session;

trait ScopeInCity
{
    /**
     * @param Builder $query
     * @param string $id
     * @return $this
     */
    public function scopeInCity(Builder $query)
    {
        if (Session::has('city_id')) {
            $city_id = Session::get('city_id');
            if ($city_id) {
                return $query->where(function($query) use ($city_id) {
                    $query->whereNull('city_id')->orWhere('city_id', $city_id);
                });
            }
        }
        return $query;
    }
}