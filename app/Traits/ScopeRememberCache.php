<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Watson\Rememberable\Rememberable;

trait ScopeRememberCache
{
    use Rememberable;
    /**
     * @param Builder $query
     * @param int $minutes
     * @param string $cacheDriver
     * @return mixed
     */
    public function scopeRememberCache(Builder $query, int $minutes, string $cacheDriver = null)
    {
        return $query->remember($minutes)
            ->cacheDriver($cacheDriver ?? config('cache.default'));
    }
}