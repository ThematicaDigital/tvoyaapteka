<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class FakerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $faker = \Faker\Factory::create('ru_RU');
        View::share(compact('faker'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
