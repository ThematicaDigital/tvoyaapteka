<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator)
        {
            $value = User::cryptPassword($value,substr(Auth::user()->password,0,8));
            if($value == Auth::user()->password){
                return true;
            }else{
                return false;
            }

        }, 'Старый пароль не совпадает');

        \Validator::extend('discount_card', function ($attribute, $value, $parameters, $validator)
        {
            if (mb_substr($value, 0, 5) == '55500') {
                $last = mb_substr($value, -1); // 13й символ
                $array = str_split(mb_substr($value, 0, 12)); // строка из 12 символов
                $odd = array_sum(array_filter($array, function($var) { return !($var & 1); }, ARRAY_FILTER_USE_KEY)); // сумма нечетных символов
                $even = array_sum(array_filter($array, function($var) { return $var & 1; }, ARRAY_FILTER_USE_KEY)); // сумма четных символов
                $control = $even*3+$odd;
                $control = 10 - ($control - ($control - $control%10)); // контрольное число
                if ($control == 10) $control = 0;
                if ($control == $last) { // проверка контрольного числа и 13го символа строки
                    return true;
                }
            }
            return false;
        }, 'Неверный формат номера карты');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        //
    }
}
