<?php

namespace App\Providers;

use App\Http\ViewComposers\ActualDrugViewComposer;
use App\Http\ViewComposers\ContactsRegionViewComposer;
use App\Http\ViewComposers\DrugsWithDiscountViewComposer;
use App\Http\ViewComposers\FooterPartnersViewComposer;
use App\Http\ViewComposers\HeaderCatalogViewComposer;
use App\Http\ViewComposers\HeaderRegionsViewComposer;
use App\Http\ViewComposers\HeaderDrugstoreViewComposer;
use App\Http\ViewComposers\LastArticlesViewComposer;
use App\Http\ViewComposers\SaleHitsViewComposer;
use App\Http\ViewComposers\IndividualsViewComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('drugs.actuals', ActualDrugViewComposer::class);
        View::composer('drugs.discounts', DrugsWithDiscountViewComposer::class);
        View::composer('articles.widget-last', LastArticlesViewComposer::class);
        View::composer('include.header-catalog', HeaderCatalogViewComposer::class);
        View::composer('include.header', HeaderRegionsViewComposer::class);
        View::composer('include.header', HeaderDrugstoreViewComposer::class);
        View::composer('partners.footer', FooterPartnersViewComposer::class);
        View::composer('contacts.contacts', ContactsRegionViewComposer::class);
        View::composer([
            'individuals.category',
            'individuals.profile',
        ], IndividualsViewComposer::class);
        View::composer([
            'drugs.hits',
            'individuals.404',
            'individuals.cart',
        ], SaleHitsViewComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
