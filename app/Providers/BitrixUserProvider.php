<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use \App\Models\User;

class BitrixUserProvider extends EloquentUserProvider
{
    /**
    * Validate a user against the given credentials.
    *
    * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
    * @param  array  $credentials
    * @return bool
    */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        // Password from the request
        $plain = $credentials['password'];

        // Hash value from the database
        $hashedValue = $user->getAuthPassword();

        // Calculate salt from the hash value
        $salt = substr($hashedValue, 0, 8);

        // Check hash value and password
        if ($hashedValue === User::cryptPassword($plain, $salt)) {
            return true;
        }

        return false;
    }
}
