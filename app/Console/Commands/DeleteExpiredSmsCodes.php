<?php

namespace App\Console\Commands;

use App\Models\SmsCode;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteExpiredSmsCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms-codes:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete expired sms codes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiredTime = Carbon::now()->subMinutes(config('smscodes.expired_time'))->toDateTimeString();
        SmsCode::where('created_at', '<', $expiredTime)->delete();
    }
}
