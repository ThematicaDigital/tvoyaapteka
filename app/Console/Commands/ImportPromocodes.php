<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Promocode;
use DB;

class ImportPromocodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:promocodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import promocodes from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * STATUS_ID:
     * N - Принят
     * P - Повтор
     * W - В работе
     * A - Одобрен
     * F - Заказ выкуплен
     *
     * @todo Promocode ID
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        Promocode::truncate();
        $limit = 0;
        while ($promocodes = DB::connection('tvoyaapteka')->select("
            SELECT
                c.ID,
                c.COUPON,
                c.TYPE,
                c.DATE_CREATE
            FROM b_sale_discount_coupon AS c
            ORDER BY c.ID ASC
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($promocodes) {
                DB::beginTransaction();
                foreach ($promocodes as $promocode) {
                    if ($promocode->TYPE == 2) {
                        $type = Promocode::TYPE['ONE_TIME'];
                    } else if ($promocode->TYPE == 4) {
                        $type = Promocode::TYPE['MANY_TIMES'];
                    }
                    Promocode::create([
                        'id' => $promocode->ID,
                        'title' => $promocode->COUPON,
                        'type' => $type,
                        'amount' => 2,
                        'is_percent' => true,
                        'created_at' => $promocode->DATE_CREATE,
                    ]);
                    echo $promocode->ID . "\n";
                }
                DB::commit();
            }
        }
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
