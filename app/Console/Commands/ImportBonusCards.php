<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\BonusCard;

class ImportBonusCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:bonus:cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import bonus cards from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BonusCard::truncate();
        $start = new \DateTime;
        $limit = 0;
        while ($cards = DB::connection('tvoyaapteka')->select("
            SELECT
                u.VALUE_ID,
                u.UF_BONUS_CARD,
                ua.CURRENT_BUDGET
            FROM b_uts_user AS u
            LEFT JOIN b_sale_user_account AS ua ON u.VALUE_ID = ua.USER_ID
            WHERE u.UF_BONUS_CARD IS NOT NULL
            AND CHAR_LENGTH(u.UF_BONUS_CARD) = 13
            AND ua.CURRENT_BUDGET IS NOT NULL
            AND ua.CURRENT_BUDGET <> ''
            ORDER BY u.VALUE_ID ASC
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($cards) {
                DB::beginTransaction();
                foreach ($cards as $card) {
                    BonusCard::create([
                        'id' => $card->VALUE_ID,
                        'user_id' => $card->VALUE_ID,
                        'number' => $card->UF_BONUS_CARD,
                        'amount' => $card->CURRENT_BUDGET
                    ]);
                    echo $card->VALUE_ID . "\n";
                }
                DB::commit();
            }
        }
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
