<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Trigram;

class SphinxBuildTrigrams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sphinx:trigrams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build trigrams for sphinx search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Build trigramms from sphinx dictionary
     *
     * @see http://sphinxsearch.com/docs/current/ref-indexer.html
     *
     * @example indexer INDEX --buildstops PATH/dict.txt 10000 --buildfreqs Build dictionary with frequence
     *
     * @return mixed
     */
    public function handle()
    {
        $handle = fopen(ENV('SPHINX_PATH_TO_DICTIONARY'), "r");
        if ($handle) {
            while (($string = fgets($handle, 4096)) !== false) {
                list($keyword, $frequence) = explode(' ', $string);
                Trigram::insert([
                    'keyword' => $keyword,
                    'trigrams' => Trigram::buildTrigrams($keyword),
                    'frequence' => $frequence
                ]);
            }
            fclose($handle);
        }
    }
}
