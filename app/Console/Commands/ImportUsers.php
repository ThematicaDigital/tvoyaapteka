<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\User;
use App\Models\Group;
use Illuminate\Support\Facades\Schema;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import users from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $limit = 0;
        while ($users = DB::connection('tvoyaapteka')->select("
            SELECT
                ID,
                NAME,
                LAST_NAME,
                SECOND_NAME,
                IF(RIGHT(EMAIL, 10) = '@mysite.ru' OR EMAIL = '', NULL, EMAIL) AS EMAIL,
                PASSWORD,
                DATE_REGISTER,
                LAST_LOGIN,
                IF(CHAR_LENGTH(LOGIN) = 11, SUBSTR(LOGIN, 2), SUBSTR(LOGIN, 3)) AS LOGIN,
                IF(CHAR_LENGTH(LOGIN) = 11, SUBSTR(LOGIN, 2), SUBSTR(LOGIN, 3)) AS phone
            FROM b_user
            WHERE ACTIVE = 'Y'
            AND ((CHAR_LENGTH(LOGIN) = 11 AND LEFT(LOGIN, 2) = '89') OR (CHAR_LENGTH(LOGIN) = 12 AND LEFT(LOGIN, 3) = '+79'))
            GROUP BY phone
            ORDER BY ID
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($users) {
                DB::beginTransaction();
                foreach ($users as $user) {
                    User::insert([
                        'id' => $user->ID,
                        'firstname' => !empty($user->NAME) ? $user->NAME : null,
                        'lastname' => !empty($user->LAST_NAME) ? $user->LAST_NAME : null,
                        'surname' => !empty($user->SECOND_NAME) ? $user->SECOND_NAME : null,
                        'phone' => $user->LOGIN,
                        'email' => $user->EMAIL ? $user->EMAIL : null,
                        'password' => $user->PASSWORD,
                        'group_id' => Group::GROUP_ID['CONFIRMED_USERS'],
                        'created_at' => $user->DATE_REGISTER,
                        'updated_at' => $user->LAST_LOGIN,
                    ]);
                    echo $user->ID . "\n";
                }
                DB::commit();
            }
        }
        Schema::enableForeignKeyConstraints();
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
