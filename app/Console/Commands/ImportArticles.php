<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Article;

class ImportArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import articles from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        $articles = DB::connection('tvoyaapteka')->select('
            SELECT
                e.ID,
                e.DATE_CREATE,
                e.NAME,
                e.PREVIEW_TEXT,
                e.DETAIL_TEXT,
                e.CODE,
                CONCAT_WS("/", "uploads", f.SUBDIR, f.FILE_NAME) AS image
            FROM b_iblock_element AS e
            INNER JOIN b_file AS f ON f.ID = e.DETAIL_PICTURE AND f.FILE_NAME IS NOT NULL AND f.SUBDIR IS NOT NULL
            WHERE e.IBLOCK_ID = 41 AND e.ACTIVE = "Y" AND (e.ACTIVE_FROM IS NULL OR e.ACTIVE_FROM < NOW()) AND (e.ACTIVE_TO IS NULL OR e.ACTIVE_TO > NOW())
        ');
        if ($articles) {
            Article::truncate();
            DB::beginTransaction();
            foreach ($articles as $article) {
                Article::create([
                    'id' => $article->ID,
                    'title' => $article->NAME,
                    'slug' => $article->CODE,
                    'announce' => $article->PREVIEW_TEXT,
                    'content' => $article->DETAIL_TEXT,
                    'images' => [$article->image],
                    'created_at' => $article->DATE_CREATE,
                    'updated_at' => $article->DATE_CREATE,
                ]);
                echo $article->ID . "\n";
            }
            DB::commit();
        }
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
