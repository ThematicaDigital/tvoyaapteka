<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VidalProduct;
use App\Models\VidalDocument;
use App\Models\VidalDocumentProduct;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Storage;

class VidalInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vidal:install {mdb}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Put vidal descriptions from .mdb file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Truncate descriptions table
        VidalProduct::truncate();
        VidalDocument::truncate();
        VidalDocumentProduct::truncate();

        foreach ([
            'Product' => 'vidal_Product',
            'Document' => 'vidal_Document',
            'Product_Document' => 'vidal_Document_Product'
        ] as $mdbTable => $mysqlTable) {
            echo "Export {$mdbTable} to {$mysqlTable} starting...\n";

            $filename = storage_path('app/' . md5(time()) . '.csv');

            // Export from mdb
            $command = sprintf(
                'MDB_JET3_CHARSET="UTF-8" MDBICONV="UTF-8" mdb-export -d"|" -H -Q -R@ %s %s > %s',
                $this->argument('mdb'),
                $mdbTable,
                $filename
            );
            $process = new Process($command);
            $process->run();
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            // MySql import
            \DB::connection()->getpdo()->exec(sprintf(
                "LOAD DATA LOCAL INFILE '%s' INTO TABLE %s CHARACTER SET utf8 FIELDS TERMINATED BY '|' LINES TERMINATED BY '@'",
                $filename,
                $mysqlTable
            ));

            unlink($filename);

            echo "Export {$mdbTable} to {$mysqlTable} successfully completed!\n";
        }

        echo "Export all data successfully completed!\n";
    }
}
