<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DrugOrder;
use App\Models\Order;
use DB;
use Illuminate\Support\Facades\Schema;

class ImportOrdersDrugs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:orders:drugs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import drug orders from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        Schema::disableForeignKeyConstraints();
        DrugOrder::truncate();
        $limit = 0;
        while ($drugs = DB::connection('tvoyaapteka')->select("
            SELECT
                b.ID,
                b.ORDER_ID,
                b.PRICE,
                b.DATE_INSERT,
                b.DATE_UPDATE,
                b.QUANTITY,
                b.PRODUCT_XML_ID
            FROM b_sale_basket AS b
            LEFT JOIN b_sale_order AS o ON o.ID = b.ORDER_ID
            LEFT JOIN b_catalog_store AS s ON o.STORE_ID = s.ID
            WHERE o.STORE_ID IS NOT NULL
            AND s.XML_ID IS NOT NULL
            AND b.ORDER_ID IS NOT NULL
            GROUP BY b.ID
            ORDER BY b.ID
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($drugs) {
                DB::beginTransaction();
                foreach ($drugs as $drug) {
                    if (Order::where('id', $drug->ORDER_ID)->first()) {
                        DrugOrder::create([
                            'id' => $drug->ID,
                            'order_id' => $drug->ORDER_ID,
                            'drug_id' => $drug->PRODUCT_XML_ID,
                            'count' => (int)$drug->QUANTITY,
                            'price' => $drug->PRICE,
                            'created_at' => $drug->DATE_INSERT,
                            'updated_at' => $drug->DATE_UPDATE,
                            'discount' => null,
                            'discount_is_percent' => false,
                        ]);
                    }
                    echo $drug->ID . "\n";
                }
                DB::commit();
            }
        }
        Schema::enableForeignKeyConstraints();
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
