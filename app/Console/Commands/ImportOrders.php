<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use DB;
use Illuminate\Support\Facades\Schema;

class ImportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import orders from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * STATUS_ID:
     * N - Принят
     * P - Повтор
     * W - В работе
     * A - Одобрен
     * F - Заказ выкуплен
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        Schema::disableForeignKeyConstraints();
        Order::truncate();
        $limit = 0;
        while ($orders = DB::connection('tvoyaapteka')->select("
            SELECT
                o.ID,
                s.XML_ID AS STORE_ID,
                o.PAYED,
                o.DATE_PAYED,
                o.CANCELED,
                o.DATE_CANCELED,
                o.STATUS_ID,
                o.DATE_STATUS,
                o.DATE_INSERT,
                o.DATE_UPDATE,
                o.USER_ID,
                dc.ID AS COUPON_ID
            FROM b_sale_order AS o
            LEFT JOIN b_catalog_store AS s ON o.STORE_ID = s.ID
            LEFT JOIN b_sale_order_coupons AS c ON o.ID = c.ORDER_ID
            LEFT JOIN b_sale_discount_coupon AS dc ON dc.COUPON = c.COUPON
            WHERE o.STORE_ID IS NOT NULL
            AND s.XML_ID IS NOT NULL
            GROUP BY o.ID
            ORDER BY o.ID
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($orders) {
                DB::beginTransaction();
                foreach ($orders as $order) {
                    $pay_datetime = null;
                    $updated_at = $order->DATE_UPDATE;
                    if ($order->PAYED == 'Y' || $order->STATUS_ID == 'F') {
                        $status = Order::STATUS['PAID'];
                        $pay_datetime = $order->DATE_PAYED;
                    } else if ($order->CANCELED == 'Y') {
                        $status = Order::STATUS['CANCELED'];
                        $updated_at = $order->DATE_CANCELED;
                    } elseif ($order->STATUS_ID == 'A') {
                        $status = Order::STATUS['DONE'];
                    } elseif (in_array($order->STATUS_ID, ['N', 'W'])) {
                        $status = Order::STATUS['IN_PROGRESS'];
                    }
                    Order::create([
                        'id' => $order->ID,
                        'drugstore_id' => $order->STORE_ID,
                        'status' => $status,
                        'promocode_id' => $order->COUPON_ID,
                        'paid_at' => $pay_datetime,
                        'created_at' => $order->DATE_INSERT,
                        'updated_at' => $updated_at,
                        'user_id' => $order->USER_ID
                    ]);
                    echo $order->ID . "\n";
                }
                DB::commit();
            }
        }
        Schema::enableForeignKeyConstraints();
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
