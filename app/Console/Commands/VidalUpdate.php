<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Description;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Storage;

class VidalUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vidal:update {mdb}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Append vidal descriptions from .mdb file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Export from mdb
        $command = sprintf('MDB_JET3_CHARSET="UTF-8" MDBICONV="UTF-8" mdb-export -d"|" -H -Q -R@ %s Product', $this->argument('mdb'));
        $process = new Process($command);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        // Html special chars
        $output = htmlspecialchars($process->getOutput());

        // Put file in storage
        $filename = md5(time()) . '.csv';
        Storage::put($filename, $output);

        // MySql import
        $query = sprintf("LOAD DATA LOCAL INFILE '%s' INTO TABLE descriptions CHARACTER SET utf8 FIELDS TERMINATED BY '|' LINES TERMINATED BY '@'", storage_path("app/{$filename}"));
        \DB::connection()->getpdo()->exec($query);

        // Clear
        Storage::delete($filename);

        echo "Success!\n";
    }
}
