<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\BonusLog;
use App\Models\Drugstore;

class ImportBonusLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:bonus:logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import bonus logs from the old side';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new \DateTime;
        $limit = 0;
        BonusLog::truncate();
        $drugstore = Drugstore::first();
        while ($logs = DB::connection('tvoyaapteka')->select("
            SELECT
                t.ID,
                t.USER_ID,
                t.TRANSACT_DATE,
                t.AMOUNT,
                t.DEBIT
            FROM b_sale_user_transact AS t
            ORDER BY t.ID ASC
            LIMIT {$limit}, 10000
        ")) {
            $limit += 10000;
            if ($logs) {
                DB::beginTransaction();
                foreach ($logs as $log) {
                    BonusLog::create([
                        'drugstore_id' => $drugstore->id,
                        'bonus_card_id' => $log->USER_ID,
                        'amount' => $log->DEBIT == 'Y' ? abs($log->AMOUNT) : -1 * abs($log->AMOUNT),
                        'created_at' => $log->TRANSACT_DATE
                    ]);
                    echo $log->ID . "\n";
                }
                DB::commit();
            }
        }
        $interval = $start->diff(new \DateTime);
        echo "\n" . $interval->format('%h') . " Hours " . $interval->format('%i') . " Minutes \n\n";
    }
}
