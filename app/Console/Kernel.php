<?php

namespace App\Console;

use App\Console\Commands\DeleteExpiredSmsCodes;
use App\Console\Commands\GeneratePromocodes;
use App\Jobs\GeneratePromocodeJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Run build dictionary
        $schedule->exec('/usr/bin/indexer ' . env('SPHINX_INDEX_DRUGS') . ' --buildstops ' . env('SPHINX_PATH_TO_DICTIONARY') . ' 100000 --buildfreqs')->dailyAt('00:00');

        // Run build trigrams
        $schedule->command('sphinx:trigrams')->dailyAt('01:00');

        // Run indexer all indexes
        $schedule->exec('/usr/bin/indexer --all --rotate')->dailyAt('02:00');

        // delete expired sms codes
        $schedule->command(DeleteExpiredSmsCodes::class)->everyMinute();

        // generate promocodes
//        $schedule->job(new GeneratePromocodeJob)->dailyAt('03:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
