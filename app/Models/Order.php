<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Order extends Model
{
    use CrudTrait;

    /**
     * @var array
     */
    const STATUS = ['IN_PROGRESS' => 1, 'APPROVED' => 2, 'DONE' => 3, 'CANCELED' => 4, 'PAID' => 5];
//    APPROVED NOT USED
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $fillable = [
        'id', 'drugstore_id', 'status', 'promocode_id', 'user_id', 'paid_at', 'visible',
    ];
    protected $dates = [
        'created_datetime',
        'pay_datetime',
    ];
    protected $casts = [
        'status' => 'integer',
    ];
    protected $appends = [
        'total_price',
        'total_price_with_promocode',
    ];

    /**
     * get user
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * get promocode
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promocode()
    {
        return $this->belongsTo(Promocode::class)->withTrashed();
    }

    /**
     * get drugstore
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drugstore()
    {
        return $this->belongsTo(Drugstore::class)->withTrashed();
    }

    /**
     * get drugs
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class)->using(DrugOrder::class)->withPivot('count', 'price', 'discount', 'discount_is_percent');
    }

    public function scopeIsVisible($query)
    {
        return $query->where('visible', true);
    }

    public function getTotalPriceAttribute()
    {
        $total = $this->drugs->reduce(function ($carry, $drug) {
            return $carry + $this->getTotalPriceForDrug($drug->pivot);
        });
        return round($total, 2);
    }

    public function getTotalPriceWithPromocodeAttribute()
    {
        if (is_null($this->promocode)) {
            return $this->totalPrice;
        }

        if ($this->promocode->drugs->isEmpty()) {
            return $this->promocode->getPriceWithPromocode($this->totalPrice);
        }

        $drugsPriceInPromocode = $this->getDrugsPriceInPromocode();
        if (is_null($drugsPriceInPromocode)) {
            return $this->totalPrice;
        }

        return round($this->totalPrice - $drugsPriceInPromocode + $this->promocode->getPriceWithPromocode($drugsPriceInPromocode), 2);
    }

    public function getDrugsPriceInPromocode()
    {
        $itemsInPromocode = $this->drugs->whereIn('id', $this->promocode->drugs->pluck('id'));
        return $itemsInPromocode->reduce(function ($carry, $item) {
            return $carry + $this->getPriceForDrug($item->pivot);
        });
    }

    private function getPriceForDrug(DrugOrder $drug)
    {
        $discount = $drug->discount;
        if ($drug->discount_is_percent) {
            $discount = $drug->price * $discount * 0.01;
        }
        return $drug->price - $discount;
    }
    private function getTotalPriceForDrug(DrugOrder $drug)
    {
        return $drug->count * $this->getPriceForDrug($drug);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
