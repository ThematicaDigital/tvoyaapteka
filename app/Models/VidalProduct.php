<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VidalProduct extends Model
{
    protected $table = 'vidal_Product';

    protected $guarded = ['ProductID'];

    public function vidalDocuments()
    {
        return $this->belongsToMany(
        	VidalDocument::class,
        	'vidal_Document_Product',
        	'vidal_product_id',
        	'vidal_document_id',
        	'ProductID',
        	'DocumentID'
    	);
    }
}