<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'firstname', 'lastname', 'surname', 'phone', 'email', 'password', 'is_notify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'is_notify' => 'boolean',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function favouriteDrugs()
    {
        return $this->belongsToMany(Drug::class);
    }

    public function cartDrugs()
    {
        return $this->belongsToMany(Drug::class, 'carts')->using(Cart::class)->withPivot(['drugstore_id', 'count'])->withTimestamps();
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->isVisible();
    }

    public function bonusCard()
    {
        return $this->hasOne(BonusCard::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Get DeviceToken has-many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deviceTokens()
    {
        return $this->hasMany(DeviceToken::class);
    }

    /**
     * return group slug
     *
     * @param  void
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname} {$this->surname}";
    }

    public function getIsAdminAttribute()
    {
        return $this->group_id === Group::GROUP_ID['ADMINS'];
    }

    public function getIsUnregisteredAttribute()
    {
        return $this->group_id === Group::GROUP_ID['UNREG_USERS'];
    }

    /**
     * set phone as last 10 numbers
     *
     * @param  string $phone
     * @return void
     */
    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = clearPhone($phone);
    }

    /**
     * set bcrypted password
     *
     * @param  string $password
     * @return void
     */
    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = self::cryptPassword($password);
    }

    /**
     * Crypt password as Bitrix
     *
     * @param string $password
     * @param boolean $salt
     *
     * @return string
     */
    public static function cryptPassword($password, $salt = null)
    {
        $salt = $salt ?? substr(md5(time()), 0, 8);
        return $salt . md5($salt . $password);
    }

    public function scopeGetByPhone($query, $phone)
    {
        return $this->where('phone', $phone)->firstOrFail();
    }
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function routeNotificationForSmscru()
    {
        return '+7' . $this->phone;
    }
}
