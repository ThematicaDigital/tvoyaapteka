<?php

namespace App\Models;

use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

class Article extends Model
{
    use CrudTrait, Sluggable, ScopeRememberCache;

    protected $guarded = [
        'clear_images', //for deleting images from backpack upload_multiple field
    ];

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'images' => 'json'
    ];

    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class);
    }

    /**
     * get category
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id', 'id');
    }

    /**
     * Get main image
     *
     * @return string
     */
    public function getMainImageAttribute()
    {
        return $this->images ? current($this->images) : '/images/no_photo.png';
    }

    /**
     * Get preview image
     *
     * @return string
     */
    public function getPreviewPicAttribute()
    {
        return $this->preview_image ? $this->preview_image : $this->main_image;
    }

    /**
     * Get created date formated
     *
     * @return string
     */
    public function getCreatedAttribute()
    {
        return Carbon::parse($this->created_at)->format('d.m.Y');
    }

    public function scopeSearch($query)
    {
        return $query->when(!empty(request()->get('q')), function($query) {
            return $query->where('title', 'LIKE', '%' . request()->get('q') . '%')
                ->orWHere('announce', 'LIKE', '%' . request()->get('q') . '%')
                ->orWHere('content', 'LIKE', '%' . request()->get('q') . '%');
        });
    }
}