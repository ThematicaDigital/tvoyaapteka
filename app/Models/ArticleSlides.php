<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ArticleSlides extends Model
{
    use CrudTrait;

    protected $table = 'article_slides';

    protected $appends = ['mobile_pic'];

    protected $fillable = [
        'image',
        'city_id',
        'order',
        'mobile_image',
        'url',
        'start_datetime',
        'end_datetime'
    ];

    /**
     * Get city
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function scopeInCity($query, $city_id = null)
    {
        return $query->whereNull('city_id')
            ->when(!is_null($city_id), function ($query) use ($city_id) {
                $query->orWhere('city_id', $city_id);
            });
    }

    /**
     * Get mobile image
     *
     * @return string
     */
    public function getMobilePicAttribute()
    {
        return $this->mobile_image ? $this->mobile_image : $this->image;
    }
}
