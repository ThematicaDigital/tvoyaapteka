<?php

namespace App\Models;

use App\Http\Controllers\FavoriteController;
use App\Traits\ScopeRememberCache;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drug extends Model
{
    use SoftDeletes;
    use CrudTrait, Sluggable, ScopeRememberCache;

    /**
     * @var array
     */
    const PRESCRIPTION = ['NO' => 1, 'NORMAL' => 2, 'HIGH' => 3];

    const FAVORITES_COOKIE = 'favorites';

    const FAVORITES_EXPIRES = 43200;
    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    //protected $table = 'drugs';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['image', 'url', 'is_favorite', 'title_admin'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'images' => 'json',
        'type' => 'integer',
        'default_price' => 'float',
    ];

    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class)->using(CityDrug::class)->as('price')->withPivot('price', 'discount', 'discount_is_percent', 'start_at', 'end_at');
    }

    public function forUses()
    {
        return $this->belongsToMany(ForUse::class);
    }

    /**
     * get drugs
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugstores()
    {
        return $this->belongsToMany(Drugstore::class)->using(DrugDrugstore::class)->where('in_stock', '>', 0)->withPivot('in_stock')->withTimestamps();
    }

    /**
     * get articles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    /**
     * get category
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * get vidal product
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function vidalProduct()
    {
        return $this->hasOne(VidalProduct::class, 'RegistrationNumber', 'registration_number')->with('vidalDocuments');
    }

    /**
     * get manufacturer
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    /**
     * Get main image src
     *
     * @return string
     */
    public function getImageAttribute()
    {
        if ($this->images) {
            return current($this->images);
        }
        return 'images/no_photo.png';
    }

    /**
     * Get main image src
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('catalog.view', [$this->category->slug, $this->slug], false);
    }

    /**
     * Get main image src
     *
     * @return string
     */
    public function getIsFavoriteAttribute()
    {
        $favouriteIds = unserialize(\Cookie::get(self::FAVORITES_COOKIE));
        if (!$favouriteIds) {
            return false;
        }
        return in_array($this->id, unserialize(\Cookie::get(self::FAVORITES_COOKIE)));
    }

    /**
     * Get main image src
     *
     * @return string
     */
    public function getTitleAdminAttribute()
    {
        return "ID {$this->id} {$this->title}";
    }

    /**
     * Analogs of drug
     *
     * @todo Analogs
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Drug $drug
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsAnalog($query, Drug $drug)
    {
        return $query->whereNotNull('molecular_composition')->where('molecular_composition', $drug->molecular_composition);
    }

    public function getAnalogsAttribute()
    {
        return self::getAnalogs($this);
    }

    public function scopeGetAnalogs($query, Drug $drug, $cityId = null, $limit = 4)
    {
        $drugs = Drug::select('drugs.*')
            ->withPrice($cityId)
            ->exclude($drug->id)
            ->isAnalog($drug)
            ->with('manufacturer')
            ->withDrugstores($cityId)
            ->limit($limit)
            ->get();
        return $drugs;
    }
    /**
     * Scope drugs with category and descendants
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $category_id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCategoryId($query, $category_id = null)
    {
        if ($category_id) {
            $categories = Category::whereDescendantOrSelf($category_id)->pluck('id');
            return $query->whereIn('category_id', $categories);
        }
        return $query;
    }

    /**
     * Hits of sales
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsHit($query)
    {
        return $query->where('drugs.is_hit', true);
    }

    /**
     * Scope is 24h
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIs24h($query)
    {
        return $query->whereHas('drugstores', function($query) {
            $query->where('drugstores.is_24h', true)->rememberCache(60);
        });
    }

    /**
     * Hits of sales
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCityId($query, $city_id)
    {
        return $query->with(['drugstores' => function($query) use ($city_id) {
            $query->where('drugstores.city_id', $city_id)->rememberCache(60);
        }]);
    }

    /**
     * Hits of sales
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithDrugstoreId($query, $drugstore_id = null)
    {
        if ($drugstore_id) {
            $query->where('drug_drugstore.drugstore_id', $drugstore_id);
        }
        return $query;
    }

    public function scopeIsTooBuy($query, Drug $drug)
    {
        return $query->where('drugs.id', '<>', $drug->id)->whereNotNull('too_buy')->where('too_buy', $drug->too_buy);
    }

    public function getIsTooBuyAttribute()
    {
        return self::getIsTooBuy($this);
    }

    public function scopeGetIsTooBuy($query, Drug $drug, $cityId = null, $limit = 4)
    {
        $drugs = Drug::select('drugs.*')
            ->withPrice($cityId)
            ->with('manufacturer')
            ->withDrugstores($cityId)
            ->exclude($drug->id)
            ->isTooBuy($drug)
            ->limit($limit)
            ->get();
        return $drugs;
    }


    public function scopeExclude($query, $id)
    {
        return $query->where('drugs.id', '<>', $id);
    }

    /**
     * Buys with drug
     *
     * @todo Buys
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $cityId
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithPrice($query, $cityId = null)
    {
        if (is_null($cityId)) {
            $cityId = session()->get('city_id', false);
        }
        if ($cityId) {
            return $query->join('city_drug', 'drugs.id', '=', 'city_drug.drug_id')
                ->where('city_drug.city_id', $cityId)
                ->addSelect(\DB::raw('
                    city_drug.price AS price, city_drug.discount_is_percent AS discount_is_percent,CASE
                        WHEN CURDATE() BETWEEN city_drug.start_at and city_drug.end_at THEN city_drug.discount
                        ELSE NULL
                    END as discount
                '));
        }
        return $query->addSelect(\DB::raw('default_price AS price, null AS discount, false AS discount_is_percent'));
    }


    public function scopeOnlyWithDiscount($query, $cityId = null)
    {
        if (is_null($cityId)) {
            $cityId = session()->get('city_id', false);
        }
        if ($cityId) {
            return $query->join('city_drug', 'drugs.id', '=', 'city_drug.drug_id')
                ->where('city_drug.city_id', $cityId)
                ->whereRaw('CURDATE() BETWEEN city_drug.start_at and city_drug.end_at')
                ->whereNotNull('city_drug.discount')
                ->where('city_drug.discount', '>', 0)
                ->addSelect(\DB::raw('
                    city_drug.price AS price,
                    city_drug.discount_is_percent AS discount_is_percent,
                    city_drug.discount as discount
                '));
        }
        return $query->addSelect(\DB::raw('default_price AS price, null AS discount, false AS discount_is_percent'));
    }

    public function scopeWithDrugstores($query, $cityId = null, $is24h = false)
    {
        return $query
            ->join('drug_drugstore', 'drugs.id', '=', 'drug_drugstore.drug_id')
            ->join('drugstores', 'drugstores.id', '=', 'drug_drugstore.drugstore_id')
            ->when(!is_null($cityId), function ($query) use ($cityId) {
                return $query->where('drugstores.city_id', $cityId);
            })
            ->when($is24h, function ($query) {
                return $query->where('drugstores.is_24h', true);
            })
            ->where('drug_drugstore.in_stock', '>', 0)
            ->whereNull('drugstores.deleted_at')
            ->with([
                'drugstores' => function ($query) use ($cityId, $is24h) {
                    return $query->when(!is_null($cityId), function ($query) use ($cityId) {
                        return $query->where('drugstores.city_id', $cityId);
                    })->when($is24h, function ($query) {
                        return $query->where('drugstores.is_24h', true);
                    })->withActiveFreeday()->whereNull('deleted_at')->rememberCache(60);
                }
            ])
            ->distinct();
    }

    public function scopeInStockForCity($query, $cityId = null)
    {
        return $query->whereHas('drugstores', function ($q) use ($cityId) {
            $q->when(!is_null($cityId), function ($q) use ($cityId) {
                $q->where('city_id', $cityId);
            })->rememberCache(60);
        });
    }
}
