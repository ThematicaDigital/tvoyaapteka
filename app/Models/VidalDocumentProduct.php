<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VidalDocumentProduct extends Model
{
    protected $table = 'vidal_Document_Product';
}