<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class BonusLog extends Model
{
    use CrudTrait;

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'amount' => 'float',
    ];

    protected $fillable = [
        'id', 'drugstore_id', 'bonus_card_id', 'amount'
    ];

    public function drugstore()
    {
        return $this->belongsTo(Drugstore::class);
    }
    public function bonusCard()
    {
        return $this->belongsTo(BonusCard::class);
    }

}