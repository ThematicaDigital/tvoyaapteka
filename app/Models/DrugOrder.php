<?php

namespace App\Models;


use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Relations\Pivot;

class DrugOrder extends Pivot
{
    use ScopeRememberCache;
    protected $fillable = [
        'id', 'order_id', 'drug_id', 'count', 'price', 'discount', 'discount_is_percent'
    ];

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'count' => 'integer',
        'price' => 'float',
        'discount' => 'float',
        'discount_is_percent' => 'boolean',
    ];
}
