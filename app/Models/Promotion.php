<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use App\Traits\ScopeInCity;

class Promotion extends Model
{
    use ScopeInCity;
    use CrudTrait;

    protected $guarded = [
        'id',
        'clear_images'
    ];

    /**
     * Appends attributes
     * @var array
     */
    protected $appends = ['image', 'diff'];

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'images' => 'json'
    ];

    /**
     * get City
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class);
    }

    /**
     * Get main image
     *
     * @return string
     */
    public function getImageAttribute()
    {
        return current($this->images);
    }

    /**
     * Get diff milliseconds
     *
     * @return string
     */
    public function getDiffAttribute()
    {
        if (is_null($this->end_datetime)) {
            return null;
        }
        return Carbon::parse($this->end_datetime)->diffInSeconds(Carbon::now()) * 1000;
    }

    public function scopeInCity($query, $city_id = null)
    {
        return $query->whereNull('city_id')
            ->when(!is_null($city_id), function ($query) use ($city_id) {
                $query->orWhere('city_id', $city_id);
            });
    }

    public function scopeActive($query)
    {
        return $query->where('end_datetime', '>', Carbon::now()->toDateTimeString())->orWhere('end_datetime', null);
    }
}
