<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * @var array
     */
    const GROUP_ID = ['ADMINS' => 1, 'USERS' => 2, 'CONFIRMED_USERS' => 3, 'UNREG_USERS' => 4];
}
