<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Freeday extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];

    protected $dates = [
        'start_datetime',
        'end_datetime'
    ];
    protected $casts = [
        'start_datetime' => 'datetime:d.m.Y',
        'end_datetime' => 'datetime:d.m.Y',
    ];

    public function freedayStatus()
    {
        return $this->belongsTo(\App\Models\FreedayStatus::class);
    }

    public function scopeActive($query)
    {
        return $query->whereDate('end_datetime', '>=', Carbon::now()->toDateString())
            ->whereDate('start_datetime', '<=', Carbon::now()->toDateString())
            ->orWhereDate('start_datetime', '=', Carbon::now()->addDay()->toDateString());
    }

    public function getAdminTitleAttribute()
    {
        return 'c ' . $this->start_datetime . ' по ' . $this->end_datetime . ' - ' . $this->freedayStatus->title;
    }
}
