<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Slide extends Model
{
    use CrudTrait;

    /**
     * @var array
     */
    const POSITION = ['TOP' => 1, 'BOTTOM' => 2];

    protected $guarded = ['id'];

    protected $appends = ['mobile_pic'];
    
    /**
     * Get drug
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }

    /**
     * Get city
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function scopeInCity($query, $city_id = null)
    {
        return $query->whereNull('city_id')
            ->when(!is_null($city_id), function ($query) use ($city_id) {
                $query->orWhere('city_id', $city_id);
            });
    }

    public function scopeWithDrug($query, $city_id = null)
    {
        return $query->with([
            'drug' => function ($q) use ($city_id) {
                $q->select('drugs.*')
                    ->withDrugstores($city_id)
                    ->withPrice($city_id)->rememberCache(60);
            }
        ]);
    }

    /**
     * Get mobile image
     *
     * @return string
     */
    public function getMobilePicAttribute()
    {
        return $this->mobile_image ? $this->mobile_image : $this->image;
    }
}
