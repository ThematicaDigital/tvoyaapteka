<?php

namespace App\Models;

use App\Exceptions\PromocodeException;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocode extends Model
{
    use SoftDeletes, CrudTrait;
    protected $guarded = ['id',];
    /**
     * @var array
     */
    const TYPE = ['ONE_TIME' => 1, 'MANY_TIMES' => 2, 'ONE_TIME_FOR_USER' => 3];
    const AMOUNT_TYPE = ['PERCENT' => 1, 'DISCOUNT' => 2, 'FIXED_PRICE' => 3];

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class);
    }

    public function getCartPriceWithPromocode(Collection $cartItems)
    {

        if (!$this->validateByCart($cartItems)) {
            throw new PromocodeException('Условия промокода не выполнены');
        }

        $totalCartPrice = getTotalCartPrice($cartItems);

        if ($this->drugs->isEmpty()) {
            return $this->getPriceWithPromocode($totalCartPrice);
        }

        $priceDrugsInPromocode = $this->getDrugsPriceInPromocode($cartItems);

        return round($totalCartPrice - $priceDrugsInPromocode + $this->getPriceWithPromocode($priceDrugsInPromocode), 2);
    }

    public function validateByCart(Collection $cartItems)
    {
        if ($this->drugs->isEmpty()) {
            if ($this->amount_type == self::AMOUNT_TYPE['FIXED_PRICE']) {
                return false;
            }
            return true;
        }

        $diffPromocodeDrugs = $this->drugs->pluck('id')->diff($cartItems->pluck('drug_id'));
        if ($diffPromocodeDrugs->isNotEmpty()) {
            return false;
        }

        if ($this->getCartItemsInPromocode($cartItems)->groupBy('drugstore_id')->count() > 1) {
            return false;
        }
        return true;
    }

    public function validateByUser(User $user)
    {
        if ($this->type == Promocode::TYPE['ONE_TIME_FOR_USER']) {
            $order = Order::where('promocode_id', $this->id)->where('user_id', $user->id)->first();
            return is_null($order);
        }
        return true;
    }

    private function getCartItemsInPromocode(Collection $cartItems)
    {
        return $cartItems->whereIn('drug_id', $this->drugs->pluck('id'));
    }

    public function getDrugsPriceInPromocode($cartItems)
    {
        return $this->getCartItemsInPromocode($cartItems)->reduce(function ($carry, $item) {
            return $carry + $item->priceWithDiscount;
        });
    }

    public function getPriceWithPromocode($price)
    {
        $discount = $this->amount;

        switch ($this->amount_type) {
            case self::AMOUNT_TYPE['PERCENT'] :
                $discount = $price * $discount * 0.01;
                break;
            case self::AMOUNT_TYPE['FIXED_PRICE'] :
                $price = $discount;
                $discount = 0;
                break;
        }

        return round($price - $discount, 2);
    }

    public function scopeIsEmpty($query)
    {
        return $query->whereNull('amount');
    }
}
