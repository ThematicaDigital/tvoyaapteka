<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class BonusCard extends Model
{
    use CrudTrait;

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected $dates = [
        'activation_date',
    ];

    protected $fillable = [
        'id', 'user_id', 'number'
    ];

    protected $appends = ['is_active'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bonusLogs()
    {
        return $this->hasMany(BonusLog::class);
    }

    public function getIsActiveAttribute()
    {
        return is_null($this->deleted_at);
    }

}