<?php

namespace App\Models;

use App\Traits\ScopeRememberCache;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use CrudTrait, ScopeRememberCache;
    use Sluggable, NodeTrait {
        NodeTrait::replicate insteadof Sluggable;
    }

    /**
     * Appends attributes
     * @var array
     */
    protected $appends = [
        'seoTextFull'
    ];

    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'categorys';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
     protected $guarded = [];

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function drugs()
    {
        $cityId = session('city_id');
        return $this->hasMany(Drug::class)->inStockForCity($cityId);
    }

    /**
     * Get seo text full
     *
     * @return string
     */ 
    public function getSeoTextFullAttribute()
    {
        $city = City::where('id', session('city_id'))->first();
        $this->seo_text = str_replace('#название_категории#', $this->title, $this->seo_text);
        $this->seo_text = str_replace('#название_города#', $city->title, $this->seo_text);
        $this->seo_text = str_replace('#название_города_ПП#', $city->title_seo_form, $this->seo_text);
        return $this->seo_text;
    }
}
