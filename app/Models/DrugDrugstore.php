<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class DrugDrugstore extends Pivot
{
    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'in_stock' => 'integer',
    ];

    public function getUpdatedAtColumn()
    {
        if ($this->pivotParent) {
            return $this->pivotParent->getUpdatedAtColumn();
        }

        return static::UPDATED_AT;
    }

    public function getCreatedAtColumn()
    {
        if ($this->pivotParent) {
            return $this->pivotParent->getCreatedAtColumn();
        }

        return static::CREATED_AT;
    }
}
