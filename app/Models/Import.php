<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Import extends Model
{
    use CrudTrait;

    protected $fillable = ['import_id', 'progress'];
}
