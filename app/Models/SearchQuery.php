<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class SearchQuery extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];
//    protected $dateFormat = 'd.m.Y';

    public function scopeIsVoiced(Builder $query)
    {
        return $query->where('isVoiced', true);
    }
}
