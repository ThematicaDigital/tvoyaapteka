<?php

namespace App\Models;

use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drugstore extends Model
{
    use CrudTrait, ScopeRememberCache, SoftDeletes;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'drugstores';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
     protected $guarded = [];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
     protected $appends = ['main_image'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'images' => 'json',
        'is_24h' => 'boolean',
    ];

    /**
     * get drugs
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class)->using(DrugDrugstore::class)->withPivot('in_stock')->withTimestamps();
    }

    /**
     * Get city
     * @return [type] [description]
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get freeday
     * @return [type] [description]
     */
    public function freeday()
    {
        return $this->belongsTo(Freeday::class);
    }

    /**
     * Scope drustores with city by id
     * @param  [type] $query [description]
     * @param  [type] $slug  [description]
     * @return [type]        [description]
     */
    public function scopeWithCityId($query, $city_id)
    {
        if ($city_id) {
            return $query->where('city_id', $city_id);
        }
        return $query;
    }

    /**
     * Scope sort drugstores by title
     * @param  [type] $query [description]
     * @param  [type] $slug  [description]
     * @return [type]        [description]
     */
    public function scopeSortTitle($query)
    {
        return $query->orderBy('title');
    }

    public function scopeWithActiveFreeday($query)
    {
        return $query
            ->with(['freeday' => function ($q) {
                $q->active()->with('freedayStatus');
            }]);
    }

    /**
     * Get main image
     *
     * @return string
     */
    public function getMainImageAttribute()
    {
        return $this->images ? current($this->images) : '/images/no_photo.png';
    }

    /**
     * Get work time as text
     *
     * @return string|boolean
     */
    public function getWorktimeTextAttribute()
    {
        if ($this->is_24h && !$this->worktime) {
            return 'Круглосуточно';
        } else {
            return $this->worktime;
        }
        return false;
    }
}