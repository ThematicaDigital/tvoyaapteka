<?php

namespace App\Models;

use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategory extends Model
{
    use SoftDeletes, CrudTrait, Sluggable, ScopeRememberCache;

    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $table = 'article_categories';
    protected $fillable = [
        'category_id',
        'title',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    /**
     * Get articles
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'id', 'category_id');
    }
}
