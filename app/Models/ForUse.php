<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class ForUse extends Model
{
    use CrudTrait;

    protected $guarded = [];

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class);
    }
    public function parent()
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }
}
