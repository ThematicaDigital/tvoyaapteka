<?php

namespace App\Models;


use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Cart extends Pivot
{
    use ScopeRememberCache;

    protected $table = 'carts';

    protected $appends = ['price_with_discount', 'total_price_with_discount'];

    public function drugstore()
    {
        return $this->belongsTo(Drugstore::class);
    }

    public function drug()
    {
        return $this->belongsTo(Drug::class);
    }

    public function getPriceWithDiscountAttribute()
    {
        if (!empty($this->drug->cities->keyBy('id')[$this->drugstore->city_id])) {
            $itemPrices = $this->drug->cities->keyBy('id')[$this->drugstore->city_id]->price;
            return $itemPrices->priceWithDiscount;
        }

        return round($this->drug->default_price, 2);

    }

    public function getTotalPriceWithDiscountAttribute()
    {
        return round($this->count * $this->priceWithDiscount, 2);
    }
}
