<?php

namespace App\Models;

use App\Traits\ScopeRememberCache;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use CrudTrait, Sluggable, ScopeRememberCache, SoftDeletes;
     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'citys';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
     protected $guarded = [];
    // protected $hidden = [];
    // protected $dates = [];


    /**
     * Sluggable configuration
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function vacancies()
    {
        return $this->hasMany(Vacancy::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class)->orWhere('city_id', null);
    }

    /**
     * Get drugs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function drugs()
    {
        return $this->belongsToMany(Drug::class);
    }
}
