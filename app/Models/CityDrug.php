<?php

namespace App\Models;


use App\Traits\ScopeRememberCache;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CityDrug extends Pivot
{
    use ScopeRememberCache;
    /**
     * Casting
     * @var array
     */
    protected $casts = [
        'price' => 'float',
        'discount' => 'float',
        'discount_is_percent' => 'boolean',
    ];

    protected $dates = ['start_at', 'end_at',];

    public function getPriceWithDiscountAttribute()
    {

        $discountStartAt = $this->start_at ?? now()->subDay();
        $discountEndAt = $this->end_at ?? now()->addDay();
        $discount = 0;
        if (now()->between($discountStartAt, $discountEndAt)) {
            $discount = $this->discount;
        }
        if ($this->discount_is_percent) {
            $discount = $this->price * $discount * 0.01;
        }
        return round($this->price - $discount, 2);

    }
}
