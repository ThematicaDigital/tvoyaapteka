<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use sngrl\SphinxSearch\SphinxSearch;
use App\Models\Drug;

class Trigram extends Model
{
	/**
	 * Build trigrams from keyword
	 *
	 * @param string $keyword Keyword for building
	 *
	 * @return string Trigrams from keyword
	 */
    public static function buildTrigrams($keyword)
    {
        $keyword = "__{$keyword}__";
        for ($i = 0; $i < mb_strlen($keyword, 'UTF-8') - 2; $i++) {
            $trigrams[] = mb_substr($keyword, $i, 3, 'UTF-8');
        }
        return implode(' ', $trigrams);
    }

    /**
     * Search suggest with typo
     *
     * @param string $query
     *
     * @return string|boolean
     */
    public function searchSuggest($query)
    {
        $length = strlen($query);

        $sphinx = new SphinxSearch();
        $results = $sphinx->search(Trigram::buildTrigrams($query), env('SPHINX_INDEX_TRIGRAMS'))
            ->setSelect("*, @weight + 2 - abs(len - {$length}) AS rank")
            ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_ANY)
            ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "rank DESC, frequence DESC")
            ->setRankingMode(\Sphinx\SphinxClient::SPH_RANK_WORDCOUNT)
            ->range('len', $length - 2, $length + 2)
            ->get();

        $suggest = false;

        if ($results && $results['total'] > 0) {
            foreach ($results['matches'] as $result) {
                if ($result['attrs']['keyword'] !== $query) {
                    $suggest = $result['attrs']['keyword'];
                    break;
                }
            }
        }

        return $suggest;
    }

    /**
     * Search suggests for autocomplete
     *
     * @param string $query Word for query
     * @param integer $count Limit for results
     * @param integer $skip Offset for results
     *
     * @return array|boolean
     */
    public function searchSuggests($query, $count, $skip = 0)
    {
        $sphinx = new SphinxSearch();

        $results = $sphinx->search($query, env('SPHINX_INDEX_DRUGS'))
            ->limit($count, $skip)
            ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
            ->setFieldWeights([
                'title' => 1000,
                'molecular_composition' => 80,
                'mva' => 60,
                'manufacturer_id' => 40,
            ])
            ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@weight DESC")
            ->setRankingMode(\Sphinx\SphinxClient::SPH_RANK_EXPR, 'sum((hit_count+(lcs-1)*max_lcs)*user_weight*100/min_hit_pos)')
            ->filter('city_id', [(int)(session()->get('city_id', false) ?? 8)])
            ->get();

        return $results;
    }

    public function search($request)
    {
        $sphinx = new SphinxSearch();

        $query = $sphinx->search($request->q, env('SPHINX_INDEX_DRUGS'))
            ->limit($request->limit ?? 10, $request->offset ?? 0)
            ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
            ->setFieldWeights([
                'title' => 1000,
                'molecular_composition' => 80,
                'mva' => 60,
                'manufacturer_id' => 40,
            ])
            ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@weight DESC")
            ->setRankingMode(\Sphinx\SphinxClient::SPH_RANK_EXPR, 'sum((hit_count+(lcs-1)*max_lcs)*user_weight*100/min_hit_pos)');

        if (!empty($request->hit)) {
            $query->filter('is_hit', [true]);
        }

        if (!empty($request->sales)) {
            $query->range('discount', 1, 9999999);
        }

        if (!empty($request->city_id)) {
            $query->filter('city_id', [(int)$request->city_id]);
        }

        if (!empty($request->manufacturers)) {
            $query->filter('manufacturer_id', explode(',', $request->manufacturers));
        }

        if (!empty($request->price_min) && !empty($request->price_max)) {
            $query->range('price', $request->price_min, $request->price_max);
        } else if (!empty($request->price_min)) {
            $query->range('price', $request->price_min, 9999999);
        } else if (!empty($request->price_max)) {
            $query->range('price', 0, $request->price_max);
        }

        return $query->get();
    }
}