<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VidalDocument extends Model
{
    protected $table = 'vidal_Document';

    protected $guarded = ['DocumentID'];

    public function vidalProducts()
    {
        return $this->belongsToMany(VidalProduct::class, 'vidal_Document_Product', 'vidal_document_id', 'vidal_product_id');
    }
}