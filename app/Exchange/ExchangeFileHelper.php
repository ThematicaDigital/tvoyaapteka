<?php

namespace App\Exchange;

use Illuminate\Support\Facades\Storage;

class ExchangeFileHelper
{

    /**
     * @param string $path
     */
    public function unzipAll($path)
    {
        $zip = new \ZipArchive();

        foreach (Storage::allFiles($path) as $file) {

            if (pathinfo($file, PATHINFO_EXTENSION) == 'zip') {

                $res = $zip->open(storage_path('app/' . $file));

                if ($res === true) {

                    $zip->extractTo(storage_path('app/' . $path));
                    $zip->close();

                    Storage::delete($file);

                }

            }

        }
    }
}