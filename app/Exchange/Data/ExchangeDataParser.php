<?php

namespace App\Exchange\Data;

use JsonStreamingParser\Listener;
use App\Exchange\Parser\Parser;

/**
 * Service Class for json parsing
 * with help JsonStreamingParser
 */
class ExchangeDataParser
{
	/**
	 * File for parse
	 * @var string
	 */
	protected $file;

	/**
	 * Json parsing listener
	 * @var Listener
	 */
	protected $jsonListener;

	/**
	 * Json streaming parser
	 * @var Parser
	 */
	protected $parser;

	/**
	 * file stream
	 * @var resource
	 */
	protected $stream;

	protected $id;

	/**
	 * Constructor
	 * @param string $file
	 * @param Listener $jsonListener
	 */
	public function __construct($file, Listener $jsonListener, $id)
	{
		$this->file = $file;
        $this->jsonListener = $jsonListener;
        $this->id = $id;
	}

    public function getListenerClass()
    {
        return get_class($this->jsonListener);
    }
	/**
	 * Parsing
	 * @return void
     * @throws \Exception
	 */
	public function parse()
	{
        try {
            $this->openStream();
            $this->initializeParser();
            $this->parser->parse();
        } catch (\Exception $e) {
            throw $e;
        } finally {
            $this->closeStream();
        }
	}

    /**
     * open file stream
     * @return void
     */
    private function openStream()
    {
        $this->stream = fopen($this->file, 'r');
    }

	/**
	 * Initialization parser
	 * @return void
	 */
	protected function initializeParser()
	{
        $this->parser = new Parser($this->stream, $this->jsonListener, "\n", false, 8192, $this->id);
	}

    /**
     * close file stream
     * @return void
     */
	private function closeStream()
    {
         fclose($this->stream);
    }
}
