<?php

namespace App\Exchange\Listeners;


use App\Jobs\ProcessRecommendedDrugsJob;

class ForusesListener extends BaseListener
{
    protected $mapKeyToModel = [
        'recommended_categories' => \App\Models\ForUse::class,
    ];

    public function endObject()
    {
        if (is_array($this->currentItem)) {
            ProcessRecommendedDrugsJob::dispatch($this->currentItem);
        }
        $this->currentItem = null;
    }
}
