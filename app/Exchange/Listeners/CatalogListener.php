<?php

namespace App\Exchange\Listeners;

use App\Models\Drug;

class CatalogListener extends BaseListener
{
    protected $mapKeyToModel = [
        'categories' => \App\Models\Category::class,
        'products' => \App\Models\Drug::class,
        'manufacturers' => \App\Models\Manufacturer::class
    ];

    protected $publicPathToSaveImages = 'uploads/drugs/';

    protected function insertValueToCurrentItem($value)
    {
        if ($this->currentKey === 'type' && $this->currentObjectClassName === Drug::class) {
            $value = Drug::PRESCRIPTION[$value];
        }
        $this->currentItem[$this->currentKey] = $value;
    }
}
