<?php

namespace App\Exchange\Listeners;


use App\Jobs\NotifyPromotionsSubscribersJob;
use App\Jobs\ProcessDrugDiscountsJob;

class DiscountListener extends BaseListener
{
    protected $mapKeyToModel = [
        'discounts' => \App\Models\CityDrug::class,
    ];

    public function endObject()
    {
        if (is_array($this->currentItem)) {
            $this->dispatchProcessDiscount();
        }
        $this->currentItem = null;
    }

    private function dispatchProcessDiscount()
    {
        $discount = $this->currentItem;
        unset($discount['product_id']);
        ProcessDrugDiscountsJob::dispatch($this->currentItem['product_id'], $discount);
    }
    public function endDocument()
    {
        NotifyPromotionsSubscribersJob::dispatch();
    }
}
