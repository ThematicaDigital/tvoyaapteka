<?php

namespace App\Exchange\Listeners;


use App\Jobs\ProcessDrugPricesJob;

class OffersListener extends BaseListener
{
    protected $mapKeyToModel = [
        'offers' => \App\Models\Drug::class,
    ];
    private $prevProductId;
    private $productPrices;

    public function endObject()
    {
        if (is_array($this->currentItem)) {
            $this->addPriceOrDispatchProcessDrugPrices();
        }
        $this->currentItem = null;
    }

    public function endArray()
    {
        $offer = $this->currentItem;
        unset($offer['product_id']);
        $productPrices = [
            'drugId' => $this->prevProductId,
            'prices' => $this->productPrices
        ];
        $this->productPrices = [$this->currentItem['city_id'] => $offer];
        ProcessDrugPricesJob::dispatch($productPrices);
    }
    private function addPriceOrDispatchProcessDrugPrices()
    {
        $offer = $this->currentItem;
        unset($offer['product_id']);
        if ($this->prevProductId === $this->currentItem['product_id'] || is_null($this->prevProductId)) {
            $this->productPrices[$this->currentItem['city_id']] = $offer;
        } else {
            $productPrices = [
                'drugId' => $this->prevProductId,
                'prices' => $this->productPrices
            ];
            $this->productPrices = [$this->currentItem['city_id'] => $offer];
            ProcessDrugPricesJob::dispatch($productPrices);
        }
        $this->prevProductId = $this->currentItem['product_id'];
    }
}
