<?php

namespace App\Exchange\Listeners;


use JsonStreamingParser\Listener;

abstract class BaseListener implements Listener
{
    protected $mapKeyToModel = [];
    protected $currentObjectClassName;
    protected $currentItem;
    protected $currentKey;
    protected $arrayInCurrentItem;
    protected $insertItemJobClassName = \App\Jobs\ProcessExchangedItemJob::class;
    protected $publicPathToSaveImages = 'uploads/';

    public function startDocument()
    {

    }

    public function startObject()
    {
        if ( !is_null($this->currentObjectClassName) ){
            $this->currentItem = [];
        }
    }

    public function endObject()
    {
        if (is_array($this->currentItem)) {
            $this->insertItemJobClassName::dispatch($this->currentObjectClassName, $this->currentItem, $this->publicPathToSaveImages)->onQueue('exchange');
        }
        $this->currentItem = null;
    }

    public function startArray()
    {
        if (is_array($this->currentItem)) {
            $this->arrayInCurrentItem = [];
        }
    }

    public function endArray()
    {
        if (is_array($this->currentItem)) {
            $this->insertArrayToCurrentItem();
        }
        $this->arrayInCurrentItem = null;
    }

    public function key($key)
    {
        if ( array_key_exists($key, $this->mapKeyToModel) ){
            $this->currentObjectClassName = $this->mapKeyToModel[$key];
        }else{
            $this->currentKey = $key;
        }
    }

    public function value($value)
    {
        if (is_null($this->arrayInCurrentItem)) {
            $this->insertValueToCurrentItem($value);
        }
        else {
            $this->arrayInCurrentItem[] = $value;
        }
    }

    public function endDocument()
    {
        // TODO: Implement endDocument() method.
    }

    /**
     * @param string $whitespace
     */
    public function whitespace($whitespace)
    {
        // TODO: Implement whitespace() method.
    }

    protected function insertArrayToCurrentItem()
    {
        $this->currentItem[$this->currentKey] = $this->arrayInCurrentItem;
    }

    protected function insertValueToCurrentItem($value)
    {
        $this->currentItem[$this->currentKey] = $value;
    }
}