<?php

namespace App\Exchange\Listeners;


use App\Jobs\ProcessDrugsCountJob;

class InStockListener extends BaseListener
{
    const DRUGS_COUNT_LIMIT = 100;
    protected $mapKeyToModel = [
        'in_stocks' => \App\Models\Drugstore::class,
    ];
    private $prevDrugstoreId;
    private $drugsCount = [];

    public function endObject()
    {
        if (is_array($this->currentItem)) {
            $this->addCountOfDrugOrDispatchProcessDrugCount();
        }
        $this->currentItem = null;
    }

    public function endArray()
    {
        $this->setDrugsCountAndDispatch();
    }

    private function addCountOfDrugOrDispatchProcessDrugCount()
    {
        if ($this->isAvailableAddingToDrugsCount()) {
            $this->drugsCount[$this->currentItem['product_id']] = ['in_stock' => $this->currentItem['count']];
        } else {
            $this->setDrugsCountAndDispatch();
        }
        $this->prevDrugstoreId = $this->currentItem['drugstore_id'];
    }

    private function isAvailableAddingToDrugsCount()
    {
        return (($this->prevDrugstoreId === $this->currentItem['drugstore_id'] || is_null($this->prevDrugstoreId))
            && count($this->drugsCount) < self::DRUGS_COUNT_LIMIT);
    }

    private function setDrugsCountAndDispatch()
    {
        $drugsCount = [
            'drugstoreId' => $this->prevDrugstoreId,
            'count' => $this->drugsCount
        ];
        $this->drugsCount = [$this->currentItem['product_id'] => ['in_stock' => $this->currentItem['count']]];
        ProcessDrugsCountJob::dispatch($drugsCount);
    }
}
