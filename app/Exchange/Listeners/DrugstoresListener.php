<?php

namespace App\Exchange\Listeners;


class DrugstoresListener extends BaseListener
{
    protected $mapKeyToModel = [
        'regions' => \App\Models\Region::class,
        'cities' => \App\Models\City::class,
        'drugstores' => \App\Models\Drugstore::class
    ];

    protected $publicPathToSaveImages = 'uploads/drugstores/';

}
