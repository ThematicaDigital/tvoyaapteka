<?php

namespace App\Services;

use App\Models\Manufacturer;
use Illuminate\Http\Request;

class ManufacturerService
{
    public function getManufacturersForInStockDrugsInCity(Request $request)
    {
        $query = Manufacturer::select('manufacturers.id', 'manufacturers.title')
            ->join('drugs', function ($join) {
                $join->on('drugs.manufacturer_id', '=', 'manufacturers.id');
            })
            ->join('drug_drugstore', function ($join) {
                $join->on('drugs.id', '=', 'drug_drugstore.drug_id')
                    ->on('drug_drugstore.in_stock', '>', \DB::raw(0));
            })
            ->join('drugstores', function ($join) use ($request) {
                $join->on('drugstores.id', '=', 'drug_drugstore.drugstore_id')
                    ->on('drugstores.city_id', '=', \DB::raw($request->city_id));
            });
        if ($request->has('manufacturer') && $request->manufacturer != '') {
            $query->where('manufacturers.title', 'LIKE', '%' . $request->manufacturer . '%')->limit(10);
        }
        return $query
            ->groupBy('manufacturers.id')
            ->orderBy('manufacturers.title')
            ->rememberCache(60)
            ->get()
            ->toArray();
    }
}
