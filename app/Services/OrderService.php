<?php

namespace App\Services;

use App\Exceptions\OrderException;
use App\Models\DrugDrugstore;
use App\Models\Drugstore;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class OrderService
{

    /**
     * @param Collection $cartItems
     * @param User $user
     * @param bool $orderVisible
     * @return Collection|\Tightenco\Collect\Support\Collection
     * @throws OrderException
     */
    public function createOrdersByCartItems(Collection $cartItems, User $user, $orderVisible = true)
    {
        $ordersFields = collect();
        $drugDrugstoreFromAllOrders = collect();
        $errors = [];

        $cartItems = $cartItems->groupBy('drugstore_id');
        foreach ($cartItems as $drugstoreId => $items) {
            $drugs = $items->pluck('drug_id')->toArray();
            $items = $items->keyBy('drug_id');
            $drugDrugstore = DrugDrugstore::where('drugstore_id', $drugstoreId)->whereIn('drug_id', $drugs)->get();

            $drugDrugstore = $drugDrugstore->map(function ($item) use ($items, &$errors) {
                if ($item->in_stock < $items[$item->drug_id]->count) {
                    $errors['drugs'][$item->drug_id] = 'Доступное количество товара на складе: ' . $item->in_stock;
                }
                $item->in_stock = $item->in_stock - $items[$item->drug_id]->count;
                return $item;
            });
            if ($drugDrugstore->isEmpty()) {
                foreach ($drugs as $drug) {
                    $errors['drugs'][$drug] = 'Доступное количество товара на складе: 0';
                }
            }
            $orderFields = [
                'drugstore_id' => $drugstoreId,
                'user_id' => $user->id,
                'status' => Order::STATUS['IN_PROGRESS'],
                'visible' => $orderVisible,
                'pivot' => $this->createFieldsForDrugOrderPivot($items),
            ];

            $ordersFields->push($orderFields);
            $drugDrugstoreFromAllOrders->push($drugDrugstore);
        }

        $drugstores = Drugstore::whereIn('id', $cartItems->keys())->whereHas('freeday', function ($query) {
            $now = Carbon::now()->toDateTimeString();
            $query->where('start_datetime', '<=', $now)->where('end_datetime', '>=', $now);
        })
            ->with('freeday.freedayStatus')
            ->get();

        foreach ($drugstores as $drugstore) {
            $errors['drugstores'][$drugstore->id] = 'Аптека закрыта по причине: ' . $drugstore->freeday->freedayStatus->title;
        }

        if (!empty($errors)) {
            throw new OrderException($errors, 'Возникли ошибки при попытке создания заказа');
        }

        $drugDrugstoreFromAllOrders->flatten()->map(function ($item) {
            $item->save();
        });

        $orders = $ordersFields->map(function ($order) {
            $pivotFields = $order['pivot'];
            unset($order['pivot']);
            $createdOrder = Order::create($order);
            $createdOrder->drugs()->attach($pivotFields);
            $createdOrder->load('drugstore');
            return $createdOrder;
        });

        return $orders;
    }

    /**
     * @param Collection $items
     * @return array
     */
    private function createFieldsForDrugOrderPivot(Collection $items)
    {
        $pivotFields = $items->mapWithKeys(function ($item) {
            if (!empty($item->drug->cities->keyBy('id')[$item->drugstore->city_id])) {
                $itemPrices = $item->drug->cities->keyBy('id')[$item->drugstore->city_id]->price;
                return [
                    $item->drug_id => [
                        'count' => $item->count,
                        'price' => $itemPrices->price,
                        'discount' => $itemPrices->discount,
                        'discount_is_percent' => $itemPrices->discount_is_percent,
                    ]
                ];
            }
            return [
                $item->drug_id => [
                    'count' => $item->count,
                    'price' => $item->drug->default_price,
                    'discount' => 0,
                    'discount_is_percent' => false
                ]
            ];
        })->toArray();
        return $pivotFields;
    }

    public function cancelOrder(Order $order)
    {
        $drugsIds = $order->drugs->pluck('id')->toArray();
        $orderDrugs = $order->drugs->keyBy('id');

        $drugDrugstore = DrugDrugstore::where('drugstore_id', $order->drugstore_id)->whereIn('drug_id', $drugsIds)->get();
        $drugDrugstore->map(function ($item) use ($orderDrugs) {
            $item->in_stock = $item->in_stock + $orderDrugs[$item->drug_id]->pivot->count;
            $item->save();
        });
        $order->status = Order::STATUS['CANCELED'];
        $order->save();
    }
}
