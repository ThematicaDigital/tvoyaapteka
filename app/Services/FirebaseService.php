<?php

namespace App\Services;


use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Response;

use Kreait\Firebase;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\MessageToRegistrationToken;
use Kreait\Firebase\Messaging\Notification;

class FirebaseService
{
    private $firebase;
    private $topic;

    public function __construct(Firebase $firebase)
    {
        $this->firebase = $firebase->getMessaging();
        $this->topic = 'promotions';
    }

    public function subscribeOrUnsubscribePromotions(User $user)
    {
        if ($user->deviceTokens->isEmpty()) {
            return null;
        }

        $tokens = $user->deviceTokens->pluck('value')->toArray();
        if ($user->is_notify) {
            $this->firebase->subscribeToTopic($this->topic, $tokens);
        } else {
            $this->firebase->unsubscribeFromTopic($this->topic, $tokens);
        }
    }

    public function notifyPromotionsSubscribers()
    {
        $notification = Notification::create(\Setting::get('promotion_notification_title'), \Setting::get('promotion_notification_text'));
        $message = CloudMessage::withTarget('topic', $this->topic)
            ->withNotification($notification)
            ->withData([
                'type' => 'PROMOTIONS',
            ]);
        $this->send($message);
    }

    public function notifyChangeOrderStatus(Order $order)
    {
        $statuses = [1 => 'В работе', 2 => 'Принят',  3 => 'Выполнен', 4 => 'Отменен', 5 => 'Оплачен'];
        $order->user->deviceTokens->map(function ($item) use ($order, $statuses) {
            $notification = Notification::create('Изменен статус заказа', "Изменен статус заказа {$order->id} на {$statuses[$order->status]}");
            $message = CloudMessage::withTarget('token', $item->value)
                ->withNotification($notification)
                ->withData([
                    'type' => 'ORDER_STATUS',
                    'order_id' => strval($order->id)
                ]);
            $this->send($message);
        });
    }

    private function send(CloudMessage $message)
    {
        try {
            $this->firebase->send($message);
        } catch (\Exception $e) {
            \Log::error('Firebase error: ' . $e->getMessage());
        }
    }
}
