<?php

namespace App\Services;


use App\Exceptions\SmsException;
use App\Models\SmsCode;
use App\Notifications\ConfirmationCode;
use Carbon\Carbon;
use Illuminate\Http\Response;

class SmsCodeService
{
    public function sendCode(string $phone)
    {
        try {
            if ($this->isExistsSendedCode($phone)) {
                throw SmsException::smsCodeWasSended();
            }
            $smsCode = $this->createCode(rand(100000, 999999), $phone);
            \Notification::route('smscru', '+7' . $phone)
                ->notify(new ConfirmationCode($smsCode->code));
        } catch (\Exception $e) {
            if (isset($smsCode)) {
                $smsCode->delete();
            }

            $errors = ['errors' => ['server' => [$e->getMessage()]]];
            return response()->json($errors, Response::HTTP_SERVICE_UNAVAILABLE);
        }
        return response()->json(['message' => 'Sms код отправлен'], Response::HTTP_OK);
    }

    /**
     *
     * @param string $code
     * @param string $phone
     * @return mixed
     */
    private function createCode(string $code, string $phone)
    {
        $smsCode = SmsCode::create([
            'code' => $code,
            'phone' => $phone
        ]);
        return $smsCode;
    }

    public function delete(array $attributes)
    {
        SmsCode::where($attributes)->firstOrFail()->delete();
    }

    /**
     * @param string $phone
     * @return boolean
     */
    private function isExistsSendedCode(string $phone)
    {
        $expiredTime = Carbon::now()->subMinutes(config('smscodes.expired_time'))->toDateTimeString();

        return SmsCode::where([
                    ['created_at', '>', $expiredTime],
                    ['phone', $phone]
                ])->exists();
    }
}
