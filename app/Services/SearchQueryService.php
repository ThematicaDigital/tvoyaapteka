<?php

namespace App\Services;

use App\Models\SearchQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SearchQueryService
{
    public function addOrIncrement(string $query, bool $isVoiced = false)
    {
        try {
            $query = SearchQuery::where([
                ['text', $query],
                ['isVoiced', $isVoiced],
            ])->whereDate('created_at', now()->toDateString())->firstOrFail();
            $query->increment('count');
        } catch (ModelNotFoundException $exception) {
            SearchQuery::create([
                'text' => $query,
                'isVoiced' => $isVoiced,
                'count' => 1,
            ]);
        }
    }

}
