<?php

namespace App\Services;

use App\Models\Category;
use App\Models\City;
use App\Models\Drug;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Url;

class SitemapService
{
    public function getSitemapForCity(City $city)
    {
        $sitemapsDir = config('seo.sitemaps_dir_name');

        if (!\File::exists(public_path($sitemapsDir))) {
            \File::makeDirectory(public_path($sitemapsDir));
        }

        $drugsMapFilename = $this->getDrugsMapFilename($city);
        $categoriesMapFilename = $this->getCategoriesMapFilename($city);

        $sitemapPath = public_path($sitemapsDir . $city->slug . '_sitemap.xml');

        SitemapIndex::create()
            ->add($categoriesMapFilename)
            ->add($drugsMapFilename)
            ->writeToFile($sitemapPath);

        return $sitemapPath;
    }

    private function getDrugsMapFilename(City $city)
    {
        $sitemapsDir = config('seo.sitemaps_dir_name');
        $filename = $sitemapsDir . $city->slug . '_drugs.xml';
        $filepath = public_path($filename);

        if ($this->isNeedGenerateMap($filepath)) {
            $this->generateDrugsMapForCity($city, $filepath);
        }

        return $filename;
    }

    private function getCategoriesMapFilename(City $city)
    {
        $sitemapsDir = config('seo.sitemaps_dir_name');
        $filename = $sitemapsDir . $city->slug . '_categories.xml';
        $filepath = public_path($filename);

        if ($this->isNeedGenerateMap($filepath)) {
            $this->generateCategoriesMapForCity($filepath);
        }

        return $filename;
    }

    private function generateDrugsMapForCity(City $city, string $filepath)
    {
        $sitemap = Sitemap::create();
        Drug::select('category_id', 'slug')
            ->whereHas('category')
            ->with('category')
            ->inStockForCity($city->id)
            ->rememberCache(60*24)
            ->chunk(200, function ($drugs) use ($sitemap, $city) {
                foreach ($drugs as $drug) {
                    $sitemap->add(Url::create($drug->url));
                }
            });
        $sitemap->writeToFile($filepath);
    }

    private function generateCategoriesMapForCity(string $filepath)
    {
        $sitemap = Sitemap::create();
        $categories = Category::select('slug')->rememberCache(60*24)->get();
        foreach ($categories as $category) {
            $url = route('catalog.category', ['category' => $category->slug]);
            $sitemap->add(Url::create($url));
        }
        $sitemap->writeToFile($filepath);
    }

    private function isNeedGenerateMap(string $filepath) {
        $regenerationTime = Carbon::now()->subHours(config('seo.sitemap_regeneration_interval'));
        return !\File::exists($filepath) ||
            Carbon::createFromTimestamp(\File::lastModified($filepath))->lessThan($regenerationTime);
    }
}