<?php

namespace App\Services;

use App\Models\Article;

class ArticleService
{
    public function getArticlesForApi(int $offset, int $limit)
    {
        return Article::select('id', 'title', 'announce', 'images', 'created_at')
            ->latest()
            ->rememberCache(60)
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

}
