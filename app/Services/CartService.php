<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\Drug;
use App\Models\DrugDrugstore;
use App\Models\Drugstore;
use App\Models\User;
use Illuminate\Support\Collection;

class CartService
{
    public function getUsersCartItemsByCityId(User $user, $cityId = null)
    {
        return Cart::where('user_id', $user->id)
            ->with([
                'drugstore',
                'drug' => function ($query) use ($cityId) {
                    $query->with([
                        'drugstores' => function ($query) use ($cityId) {
                            $query->withActiveFreeday()->where([
                                ['city_id', $cityId],
                                ['in_stock', '>', 0]
                            ]);
                        },
                        'cities'
                    ]);
                }
            ])
            ->latest()
            ->get();
    }

    /**
     * @param Collection $cartItems
     * @param $cityId
     * @return \Illuminate\Support\Collection
     */
    public function getCartItemsForUnauthorizedUser(Collection $cartItems, $cityId = null)
    {
        $drugsFromCart = Drug::select('drugs.*')
            ->whereIn('drugs.id', $cartItems->pluck('drug_id'))
            ->withDrugstores($cityId)
            ->get()
            ->keyBy('id');

        $drugstoresFromCart = Drugstore::whereIn('id', $cartItems->pluck('drugstore_id'))
            ->get()
            ->keyBy('id');

        $cartItems = $cartItems->mapWithKeys(function ($item) use ($drugsFromCart, $drugstoresFromCart) {

            $cartItem = new \stdClass;
            $cartItem->count = $item['count'];
            $cartItem->drug_id = $item['drug_id'];
            $cartItem->drugstore_id = $item['drugstore_id'];
            $cartItem->drug = $drugsFromCart[$item['drug_id']];
            $cartItem->drugstore = $drugstoresFromCart[$item['drugstore_id']];

            $priceWithDiscount = round($cartItem->drug->default_price, 2);
            if (!empty($cartItem->drug->cities->keyBy('id')[$cartItem->drugstore->city_id])) {
                $itemPrices = $cartItem->drug->cities->keyBy('id')[$cartItem->drugstore->city_id]->price;
                $priceWithDiscount = $itemPrices->priceWithDiscount;
            }
            $totalPriceWithDiscount = round($cartItem->count * $priceWithDiscount, 2);
            $cartItem->price_with_discount = $priceWithDiscount;
            $cartItem->total_price_with_discount = $totalPriceWithDiscount;
            return [$cartItem->drug_id => $cartItem];
        })->values();

        return $cartItems;
    }

    public function addItemsToUsersCart(Collection $items, User $user)
    {
        $itemsInCart = $user->cartDrugs->keyBy('id');
        $refreshedItemsInCart = $items->mapWithKeys(function ($item) use ($itemsInCart) {
            $drugDrugstore = DrugDrugstore::where('drug_id', $item['drug_id'])
                ->where('drugstore_id', $item['drugstore_id'])
                ->first();
            $count = $item['count'] + ($itemsInCart[$item['drug_id']]->pivot->count ?? 0);
            try {
                return [
                    $item['drug_id'] => [
                        'count' => $count > $drugDrugstore->in_stock ? $drugDrugstore->in_stock : $count,
                        'drugstore_id' => $item['drugstore_id']
                    ]
                ];
            } catch (\Throwable $e) {
                return [];
            }
        })->toArray();
        $user->cartDrugs()->syncWithoutDetaching($refreshedItemsInCart);
    }
}
