<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Drug;
use App\Models\DrugOrder;
use App\Models\Order;
use App\Models\Trigram;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DrugService
{
    public function getSearchedDrugs(Request $request, $cityId, $suggests = false)
    {
        $sorting = $request->sorting ?? 'price';
        $limit = $request->take ?? 9;

        $query = Drug::select(
                'drugs.id',
                'drugs.type',
                'drugs.slug',
                'drugs.title',
                'drugs.images',
                'drugs.category_id',
                'drugs.manufacturer_id'
            )
            ->withDrugstores($cityId, $request->is_24h ?? false)
            ->with('manufacturer');

        if ($request->sale) {
            $query->onlyWithDiscount($cityId);
        } else {
            $query->withPrice($cityId);
        }

        if ($request->is_24h) {
            $query->is24h();
        }

        if ($request->category_id) {
            $query->withCategoryId($request->category_id);
        }

        if ($request->drugstore_id) {
            $query->withDrugstoreId($request->drugstore_id);
        }


        $query->when($request->price_min, function ($query) use ($request) {
            return $query->where('city_drug.price', '>=', $request->price_min);
        })
            ->when($request->price_max, function ($query) use ($request) {
                return $query->where('city_drug.price', '<=', $request->price_max);
            })
            ->when($request->manufacturer, function ($query) use ($request) {
                return $query->where('manufacturer_id', $request->manufacturer);
            });

        if ($suggests === false) {
            $query->orderBy($sorting, 'ASC')->rememberCache(60);
        } else {
            return $query->whereIn('drugs.id', collect($suggests['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values())
                ->orderBy(\DB::raw('FIELD(drugs.id, ' . collect($suggests['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values()->implode(',') . ')'))
                ->get()->each(function($item) {
                    $item->drugstores->each(function($drugstore) {
                        $drugstore->pivot->makeHidden([
                            'created_at',
                            'drug_id',
                            'drugstore_id',
                            'updated_at',
                        ]);
                        return $drugstore->makeHidden([
                            'pivot.created_at',
                            'pivot.drug_id',
                            'city_id',
                            'created_at',
                            'deleted_at',
                            'freeday',
                            'freeday_id',
                            'is_24h',
                            'latitude',
                            'longitude',
                            'main_image',
                            'updated_at',
                            'worktime',
                            'images',
                        ]);
                    });
                    $item->manufacturer->makeHidden([
                        'created_at',
                        'id',
                        'updated_at',
                    ]);
                    return $item->makeHidden([
                        'category',
                        'title_admin',
                        'images'
                    ]);
                });
            return $query->whereIn('drugs.id', collect($suggests['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values())
                ->orderBy(\DB::raw('FIELD(drugs.id, ' . collect($suggests['matches'])->map(function($item) { return $item['attrs']['drug_id']; })->values()->implode(',') . ')'))
                ->get();
        }
        return $query->simplePaginate($limit)->items();
    }

    public function getDrugsByActualIdForCity(int $actualId, int $cityId, int $offset, int $limit)
    {
        return Drug::select('drugs.*')
            ->join('actual_drug', function ($join) use ($actualId){
                $join
                    ->on('drugs.id', '=', 'actual_drug.drug_id');

            })
            ->withDrugstores($cityId)
            ->withPrice($cityId)
            ->where('actual_drug.actual_id', $actualId)
            ->rememberCache(60)
            ->limit(30)
            ->get();
    }

    public function getDrugsFromCategory(int $categoryId, $request)
    {
        $requestSort = $request->sort ?? 'price';
        $sort = 'city_drug.price';

        if ($requestSort == 'title') {
            $sort = 'drugs.title';
        }
        return Drug::select('drugs.*', 'city_drug.price')
            ->withCategoryId($categoryId)
            ->when($request->hit, function ($query) {
                return $query->isHit();
            })
            ->when($request->manufacturers, function ($query) use ($request) {
                return $query->whereIn('manufacturer_id', explode(',', $request->manufacturers));
            })
            ->withDrugstores($request->city_id)
            ->when($request->sales, function ($query) use ($request) {
                return $query->onlyWithDiscount($request->city_id);
            }, function ($query) use ($request) {
                return $query->withPrice($request->city_id);
            })
            ->when($request->price_min, function ($query) use ($request) {
                return $query->where('city_drug.price', '>=', $request->price_min);
            })
            ->when($request->price_max, function ($query) use ($request) {
                return $query->where('city_drug.price', '<=', $request->price_max);
            })
            ->rememberCache(60)
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? 10)
            ->orderBy($sort, $request->order ?? 'asc')
            ->get();
    }

    public function getDrugsFromSearch($request)
    {

        $requestSort = $request->sort ?? 'price';
        $sort = 'city_drug.price';

        if ($requestSort == 'title') {
            $sort = 'drugs.title';
        }

        $query = Drug::select('drugs.*', 'city_drug.price')
            ->when($request->hit, function ($query) {
                return $query->isHit();
            })
            ->when($request->manufacturers, function ($query) use ($request) {
                return $query->whereIn('manufacturer_id', explode(',', $request->manufacturers));
            })
            ->withDrugstores($request->city_id)
            ->withPrice($request->city_id)
            ->when($request->sales, function ($query) {
                return $query->whereNotNull('city_drug.discount');
            })
            ->when($request->price_min, function ($query) use ($request) {
                return $query->where('city_drug.price', '>=', $request->price_min);
            })
            ->when($request->price_max, function ($query) use ($request) {
                return $query->where('city_drug.price', '<=', $request->price_max);
            });

        if ($request->q) {
            $results = (new Trigram)->searchSuggests($request->q, $request->limit ?? 10, $request->offset ?? 0);
            if ($results) {
                $query
                    ->whereIn('drugs.id', collect($results['matches'])->keys())
                    ->rememberCache(60)
                    ->offset($request->offset ?? 0)
                    ->limit($request->limit ?? 10)
                    ->orderBy($sort, $request->order ?? 'asc');
                return $query->get();
            }
        }
        return collect([]);
    }

    public function getDrugByIdWithDataForCity(int $drug, int $cityId)
    {
        $drug = Drug::select('drugs.*')
            ->withPrice($cityId)
            ->withDrugstores($cityId)
            ->with('manufacturer')
            ->findOrFail($drug);
        return $drug;
    }

    public function getRecommendedDrugsForCategory(int $limit = 8, string $categorySlug = null)
    {
        $recommendedDrugs = $this->getIndividualDrugs();
        $count = $recommendedDrugs->count();
        if ($count < $limit) {
            $limit = $limit - $count;
            $recommendedDrugs = $recommendedDrugs->merge($this->getSaleHits($limit, $categorySlug));
        }
        return $recommendedDrugs;
    }

    /**
     * get sale hits based by order & hit checkbox
     * @param int $limit
     * @param string $categorySlug|null
     * @return mixed
     */
    public function getSaleHits(int $limit = 8, string $categorySlug = null)
    {
        $cityId = session()->get('city_id', false);
        $category = null;
        if (!is_null($categorySlug)) {
            $category = Category::where('slug', $categorySlug)->first();
        }

        $hits = $this->getDrugsMarkedAsHit($limit, $cityId, $category);

        $totalHits = $hits->count();
        if ($totalHits < $limit) {
            $limit = $limit - $totalHits;
            $hits = $hits->merge($this->getSaleHitsByOrders($limit, $cityId, $category));
        }

        return $hits;
    }

    /**
     * @param int $limit
     * @param int $cityId
     * @param Category $category
     * @return mixed
     */
    public function getDrugsMarkedAsHit(int $limit, int $cityId, Category $category = null)
    {
        return Drug::select('drugs.*')
                ->withPrice($cityId)
                ->withDrugstores($cityId)
                ->when(!is_null($category), function ($query) use ($category) {
                    return $query->where('category_id', $category->id);
                })
                ->with('manufacturer')
                ->isHit()
                ->rememberCache(60*24)
                ->limit($limit)
                ->get();
    }

    /**
     * @param int $limit
     * @param int $cityId
     * @param Category $category
     * @return mixed
     */
    public function getSaleHitsByOrders(int $limit, int $cityId, Category $category = null)
    {
        $hitsDrugIds = DrugOrder::select('drug_id', \DB::raw('count(drug_id) as drugsCount'))
            ->when(!is_null($category), function ($query) use ($category) {
                return $query->join('drugs', 'drug_order.drug_id', '=', 'drugs.id')
                    ->where('drugs.category_id', $category->id);
            })
            ->groupBy('drug_id')
            ->orderBy('drugsCount', 'desc')
            ->limit($limit)
            ->rememberCache(60 * 24 * 7)
            ->get()
            ->pluck('drug_id');

        $hits = Drug::select('drugs.*')
            ->whereIn('drugs.id', $hitsDrugIds)
            ->withPrice($cityId)
            ->withDrugstores($cityId)
            ->with('manufacturer')
            ->rememberCache(60 * 24 * 7)
            ->get();

        return $hits;
    }


    public function getRecommendedDrugsByCartItems($cartItems)
    {
        $recommendedDrugs = $this->getIndividualDrugs();

        if ($recommendedDrugs->count() < 8) {
            $tooBuysDrugs = collect([]);
            $cartItems->map(function ($item) use ($tooBuysDrugs){
                $isTooBuyDrugs = $item->drug->isTooBuy;
                if ($tooBuysDrugs->count() < 8 && $isTooBuyDrugs->isNotEmpty()) {
                    $tooBuysDrugs->push($isTooBuyDrugs);
                }
            });
            $tooBuysDrugs = $tooBuysDrugs->flatten();
            $recommendedDrugs = $recommendedDrugs->merge($tooBuysDrugs)->take(8);
        }

        return $recommendedDrugs;
    }

    public function getIndividualDrugs(int $limit = 8)
    {
        if (!\Auth::check()) {
            return collect([]);
        }
        $cityId = session()->get('city_id', false);
        $user = \Auth::user();
        $orders = Order::where([
            ['user_id', '=', $user->id],
            ['status', '=', Order::STATUS['DONE']],
            ['created_at', '>', Carbon::now()->subMonths(3)->toDateTimeString()]
        ])->with([
            'drugs.forUses.drugs' => function ($query) use ($cityId) {
                $query->select('drugs.*')
                    ->withPrice($cityId)
                    ->withDrugstores($cityId)
                    ->with('manufacturer')
                    ->rememberCache(300);
            }
        ])->oldest()->get();

        $orderDrug = collect([]);
        return $orders->map(function ($order) use ($orderDrug) {
            return $order->drugs->map(function ($drug) use ($orderDrug) {
                $orderDrug->push($drug);
                return $drug->forUses->map(function ($forUse) {
                    return $forUse->drugs;
                });
            });
        })
        ->flatten()
        ->keyBy('id')
        ->except($orderDrug->pluck('id'))
        ->take($limit);
    }

    public function getRecommendedDrugsForCartPopup(Drug $drug)
    {
        $cityId = session()->get('city_id', false);
        $drugs = Drug::getAnalogs($drug,  $cityId, 8);

        if ($drugs->count() < 8) {
            $tooBuys = Drug::getIsTooBuy($drug, $cityId, 8);
            $drugs = $drugs->merge($tooBuys);
        }

        if ($drugs->count() < 8) {
            $individuals = $this->getIndividualDrugs();
            $drugs = $drugs->merge($individuals);
        }

        return $drugs->take(8);
    }
}
