<?php

namespace App\Services;


use App\Models\User;
use App\Notifications\PromotionsUpdated;


class UserService
{

    public function notifyPromotionsSubscribers()
    {
        User::doesntHave('deviceTokens')->where('is_notify', true)
            ->chunk(100, function ($users) {
                \Notification::send($users, new PromotionsUpdated());
            });
    }


    /**
     * @param $phone
     * @param $fields
     * @return \Illuminate\Database\Eloquent\Model|User
     */
    public function getOrCreateByPhone($phone, $fields)
    {
        $user = User::firstOrNew(['phone' => $phone]);
        if (!$user->exists) {
            foreach ($fields as $field => $value) {
                $user->$field = $value;
            }
            $user->save();
        }
        return $user;
    }

}
