<?php

namespace App\Services;


use App\Models\Article;
use App\Models\Category;
use App\Models\City;
use App\Models\Drug;
use App\Models\Page;

class SeoService
{
    public static function setTagsForCatalogIndex(City $city)
    {
        $defaultSeoTitle = 'Каталог интернет-магазина Семейная аптека ' . $city->title;
        $defaultSeoDescription = '';
        $defaultSeoKeywords = '';

        self::setMetaTags($defaultSeoTitle, $defaultSeoKeywords, $defaultSeoDescription);
    }
    public static function setTagsForCategory(City $city, Category $category, string $canonicalUrl)
    {
        $defaultSeoTitle = $category->title . ' – купить по выгодным ценам ' . ($city->title_seo_form ?? $city->title) . ' в интернет-магазине Семейная аптека';
        $defaultSeoDescription = 'Купить товары из раздела ' . $category->title . ' по складским ценам '
            . ($city->title_seo_form ?? $city->title)
            . ' в интернет-магазине Семейная аптека. Лучшие цены, оформление заказа онлайн, все товары в наличии.';
        $defaultSeoKeywords = $category->title . ', купить, аптека онлайн, цены, ' . $city->title;

        self::setMetaTags($category->meta_title ?? $defaultSeoTitle,
            $category->meta_keywords?? $defaultSeoKeywords,
            $category->meta_description?? $defaultSeoDescription);
        \SEOMeta::setCanonical($canonicalUrl);
    }

    public static function setTagsForDrug(City $city, Drug $drug)
    {

        $defaultSeoTitle = $drug->title . ' – цена от ' . $drug->price . ' руб., купить в онлайн-аптеке '
            . ($city->title_seo_form ?? $city->title);
        $defaultSeoDescription = $drug->title . ' по цене от ' . $drug->price . ' руб. в аптеке '
            . ($city->title_seo_form ?? $city->title) . ' – инструкция, отзывы, описание.';
        $defaultSeoKeywords = $drug->title . ' от ' . $drug->price . ' руб, купить, инструкция, интернет-аптека, ' . $city->title;

        self::setMetaTags($drug->meta_title ?? $defaultSeoTitle,
            $drug->meta_keywords?? $defaultSeoKeywords,
            $drug->meta_description?? $defaultSeoDescription);

    }

    public static function setTagsForPromotions(string $canonicalUrl)
    {
        self::setMetaTags('Товары по акции - Семейная аптека', 'Товары по акции', 'Товары по акции');
        \SEOMeta::setCanonical($canonicalUrl);
    }

    public static function setTagsForMainPage(City $city)
    {
        $defaultSeoTitle = 'Интернет-магазин «Семейная аптека» ' . ($city->title_seo_form ?? $city->title) . ' - купить лекарства онлайн';
        $defaultSeoDescription = 'Продажа лекарств, медицинских товаров онлайн в интернет-магазине «Семейная аптека» - 
            большой выбор, низкие цены';
        $defaultSeoKeywords = 'продажа лекарств, купить лекарства, интернет-аптека, ' . $city->title;

        self::setMetaTags($city->meta_title ?? $defaultSeoTitle,
            $city->meta_keywords?? $defaultSeoKeywords,
            $city->meta_description?? $defaultSeoDescription);
    }

    public static function setTagsForPage(Page $page)
    {
        self::setMetaTags($page->meta_title ?? $page->title, $page->meta_keywords, $page->meta_description);
    }

    public static function setTagsForArticle(Article $article)
    {
        self::setMetaTags($article->meta_title ?? $article->title, $article->meta_keywords, $article->meta_description);
    }

    public static function setMetaTags($title = '', $keywords = '', $description = '')
    {
        \SEOMeta::setTitle($title);
        \SEOMeta::setKeywords($keywords);
        \SEOMeta::setDescription($description);
    }
}
