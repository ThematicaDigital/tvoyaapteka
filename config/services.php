<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'smscru' => [
        'host' => env('SMSCRU_HOST', 'https://smsc.ru/'),
        'login'  => env('SMSCRU_LOGIN', 'login'),
        'secret' => env('SMSCRU_SECRET', 'password'),
        'sender' => env('SMSCRU_SENDER', 'semeynayaapteka.ru'),
    ],
    
    'firebase' => [
        'project_id' => env('FIREBASE_PROJECT_ID', ''),
        'client_id' => env('FIREBASE_CLIENT_ID', ''),
        'client_email' => env('FIREBASE_CLIENT_EMAIL', ''),
        'private_key' => file_get_contents(app_path('../certs/' . env('FIREBASE_PRIVATE_KEY_FILENAME', '')))
    ]
];
