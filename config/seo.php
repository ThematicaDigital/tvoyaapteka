<?php
return [
    'yandex_verification' => env('SEO_YANDEX_VERIFICATION', 'yandexverification'),
    'google_verification' => env('SEO_GOOGLE_VERIFICATION', 'googleverification'),
    'sitemaps_dir_name' => 'sitemaps/',
    'sitemap_regeneration_interval' => 24,
];
