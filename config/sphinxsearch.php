<?php
return [
    'host'    => env('SPHINX_HOST', '127.0.0.1'),
    'port'    => env('SPHINX_PORT', '9312'),
    'timeout' => 30,
    'indexes' => [
        env('SPHINX_INDEX_DRUGS', 'drugs') => false,
        env('SPHINX_INDEX_TRIGRAMS', 'trigrams') => false,
    ],
    'mysql_server' => [
        'host' => env('SPHINX_MYSQL_HOST', '127.0.0.1'),
        'port' => env('SPHINX_MYSQL_PORT', '9306'),
    ]
];
