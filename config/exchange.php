<?php
return [
	'zip' => env('EXCHANGE_ZIP', 'yes'),
	'fileLimit' => env('EXCHANGE_FILE_LIMIT', 2048),
	'storagePath' => 'exchange',
	'user' => env('EXCHANGE_PASSWORD', 'user'),
	'password' => env('EXCHANGE_PASSWORD', 'password'),
	'drugsFilename' => env('EXCHANGE_DRUGS_FILENAME', 'TvAptTovar.json'),
	'drugstoresFilename' => env('EXCHANGE_DRUGSTORES_FILENAME', 'TvAptApteki.json'),
	'inStockFilename' => env('EXCHANGE_INSTOCK_FILENAME', 'TvAptOstatki.json'),
	'priceFilename' => env('EXCHANGE_PRICE_FILENAME', 'TvAptCena.json'),
	'discountsFilename' => env('EXCHANGE_DISCOUNTS_FILENAME', 'TvAptDiscounts.json'),
	'recommendationsFilename' => env('EXCHANGE_RECOMMENDATIONS_FILENAME', 'DopTovar.json'),
];
