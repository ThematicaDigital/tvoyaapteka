<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Partner::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($faker->numberBetween(1, 3)),
        'image' => 'uploads/partners/' . $faker->image('public/uploads/partners/', 130, 45, 'cats', false)
    ];
});
