<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Promocode::class, function (Faker $faker) {
    return [
        'title' => $faker->randomNumber(),
        'type' => $faker->randomElement([1, 2, 3]),
        // 'is_percent' => $faker->boolean(10),
        'amount' => $faker->randomFloat(2, 1, 300000),
    ];
});
