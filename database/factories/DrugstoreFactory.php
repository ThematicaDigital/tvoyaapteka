<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Drugstore::class, function (Faker $faker) {
    $cities = \App\Models\City::all()->pluck('id')->toArray();
    return [
        'title' => $faker->sentence(2),
        'address' => $faker->address,
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'city_id' => $faker->randomElement($cities),
        'images' => [
            'uploads/drugstores/' . $faker->image('public/uploads/drugstores/', 800, 600, 'cats', false)
        ],
        'freeday_id' => null,
        'worktime' => time('H:i', '10:00') . ' - ' . time('H:i', '24:00'),
        'is_24h' => $faker->boolean(40)
    ];
});
