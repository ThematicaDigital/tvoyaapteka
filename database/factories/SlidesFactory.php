<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Slide::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'image' => 'uploads/slides/' . $faker->image('public/uploads/slides/', 1140, 400, 'cats', false),
        'mobile_image' => 'uploads/slides/' . $faker->image('public/uploads/slides/', 400, 300, 'cats', false),
        'url' => $faker->url(),
        'drug_id' => null,
        'city_id' => null,
        'position' => $faker->randomElement([
        	App\Models\Slide::POSITION['TOP'],
        	App\Models\Slide::POSITION['BOTTOM'],
        ])
    ];
});
