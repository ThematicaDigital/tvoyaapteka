<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Page::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'content' => $faker->paragraph(),
        'meta_title' => $faker->sentence(),
        'meta_keywords' => $faker->sentence(),
        'meta_description' => $faker->sentence()
    ];
});