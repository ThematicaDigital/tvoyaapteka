<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Resume::class, function (Faker $faker) {
    $vacancies = \App\Models\Vacancy::all()->pluck('id')->toArray();
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'vacancy_id' => $faker->randomElement($vacancies),
        'file' => 'uploads/resumes/' . $faker->image('public/uploads/resumes/', 1280, 1024, 'cats', false),
        'text' => $faker->paragraph,
    ];
});
