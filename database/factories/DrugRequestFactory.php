<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\DrugRequest::class, function (Faker $faker) {

    $dosageForms = \App\Models\DosageForm::all()->pluck('id')->toArray();

    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'text' => $faker->paragraph,
        'drug' => $faker->word,
        'count' => $faker->randomDigitNotNull,
        'dosage_form_id' => $faker->randomElement($dosageForms),
    ];
});
