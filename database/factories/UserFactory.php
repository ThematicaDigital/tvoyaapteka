<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function ($faker) {
    $groups = \App\Models\Group::all()->pluck('id')->toArray();
    return [
        'firstname' => $faker->firstNameMale,
        'lastname' => $faker->lastName,
        'surname' => $faker->middleNameMale,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'password' => 'password',
        'is_notify' => $faker->boolean(10),
        'group_id' => $faker->randomElement($groups),
    ];
});
