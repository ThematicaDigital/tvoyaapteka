<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Vacancy::class, function (Faker $faker) {

    $cities = \App\Models\City::all()->pluck('id')->toArray();
    return [
        'title' => $faker->sentence($faker->numberBetween(1, 3)),
        'city_id' => $faker->randomElement($cities),
        'pay' => $faker->randomElement(['от ', 'до ']) . $faker->numberBetween(20000, 100000),
        'text' => $faker->paragraph,
        'phones' => $faker->phoneNumber,
    ];
});
