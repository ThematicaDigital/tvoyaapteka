<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Promotion::class, function (Faker $faker) {
    $cities = \App\Models\City::all()->pluck('id')->toArray();
    return [
        'title' => $faker->sentence(),
        'text' => $faker->paragraph(),
        'start_datetime' => \Carbon\Carbon::now()->toDateTimeString(),
        'end_datetime' => \Carbon\Carbon::now()->addDays(rand(15, 60))->addHours(rand(10, 100))->toDateTimeString(),
        'images' => [
            'uploads/promotions/' . $faker->image('public/uploads/promotions/', 800, 600, 'cats', false)
        ],
        'url' => $faker->url,
        'city_id' => $faker->randomElement($cities),
    ];
});
