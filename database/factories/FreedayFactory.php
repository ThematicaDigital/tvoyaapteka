<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Freeday::class, function (Faker $faker) {
    $statuses = \App\Models\FreedayStatus::all()->pluck('id')->toArray();
    $startDate = \Carbon\Carbon::now()->addDays(rand(0, 2));
    return [
        'start_datetime' => $startDate->toDateTimeString(),
        'end_datetime' => $startDate->addDay()->toDateTimeString(),
        'freeday_status_id' => $faker->randomElement($statuses),
    ];
});
