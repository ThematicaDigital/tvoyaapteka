<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Drug::class, function (Faker $faker) {
    $categories = \App\Models\Category::all()->pluck('id')->toArray();
    $manufacturers = \App\Models\Manufacturer::all()->pluck('id')->toArray();
    return [
        'category_id' => $faker->randomElement($categories),
        'title' => $faker->sentence(),
        'text' => $faker->randomHtml(2,3),
        'images' => [
            'uploads/drugs/' . $faker->image('public/uploads/drugs/', 800, 600, 'cats', false)
        ],
        'meta_title' => $faker->sentence(),
        'meta_keywords' => $faker->sentence(),
        'meta_description' => $faker->sentence(),
        'manufacturer_id' => $faker->randomElement($manufacturers),
        'is_hit' => $faker->boolean(10),
        'molecular_composition' => $faker->word,
        'type' => $faker->randomElement([1, 2, 3]),
        'default_price' => $faker->randomFloat(2, 1, 300000),
    ];
});
