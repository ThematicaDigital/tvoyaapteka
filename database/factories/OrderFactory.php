<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Order::class, function (Faker $faker) {
    $drugstores = \App\Models\Drugstore::all()->pluck('id')->toArray();
    $promocodes = \App\Models\Promocode::all()->pluck('id')->toArray();
    return [
        'user_id' => 1,
        'status' => $faker->randomElement([1,2,3,4,5]),
        'drugstore_id' => $faker->randomElement($drugstores),
        'promocode_id' => $faker->randomElement($promocodes),
        'paid_at' => \Carbon\Carbon::now()->addHours(rand(1, 5))->toDateTimeString()
    ];
});
