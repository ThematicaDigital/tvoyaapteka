<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'announce' => $faker->paragraph(),
        'content' => $faker->paragraph(),
        'images' => [
        	'uploads/articles/' . $faker->image('public/uploads/articles/', 520, 300, 'cats', false)
        ],
        'meta_title' => $faker->sentence(),
        'meta_keywords' => $faker->sentence(),
        'meta_description' => $faker->sentence()
    ];
});
