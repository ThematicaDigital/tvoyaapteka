<?php

use Illuminate\Database\Seeder;

class PartnersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::delete(File::allFiles('public/uploads/partners/'));
        \App\Models\Partner::truncate();
        factory(\App\Models\Partner::class, 7)->create();
    }
}
