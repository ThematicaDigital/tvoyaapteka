<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	File::delete(File::allFiles('public/uploads/articles/'));
        Article::truncate();
        factory(Article::class, 20)->create();
    }
}
