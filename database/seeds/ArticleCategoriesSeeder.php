<?php

use Illuminate\Database\Seeder;
use App\Models\ArticleCategory;

class ArticleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	ArticleCategory::truncate();
    	ArticleCategory::create(['title' => 'Красота']);
    	ArticleCategory::create(['title' => 'Здоровье']);
    	ArticleCategory::create(['title' => 'Питание']);
    	ArticleCategory::create(['title' => 'Спорт']);
    	ArticleCategory::create(['title' => 'Вопрос-ответ']);
    }
}
