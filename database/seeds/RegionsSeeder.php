<?php

use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Region::truncate();
        \App\Models\Region::insert([
            [
                'id' => 77,
                'title' => 'Москва',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 25,
                'title' => 'Приморский край',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 28,
                'title' => 'Амурская область',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 27,
                'title' => 'Хабаровский край',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 14,
                'title' => 'Республика Саха (Якутия)',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 79,
                'title' => 'Еврейская автономная область',
                'created_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
