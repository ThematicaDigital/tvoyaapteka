<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::truncate();
        factory(\App\Models\User::class)->create([
            'id' => 1,
            'email' => 'admin@site.com',
            'phone' => '+79098848294',
            'group_id' => 1,
        ]);
        factory(\App\Models\User::class)->create([
            'id' => 2,
            'email' => 'user@site.com',
            'phone' => '+79656713021',
            'group_id' => 2,
        ]);
        factory(\App\Models\User::class, 5)->create();
    }
}
