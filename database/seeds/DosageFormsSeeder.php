<?php

use Illuminate\Database\Seeder;

class DosageFormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DosageForm::truncate();
        \App\Models\DosageForm::insert([
            [
                'title' => 'аэрозоль',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'бальзам',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'брикеты',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'гель',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'гранулы',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'губка',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'драже',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'жидкость',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'имплант',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'капли',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'капсула',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'карамель',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'карандаш',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'концентрат',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'кора',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'корневища',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'крем',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'лак',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'леденцы',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'линимент',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'лиофилизат',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'листья',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'лосьон',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'мазь',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'масло',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'микстура',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'настойка',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'паста',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пастилки',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пеллеты',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пена',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пилюли',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пластина',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пластырь',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пленка',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'пленки',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'плоды',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'побеги',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'порошок',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'раствор',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'резинка',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'салфетки',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'саше',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'сбор',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'семена',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'сироп',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'сок',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'спрей',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'субстанция',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'суппозитории',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'суспензия',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'таблетки',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'тампон',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'трава',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'цветки',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'шампунь',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'экстракт',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'эликсир',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'эмульсия',
                'created_at' => \Carbon\Carbon::now()
            ],
        ]);
    }
}