<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::truncate();
        factory(Page::class)->create([
        	'title' => 'О компании',
            'slug' => 'about',
        ]);
        factory(Page::class)->create([
        	'title' => 'Как оформить',
            'slug' => 'how-to-apply',
        ]);
        factory(Page::class)->create([
        	'title' => 'Информация',
            'slug' => 'information',
        ]);
        factory(Page::class)->create([
        	'title' => 'Сроки и условия получения заказа',
            'slug' => 'terms-and-conditions',
        ]);
        factory(Page::class)->create([
        	'title' => 'Политика конфиденциальности',
            'slug' => 'privacy-policy',
        ]);
    }
}
