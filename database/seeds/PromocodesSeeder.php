<?php

use Illuminate\Database\Seeder;

class PromocodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\Promocode::truncate();

        factory(\App\Models\Promocode::class, 20)->create();
    }
}
