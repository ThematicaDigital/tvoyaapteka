<?php

use Illuminate\Database\Seeder;

class DrugRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\DrugRequest::class, 10)->create();
    }
}
