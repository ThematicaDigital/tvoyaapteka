<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = 'database/sqls/categories.sql';
        Category::truncate();
        DB::unprepared(file_get_contents($sql));
    }
}
