<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key' => 'facebook_link',
                'name' => 'Ссылка на фейсбук',
                'description' => '',
                'value' => 'https://www.facebook.com/327061551438674',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
            [
                'key' => 'vk_link',
                'name' => 'Ссылка на ВК',
                'description' => '',
                'value' => 'https://vk.com/semeinayaa',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
            [
                'key' => 'ok_link',
                'name' => 'Ссылка на одноклассники',
                'description' => '',
                'value' => 'https://ok.ru/group/55284927955054',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
            [
                'key' => 'instagram_link',
                'name' => 'Ссылка на instagram',
                'description' => '',
                'value' => 'https://www.instagram.com/apteka.semeynaya/',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
            [
                'key' => 'promotion_notification_title',
                'name' => 'Заголовок пуш уведомления о новых акциях',
                'description' => '',
                'value' => 'Изменения в акциях',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
            [
                'key' => 'promotion_notification_text',
                'name' => 'Текст пуш уведомления о новых акциях',
                'description' => '',
                'value' => 'Посмотрите на наши новые акции!',
                'field' => '{"name":"value","label":"Значение","type":"text"}',
                'active' => 1,
            ],
        ];

        DB::table('settings')->insert($settings);
    }
}
