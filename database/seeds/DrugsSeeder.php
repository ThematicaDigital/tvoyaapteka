<?php

use Illuminate\Database\Seeder;

class DrugsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drugstores = \App\Models\Drugstore::all()->pluck('id');
        $city = \App\Models\City::all()->pluck('id');
        $faker = \Faker\Factory::create();
        File::delete(File::allFiles('public/uploads/drugs/'));
        \App\Models\Drug::truncate();
        factory(\App\Models\Drug::class, 150)->create()->each(function (\App\Models\Drug $drug) use ($drugstores, $city, $faker) {
            $drugstoresWithAmountOfDrug = $drugstores->mapWithKeys(function ($item) {
                return [$item => ['in_stock' => rand(1, 100)]];
            })->toArray();
            $cityWithPrice = $city->mapWithKeys(function ($item) use ($faker) {
                return [
                    $item => [
                        'price' => $faker->randomFloat(1, 10000),
                        'discount' => $faker->randomFloat(1, 30),
                        'discount_is_percent' => $faker->boolean(70),
                    ]
                ];
            })->toArray();
            $drug->drugstores()->attach($drugstoresWithAmountOfDrug);
            $drug->cities()->attach($cityWithPrice);
        });
    }
}
