<?php

use Illuminate\Database\Seeder;

class FreedaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Freeday::truncate();
        factory(\App\Models\Freeday::class, 15)->create();
    }
}
