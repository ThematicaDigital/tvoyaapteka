<?php

use Illuminate\Database\Seeder;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Group::truncate();
        \App\Models\Group::insert([
            [
                'id' => 1,
                'title' => 'Администраторы',
                'slug' => 'admins'
            ],
            [
                'id' => 2,
                'title' => 'Пользователи',
                'slug' => 'users',
            ],
            [
                'id' => 3,
                'title' => 'Подтвержденные пользователи',
                'slug' => 'confirmed_users',
            ],
            [
                'id' => 4,
                'title' => 'Незарегистрированные пользователи',
                'slug' => 'unregistered_users',
            ],
        ]);
    }
}
