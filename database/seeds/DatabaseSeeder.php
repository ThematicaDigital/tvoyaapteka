<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $this->call(GroupsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SlidesSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(ArticlesSeeder::class);
        $this->call(PartnersSeeder::class);
        $this->call(RegionsSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(VacanciesSeeder::class);
        $this->call(ResumesSeeder::class);
        $this->call(DosageFormsSeeder::class);
        // $this->call(DrugRequestsSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ManufacturersSeeder::class);
        $this->call(DrugstoresSeeder::class);
        $this->call(DrugsSeeder::class);
        $this->call(PromocodesSeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(PromotionsSeeder::class);

        Schema::enableForeignKeyConstraints();
    }
}
