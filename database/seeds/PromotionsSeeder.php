<?php

use Illuminate\Database\Seeder;

class PromotionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::delete(File::allFiles('public/uploads/promotions/'));
        \App\Models\Promotion::truncate();

        $drugs = \App\Models\Drug::all()->pluck('id')->toArray();
        $faker = \Faker\Factory::create();

        factory(\App\Models\Promotion::class, 40)->create()->each(function (\App\Models\Promotion $promotion) use ($drugs, $faker) {
            $randomDrugs = $faker->randomElements($drugs, rand(1, 10));
            $promotion->drugs()->attach($randomDrugs);
        });
    }
}
