<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\City::truncate();
        \App\Models\City::insert([
            [
                'title' => 'Москва',
                'slug' => 'moskva',
                'region_id' => 77,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Артем',
                'slug' => 'artem',
                'region_id' => 25,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Владивосток',
                'slug' => 'vladivostok',
                'region_id' => 25,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Находка',
                'slug' => 'nakhodka',
                'region_id' => 25,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Трудовое',
                'slug' => 'trudovoye',
                'region_id' => 25,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Уссурийск',
                'slug' => 'ussuriysk',
                'region_id' => 25,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Белогорск',
                'slug' => 'belogorsk',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Благовещенск',
                'slug' => 'blagoveshchensk',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Екатеринославка',
                'slug' => 'yekaterinoslavka',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Зея',
                'slug' => 'zeya',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Константиновка',
                'slug' => 'konstantinovka',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Магдагачи',
                'slug' => 'magdagachi',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Новобурейский',
                'slug' => 'novobureyskiy',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Прогресс',
                'slug' => 'progress',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Райчихинск',
                'slug' => 'raychikhinsk',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Свободный',
                'slug' => 'svobodnyy',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Тында',
                'slug' => 'tynda',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Шимановск',
                'slug' => 'shimanovsk',
                'region_id' => 28,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Ванино',
                'slug' => 'vanino',
                'region_id' => 27,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Вознесенское',
                'slug' => 'voznesenskoye',
                'region_id' => 27,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Комсомольск-на-Амуре',
                'slug' => 'komsomolsk-na-amure',
                'region_id' => 27,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Хабаровск',
                'slug' => 'khabarovsk',
                'region_id' => 27,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Нерюнгри',
                'slug' => 'neryungri',
                'region_id' => 14,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Якутск',
                'slug' => 'yakutsk',
                'region_id' => 14,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Биробиджан',
                'slug' => 'birobidzhan',
                'region_id' => 79,
                'created_at' => \Carbon\Carbon::now()
            ],
        ]);
    }
}





