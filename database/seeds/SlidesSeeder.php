<?php

use Illuminate\Database\Seeder;
use App\Models\Slide;

class SlidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::delete(File::allFiles('public/uploads/slides/'));
        Slide::truncate();
        factory(Slide::class, 5)->create(['position' => Slide::POSITION['TOP']]);
    }
}
