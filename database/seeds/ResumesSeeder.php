<?php

use Illuminate\Database\Seeder;

class ResumesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::delete(File::allFiles('public/uploads/resumes/'));
        factory(\App\Models\Resume::class, 10)->create();
    }
}
