<?php

use Illuminate\Database\Seeder;

class FreedayStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\FreedayStatus::truncate();
        \App\Models\FreedayStatus::insert([
            [
                'id' => 1,
                'title' => 'Ревизия',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 2,
                'title' => 'Ремонт',
                'created_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
