<?php

use Illuminate\Database\Seeder;

class ManufacturersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Manufacturer::truncate();
        factory(\App\Models\Manufacturer::class, 15)->create();
    }
}
