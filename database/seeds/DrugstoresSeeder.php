<?php

use Illuminate\Database\Seeder;

class DrugstoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::delete(File::allFiles('public/uploads/drugstores/'));

        \App\Models\Drugstore::truncate();

        factory(\App\Models\Drugstore::class, 20)->create();
    }
}
