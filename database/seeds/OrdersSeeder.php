<?php

use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drugs = \App\Models\Drug::inRandomOrder()->take(100)->get()->pluck('id')->toArray();
        $faker = \Faker\Factory::create();
        \App\Models\Order::truncate();
        factory(\App\Models\Order::class, 20)->create()->each(function (\App\Models\Order $order) use ($faker, $drugs) {
            $randomDrugs = $faker->randomElements($drugs, rand(1, 3));
            $randomDrugsWithProps = collect($randomDrugs)->mapWithKeys(function ($item, $key) use ($faker){
                $price = $faker->randomFloat(2, 1, 10000);
                return [
                    $item => [
                        'count' => rand(1, 10),
                        'price' => $price,
                        'discount' => $faker->randomFloat(2, 1, round($price)),
                        'discount_is_percent' => $faker->boolean(10),
                    ]
                ];
            })->toArray();
            $order->drugs()->sync($randomDrugsWithProps);
        });
    }
}
