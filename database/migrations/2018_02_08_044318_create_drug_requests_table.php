<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drug_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('drug');
            $table->unsignedInteger('dosage_form_id')->nullable();
            $table->integer('count');
            $table->string('name');
            $table->string('phone');
            $table->text('text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drug_requests');
    }
}
