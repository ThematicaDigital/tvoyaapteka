<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugstoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugstores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('1c_id');
            $table->string('title');
            $table->string('address');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->unsignedInteger('city_id');
            $table->text('images')->nullable();
            $table->unsignedInteger('freeday_id')->nullable();
            $table->string('worktime')->nullable();
            $table->boolean('is_24h')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugstores');
    }
}
