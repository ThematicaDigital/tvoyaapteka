<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('drugstore_id');
            $table->enum('status', [
                \App\Models\Order::STATUS['IN_PROGRESS'],
                \App\Models\Order::STATUS['APPROVED'],
                \App\Models\Order::STATUS['DONE'],
                \App\Models\Order::STATUS['CANCELED'],
                \App\Models\Order::STATUS['PAID'],
            ])->default(\App\Models\Order::STATUS['IN_PROGRESS']);
            $table->unsignedInteger('promocode_id')->nullable();
            $table->dateTime('created_datetime')->nullable();
            $table->dateTime('pay_datetime')->nullable();
            $table->string('phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
