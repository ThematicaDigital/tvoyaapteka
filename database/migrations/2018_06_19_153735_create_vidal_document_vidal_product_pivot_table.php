<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVidalDocumentVidalProductPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vidal_Document_Product', function (Blueprint $table) {
            $table->integer('vidal_product_id')->unsigned()->index();
            $table->integer('vidal_document_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vidal_Document_Product');
    }
}
