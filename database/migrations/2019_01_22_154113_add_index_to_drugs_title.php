<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToDrugsTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drugs', function (Blueprint $table) {
            $table->index('title');
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->index(['start_at', 'end_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drugs', function (Blueprint $table) {
            $table->dropIndex('drugs_title_index');
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->dropIndex('city_drug_start_at_end_at_index');
        });
    }
}
