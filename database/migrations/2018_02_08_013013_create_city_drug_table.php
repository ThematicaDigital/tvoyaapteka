<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityDrugTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_drug', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('drug_id');
            $table->unsignedInteger('city_id');
            $table->decimal('price')->nullable();
            $table->decimal('discount')->nullable();
            $table->boolean('discount_is_percent')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_drug');
    }
}
