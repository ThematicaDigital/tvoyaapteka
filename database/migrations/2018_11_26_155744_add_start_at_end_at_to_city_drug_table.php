<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartAtEndAtToCityDrugTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_drug', function (Blueprint $table) {
            $table->dateTime('start_at')->nullable()->default(null)->after('discount_is_percent');
            $table->dateTime('end_at')->nullable()->default(null)->after('start_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_drug', function (Blueprint $table) {
            $table->dropColumn('end_at');
            $table->dropColumn('start_at');
        });
    }
}
