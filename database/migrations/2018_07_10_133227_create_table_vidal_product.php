<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVidalProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vidal_Product', function (Blueprint $table) {
            $table->increments('ProductID');
            $table->string('RusName');
            $table->string('EngName');
            $table->boolean('NonPrescriptionDrug')->default(false);
            $table->string('RegistrationDate');
            $table->string('DateOfCloseRegistration')->nullable()->default(null);
            $table->string('RegistrationNumber')->unique()->index();
            $table->boolean('PPR')->default(false);
            $table->longText('Composition');
            $table->string('ZipInfo');
            $table->string('ProductTypeCode');
            $table->boolean('GNVLS')->default(false);
            $table->boolean('DLO')->default(false);
            $table->boolean('MinAs')->default(false);
            $table->integer('MarketStatusID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vidal_Product');
    }
}
