<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountTypeFieldToPromocodes extends Migration
{
    public function __construct()
    {
        \DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promocodes', function (Blueprint $table) {
            $table->dropColumn('is_percent');
            $table->enum('amount_type', [
                \App\Models\Promocode::AMOUNT_TYPE['PERCENT'],
                \App\Models\Promocode::AMOUNT_TYPE['DISCOUNT'],
                \App\Models\Promocode::AMOUNT_TYPE['FIXED_PRICE']
            ])->default(\App\Models\Promocode::AMOUNT_TYPE['PERCENT']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promocodes', function (Blueprint $table) {
            $table->boolean('is_percent')->nullable();
            $table->dropColumn('amount_type');
        });
    }
}
