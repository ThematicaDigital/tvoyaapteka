<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysAndConstraints extends Migration
{
    public function __construct()
    {
        \DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('actual_drug', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('actual_id')->references('id')->on('actuals')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('article_drug', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('bonus_cards', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('bonus_logs', function (Blueprint $table) {
            $table->foreign('drugstore_id')->references('id')->on('drugstores')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bonus_card_id')->references('id')->on('bonus_cards')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('carts', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_drugstore', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('drugstore_id')->references('id')->on('drugstores')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_for_use', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('for_use_id')->references('id')->on('for_uses')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_order', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_page', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_promotion', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('promotion_id')->references('id')->on('promotions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drug_requests', function (Blueprint $table) {
            $table->foreign('dosage_form_id')->references('id')->on('dosage_forms')->onDelete('set null')->onUpdate('cascade');
        });
        Schema::table('drug_user', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drugs', function (Blueprint $table) {
            $table->unsignedBigInteger('manufacturer_id')->change();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('drugstores', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('freeday_id')->references('id')->on('freedays')->onDelete('set null')->onUpdate('cascade');
        });
        Schema::table('freedays', function (Blueprint $table) {
            $table->foreign('freeday_status_id')->references('id')->on('freeday_statuses')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('drugstore_id')->references('id')->on('drugstores')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('promocode_id')->references('id')->on('promocodes')->onDelete('set null')->onUpdate('cascade');
        });
        Schema::table('promotions', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null')->onUpdate('cascade');
        });
        Schema::table('resumes', function (Blueprint $table) {
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('slides', function (Blueprint $table) {
            $table->unsignedBigInteger('drug_id')->change();
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null')->onUpdate('cascade');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('vacancies', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_id')->change();
            $table->foreign('parent_id')->references('id')->on('categories')->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('actual_drug', function (Blueprint $table) {
            $table->dropForeign(['actual_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('actual_drug', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('article_drug', function (Blueprint $table) {
            $table->dropForeign(['article_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('article_drug', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('bonus_cards', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('bonus_logs', function (Blueprint $table) {
            $table->dropForeign(['drugstore_id']);
            $table->dropForeign(['bonus_card_id']);
        });
        Schema::table('carts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('carts', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign(['region_id']);
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_drugstore', function (Blueprint $table) {
            $table->dropForeign(['drugstore_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_drugstore', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_for_use', function (Blueprint $table) {
            $table->dropForeign(['for_use_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_for_use', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_order', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_order', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_page', function (Blueprint $table) {
            $table->dropForeign(['page_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_page', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_promotion', function (Blueprint $table) {
            $table->dropForeign(['promotion_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_promotion', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('drug_requests', function (Blueprint $table) {
            $table->dropForeign(['dosage_form_id']);
        });
        Schema::table('drug_user', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['drug_id']);
        });
        Schema::table('drug_user', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });


        Schema::table('drugs', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropForeign(['manufacturer_id']);
        });
        Schema::table('drugs', function (Blueprint $table) {
            $table->unsignedInteger('manufacturer_id')->change();
        });
        Schema::table('drugstores', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
            $table->dropForeign(['freeday_id']);
        });
        Schema::table('freedays', function (Blueprint $table) {
            $table->dropForeign(['freeday_status_id']);
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['drugstore_id']);
            $table->dropForeign(['promocode_id']);
        });
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::table('resumes', function (Blueprint $table) {
            $table->dropForeign(['vacancy_id']);
        });
        Schema::table('slides', function (Blueprint $table) {
            $table->dropForeign(['drug_id']);
            $table->dropForeign(['city_id']);
        });
        Schema::table('slides', function (Blueprint $table) {
            $table->unsignedInteger('drug_id')->change();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['group_id']);
        });
        Schema::table('vacancies', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->change();
        });
    }
}
