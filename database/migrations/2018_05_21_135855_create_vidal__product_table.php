<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVidalProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->boolean('type')->default(false);
            $table->string('registration_start');
            $table->string('registration_stop')->nullable()->default(null);
            $table->string('registration_number')->unique()->index();
            $table->boolean('ppr')->default(false);
            $table->longText('description');
            $table->string('dosage_form');
            $table->string('product_type');
            $table->boolean('gnvls')->default(false);
            $table->boolean('dlo')->default(false);
            $table->boolean('min_as')->default(false);
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descriptions');
    }
}
