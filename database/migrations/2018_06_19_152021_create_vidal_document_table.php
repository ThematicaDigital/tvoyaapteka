<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVidalDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vidal_Document', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('DocumentID');
            $table->string('RusName');
            $table->string('EngName');
            $table->string('Elaboration');
            $table->longText('CompaniesDescription');
            $table->integer('ArticleID');
            $table->longText('CompiledComposition');
            $table->longText('ClPhGrDescription');
            $table->longText('PhInfluence');
            $table->longText('PhKinetics');
            $table->longText('Dosage');
            $table->longText('OverDosage');
            $table->longText('Interaction');
            $table->longText('Lactation');
            $table->longText('SideEffects');
            $table->longText('StorageCondition');
            $table->longText('Indication');
            $table->longText('ContraIndication');
            $table->longText('SpecialInstruction');
            $table->string('YearEdition');
            $table->string('PregnancyUsing');
            $table->string('NursingUsing');
            $table->longText('RenalInsuf');
            $table->string('RenalInsufUsing');
            $table->longText('HepatoInsuf');
            $table->string('HepatoInsufUsing');
            $table->longText('PharmDelivery');
            $table->longText('ElderlyInsuf');
            $table->string('ElderlyInsufUsing');
            $table->longText('ChildInsuf');
            $table->string('ChildInsufUsing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vidal_Document');
    }
}
