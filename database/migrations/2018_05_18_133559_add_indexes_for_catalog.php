<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesForCatalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drugs', function (Blueprint $table) {
            $table->index(['category_id', 'default_price', 'manufacturer_id']);
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->index(['drug_id', 'city_id', 'price']);
        });
        Schema::table('drugstores', function (Blueprint $table) {
            $table->index(['city_id', 'is_24h']);
        });
        Schema::table('drug_drugstore', function (Blueprint $table) {
            $table->index(['drug_id', 'drugstore_id', 'in_stock']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drugs', function (Blueprint $table) {
            $table->dropIndex(['category_id', 'default_price', 'manufacturer_id']);
        });
        Schema::table('city_drug', function (Blueprint $table) {
            $table->dropIndex(['drug_id', 'city_id', 'price']);
        });
        Schema::table('drugstores', function (Blueprint $table) {
            $table->dropIndex(['city_id', 'is_24h']);
        });
        Schema::table('drug_drugstore', function (Blueprint $table) {
            $table->dropIndex(['drug_id', 'drugstore_id', 'in_stock']);
        });
    }
}
