<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailForFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feedbacks', function (Blueprint $table) {
            $table->string('email')->after('phone')->nullable()->default(null);
            $table->boolean('need_feedback')->after('text')->default(false);
            $table->unsignedInteger('drugstore_id')->after('text')->nullable()->default(null)->index();
            $table->foreign('drugstore_id')->references('id')->on('drugstores')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedbacks', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('need_feedback');
            $table->dropColumn('drugstore_id');
        });
    }
}
