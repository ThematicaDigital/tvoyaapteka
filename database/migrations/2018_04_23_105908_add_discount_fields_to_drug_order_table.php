<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountFieldsToDrugOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_order', function (Blueprint $table) {
            $table->decimal('discount')->nullable();
            $table->boolean('discount_is_percent')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumns('drug_order', ['discount', 'discount_is_percent'])) {
            Schema::table('drug_order', function (Blueprint $table) {
                $table->dropColumn(['discount', 'discount_is_percent']);
            });
        }
    }
}
