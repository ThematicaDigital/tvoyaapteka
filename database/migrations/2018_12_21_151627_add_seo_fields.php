<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->text('seo_text_catalog')->nullable();
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->text('seo_text')->nullable();
        });
        Schema::table('drugs', function (Blueprint $table) {
            $table->text('seo_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn(['seo_text_catalog']);
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn(['seo_text']);
        });
        Schema::table('drugs', function (Blueprint $table) {
            $table->dropColumn(['seo_text']);
        });
    }
}
