<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('1c_id');
            $table->string('title');
            $table->enum('type', [
                \App\Models\Promocode::TYPE['ONE_TIME'],
                \App\Models\Promocode::TYPE['MANY_TIMES'],
                \App\Models\Promocode::TYPE['ONE_TIME_FOR_USER']
            ])->default(\App\Models\Promocode::TYPE['ONE_TIME']);
            $table->integer('amount');
            $table->boolean('is_percent');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes');
    }
}
