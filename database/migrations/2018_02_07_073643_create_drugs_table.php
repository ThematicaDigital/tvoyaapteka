<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('text')->nullable();
            $table->text('images')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->unsignedInteger('manufacturer_id');
            $table->boolean('is_hit')->default(false);
            $table->string('molecular_composition')->nullable();
            $table->enum('type', [
                \App\Models\Drug::PRESCRIPTION['NO'],
                \App\Models\Drug::PRESCRIPTION['NORMAL'],
                \App\Models\Drug::PRESCRIPTION['HIGH']
            ])->default(\App\Models\Drug::PRESCRIPTION['NO']);
            $table->decimal('default_price');
            $table->integer('search_ranking')->nullable();
            $table->integer('search_ranking_manual')->nullable();
            $table->boolean('publish')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
    }
}
