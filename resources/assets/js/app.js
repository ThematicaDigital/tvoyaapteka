
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
"use strict";

import 'babel-polyfill';
require('./bootstrap');

//smart-app-banner
import SmartBanner from 'smart-app-banner';
// chartjs package
require('chart.js');
// vue-charts package
require('hchs-vue-charts');
//momentjs library
window.moment = require('moment');

window.Vue = require('vue');

import Vue from 'vue';
import VModal from 'vue-js-modal';
import Notifications from 'vue-notification';
import VTooltip from 'v-tooltip';
import Paginate from 'vuejs-paginate';
import VueCarousel from 'vue-carousel';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Slick from 'vue-slick';
import MaskedInput from 'vue-text-mask'
 
//Components for Auth/Registration
import Userlogin from './components/user/login.vue';
import Userregister from './components/user/registration.vue';
import UserForget from './components/user/forgetpassword.vue';

Vue.use(VueCarousel);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueCharts);
Vue.use(VModal);
Vue.use(Notifications);
Vue.use(VTooltip);
Vue.component('paginate', Paginate);
Vue.component('slick', Slick);
Vue.component('masked-input', MaskedInput);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Forms
 */
Vue.component('autocomplete', require('./components/forms/autocomplete.vue'));
Vue.component('suggestion-item', require('./components/header/suggestion-item.vue'));
Vue.component('choose-city', require('./components/forms/choose-city.vue'));
Vue.component('choose-drugstore', require('./components/forms/choose-drugstore.vue'));
Vue.component('vmodal', require('./components/forms/modal.vue'));
Vue.component('favorite', require('./components/forms/favorite.vue'));
Vue.component('add-to-cart', require('./components/forms/addToCart.vue'));
Vue.component('toggle-panel', require('./components/forms/toggle.vue'));
Vue.component('add-to-basket', require('./components/forms/add-to-basket.vue'));
Vue.component('change-notification', require('./components/forms/change-notification.vue'));
Vue.component('drug-request-form', require('./components/forms/drug-request-form.vue'));

Vue.component('individuals', require('./components/drugs/individuals.vue'));
Vue.component('individuals-popup', require('./components/drugs/individuals-popup.vue'));
Vue.component('drugs-actuals', require('./components/drugs/actuals.vue'));
Vue.component('drugs-hits', require('./components/drugs/hits.vue'));
Vue.component('discounts', require('./components/discounts/discounts.vue'));
Vue.component('drugs-item-block', require('./components/drugs/item-block.vue'));
Vue.component('drugs-item-list', require('./components/drugs/item-list.vue'));
Vue.component('attached-drugs', require('./components/drugs/attached-drugs.vue'));

Vue.component('slider-main', require('./components/main/slider-main.vue'));
Vue.component('slider-main-mobile', require('./components/main/slider-main-mobile.vue'));
Vue.component('slider-main-promo', require('./components/main/slider-main-promo.vue'));
Vue.component('vacancies-list', require('./components/vacancies/vacancies-list.vue'));
Vue.component('resume-modal', require('./components/modals/resume-modal'));
Vue.component('flash-notifications', require('./components/flash-notifications'));
Vue.component('promotions', require('./components/promotions/promotions.vue'));

Vue.component('drugstores-select', require('./components/forms/drugstores-select.vue'));

/**
 * User
 */
Vue.component('signin', require('./components/user/signin.vue'));
Vue.component('signin-open-button', require('./components/user/signin-open-button.vue'));

/**
 * Profile
 */
Vue.component('profile', require('./components/profile/profile.vue'));
Vue.component('orders', require('./components/profile/orders-index.vue'));
Vue.component('order-item', require('./components/profile/order.vue'));
Vue.component('changepswd', require('./components/profile/changepswd.vue'));
Vue.component('bonuscard', require('./components/profile/bonuscard.vue'));

/**
 * Catalog
 */
Vue.component('catalog-items', require('./components/catalog/items.vue'));
Vue.component('catalog-item', require('./components/catalog/item.vue'));
Vue.component('catalog-filter', require('./components/catalog/filter.vue'));
Vue.component('catalog-categories', require('./components/catalog/categories.vue'));
Vue.component('sidebar-categories', require('./components/catalog/sidebar-categories.vue'));
Vue.component('catalog-category', require('./components/catalog/category.vue'));
Vue.component('show-modal', require('./components/catalog/show-modal.vue'));

/**
 * Header
 */
Vue.component('catalog-menu', require('./components/header/catalog-menu.vue'));

/**
 * Favorites
 */
Vue.component('favorites-items', require('./components/favorites/items.vue'));

/**
 * Contacs
 */
Vue.component('contacs-body', require('./components/contacs/body.vue'));
Vue.component('contacs-item', require('./components/contacs/item.vue'));
Vue.component('map-widget', require('./components/contacs/ymap.vue'));

/**
 * Cart
 */
Vue.component('cart-header', require('./components/cart/header.vue'));
Vue.component('cart-index', require('./components/cart/index.vue'));
Vue.component('cart-item', require('./components/cart/item.vue'));
Vue.component('cart-preview-item', require('./components/cart/cart-preview-item.vue'));

/**
 * Admin Panel
 */
Vue.component('chart', require('./components/admin/chart.vue'));

/**
 * mobile
 */
Vue.component('mobheader', require('./components/mobile/mobheader.vue'));

/**
 * Articles
 */
Vue.component('article-slider', require('./components/article/slider.vue'));
Vue.component('article-slider-mobile', require('./components/article/slider-mobile.vue'));
Vue.component('article-categories', require('./components/article/categories.vue'));

const router = new VueRouter({
	routes:[
		{
			path:'/login',
			name:'login',
			component: Userlogin,
		},
		{
			path:'/registration/:step',
			name:'register',
			component:Userregister,
		},
		{
			path:'/forgetpassword/:step',
			name:'forgetpassword',
			component:UserForget ,
		}
	]
});

const store = new Vuex.Store({
	modules:{
		login:{
			state:{
				remember:false,
				phone: '',
				password: '',
			}
		},
		registration:{
			state:{
				phone: '',
				sms_code: '',
				password: '',
				password_confirmation: '',
			},
		},
		forget:{
			state:{
				phone: '',
				sms_code: '',
				password: '',
				password_confirmation: '',
				sucess:false,
			}
        },
        catalogFilter: {
            state: {
                minPrice: 0,
                maxPrice: 100000,
                manufacturer: null,
                sorting: 'price',
                is24h: false,
            },
            mutations: {
                setMinPrice: function (state, price) {
                    state.minPrice = price;
                },
                setMaxPrice: function (state, price) {
                    state.maxPrice = price;
                },
                setManufacturer: function (state, manufacturer) {
                    state.manufacturer = manufacturer;
                },
                setSorting: function (state, sorting) {
                    state.sorting = sorting;
                },
                setIs24h: function (state, is24h) {
                    state.is24h = is24h;
                },
            }
        }
	},
});

const app = new Vue({
	el:'#app',
    router,
    store,
    mounted: function() {
    	window.cartHeader = this.$refs.cartHeader;
    	window.authModal = this.$refs.authModal;
    }
});

new SmartBanner({
	daysHidden: 15,   // days to hide banner after close button is clicked (defaults to 15)
	daysReminder: 90, // days to hide banner after "VIEW" button is clicked (defaults to 90)
	appStoreLanguage: 'ru', // language code for the App Store (defaults to user's browser language)
	title: 'Семейная Аптека.рф',
	author: '',
	button: 'Скачать',
	store: {
		ios: 'В App Store',
		android: 'В Google Play',
		windows: 'В Windows store'
	},
	price: {
		ios: 'Бесплатно',
		android: 'Бесплатно',
		windows: 'Бесплатно'
	}
});