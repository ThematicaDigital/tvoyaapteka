@if (!$drugs->isEmpty())
	<individuals
		:drugs="{{ $drugs->toJson() }}"
		:limit="4"
		:css-class="'red'"
		:title="'Хиты продаж'">
	</individuals>
@endif