@if (!$drugs->isEmpty())
	<individuals
		:drugs="{{ $drugs->toJson() }}"
		:limit="3"
		:css-class="'blue'"
		:title="'Вам будет интересно'">
	</individuals>
@endif