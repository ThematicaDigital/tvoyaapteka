@if (!$drugs->isEmpty())
	<individuals
		:drugs="{{ $drugs->makeHidden('is_favorite')->toJson() }}"
		:limit="4"
		:css-class="'blue'"
		:title="'Хиты продаж'">
	</individuals>
@endif