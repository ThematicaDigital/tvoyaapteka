@if (!$drugs->isEmpty())
	<individuals
		:drugs="{{ $drugs->toJson() }}"
		:limit="4"
		:css-class="'blue'"
		:title="'Вам будет интересно'">
	</individuals>
@endif