@extends('layouts.app')

@section('content')
<div class="grid-container">
    <div class="grid-x grid-margin-x">
        <nav role="navigation">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li>Личный кабинет</li>
            </ul>
        </nav>
    </div>
    <div class="grid-x grid-margin-x">
        <div class="cell small-12 medium-4 large-3"><a class="button alert"><span class="white">Отправить отзыв</span></a></div>
        <div class="cell small-12 medium-4 large-3"><a class="hollow button">Все заказы</a></div>
        <div class="cell small-12 medium-4 large-3"><a class="hollow button secondary">Задать вопрос</a></div>
        <div class="cell small-12 medium-4 large-3"><a class="button blue-gradient">Читать дальше</a></div>
    </div>
    <div class="grid-x grid-margin-x">
        <label>Введите Ваше ФИО
            <input type="text">
        </label>
    </div>
    <!-- <div class="grid-x grid-margin-x">
        <div class="callout">
            <h5 class="red">Не можете найти нужный товар?</h5>
            <p>It has an easy to override visual style, and is appropriately subdued.</p>
        </div>
    </div> -->
    <div class="grid-x grid-margin-x">
        <div class="callout shadow-hover">
            <h5 class="blue">Маркетолог</h5>
            <p>It has an easy to override visual style, and is appropriately subdued.</p>
        </div>
    </div>
    <div class="grid-x grid-margin-x">
        <div class="callout red">
            <p>It has an easy to override visual style, and is appropriately subdued.</p>
        </div>
    </div>
</div>
@endsection