@if ($slides->isNotEmpty())
    <div class="slider-main-promo-container margin-vertical-2 show-for-large">
        <slider-main-promo :slides="{{ $slides->toJson() }}"></slider-main-promo>
    </div>
@endif