@if ($slides)
	<div class="grid-container">
        <div class="grid-x grid-margin-x margin-bottom-2">
			<div class="cell small-12">
				<slider-main class="hide-for-small-only" :slides="{{ $slides }}"></slider-main>
				<slider-main-mobile class="show-for-small-only" :slides="{{ $slides }}"></slider-main>
			</div>
		</div>
	</div>
@endif