@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-bottom-1">
                @include('individuals.404')
                @if (env('APP_ENV') == 'local' || env('APP_DEBUG') == true)
                    <div class="callout shadow margin-vertical-2">
                        <hgroup>
                            <h1>500</h1>
                            <h2>It's not you, it's me.</h2>
                        </hgroup>
                        <?php
                        $default_error_message = "Please return to <a href='".url('')."'>our homepage</a>.";
                        ?>
                        {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
