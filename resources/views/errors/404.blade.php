@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-bottom-1">
                <div class="errorpage text-center">
                  <h1 class="blue margin-bottom-1">404</h1>
                  <h2 class="blue margin-bottom-1">Извините</h2>
                  <p class="blue">такой страницы не существует...</p>
                </div>
                <div class="text-center">
                    <a href="/">Вернуться на главную</a>
                </div>
                @include('individuals.404')
                @if (env('APP_ENV') == 'local' || env('APP_DEBUG') == true)
                    <div class="callout shadow margin-vertical-2">
                        <hgroup>
                            <h1>404</h1>
                            <h2>Not found</h2>
                        </hgroup>
                        <?php
                          $default_error_message = "Please return to <a href='".url('')."'>our homepage</a>.";
                        ?>
                        {!! isset($exception)? ($exception->getMessage()?$exception->getMessage():$default_error_message): $default_error_message !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection