<h6 class="show-for-small-only">Партнеры</h6>
<div class="grid-x grid-margin-x">
	@foreach($partners as $partner)
		<div class="cell small-3 margin-bottom-1">
			<img src="{{ route('image', [140, 40, $partner->image]) }}" title="{{ $partner->title }}" alt="{{ $partner->title }}">
		</div>
	@endforeach
</div>