<aside class="article-sidebar">
	<div class="margin-bottom-1 profile-accordeon">
		<a class="accordion">Мой кабинет</a>
		<ul class="accpanel menu vertical">
			<li><a href="{{ route('profile') }}" title="Профиль">Профиль</a></li>
			<li><a href="{{ route('profile.orders') }}" title="Список заказов">Список заказов</a></li>
			<li><a href="{{ route('favorites.index') }}" title="Избранные товары">Избранные товары</a></li>
			<li><a href="{{ route('profile.bonus') }}" title="Дисконтная карта">Дисконтная карта</a></li>
			<li><a href="{{ route('profile.password') }}" title="Сменить пароль">Сменить пароль</a></li>
		</ul>
	</div>
	@if (Auth::check())
		<change-notification :selected="{{ Auth::user()->is_notify ? 'true' : 'false' }}">
		</change-notification>
	@endif
</aside>
@push('scripts_after')
	<script>
        var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].addEventListener("click", function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
		  });
		}
	</script>
@endpush