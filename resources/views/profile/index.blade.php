@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-top-1">
                {!! Breadcrumbs::render('profile') !!}
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-3 margin-bottom-1">
                @include('profile.sidebar')
            </div>
            <div class="cell small-12 medium-9 margin-bottom-1">
                <div class="profile-index">
                    <h1 class="profile-index__header">Личный кабинет</h1>
                    <profile :user="{{ Auth::user()->toJson() }}"></profile>
                </div>
            </div>
            <div class="cell small-12 margin-bottom-1">
                @include('individuals.profile')
            </div>
        </div>
    </div>
@endsection