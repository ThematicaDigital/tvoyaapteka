@extends('layouts.main')

@section('content')
    <div class="grid-container">
    	<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('catalog.search', $q) !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x catalog-category">
			<div class="cell small-12 medium-3 margin-bottom-1">
				@include('articles.sidebar')
			</div>
			<div class="cell small-12 medium-9">
				<div class="grid-x grid-margin-x">
					@if ($suggest)
			    		<div class="cell small-12 margin-bottom-1">
			    			Возможно вы имели ввиду: <a href="{{ route('drugs.search') }}?q={{ $suggest }}">{{ $suggest }}</a>
			    		</div>
			    	@endif
			    	@if ($drugs)
			    		@foreach ($drugs as $drug)
			    			<div class="width-100 callout shadow-hover padding-1 drug">
								<div class="grid-x grid-margin-x">
									<div class="cell small-12 medium-3">
										<a href="{{ $drug->url }}" title="{{ $drug->title }}">
											<img src="{{ $drug->image }}" title="{{ $drug->title }}" alt="{{ $drug->title }}" />
										</a>
									</div>
									<div class="cell small-12 medium-6">
										<a href="{{ $drug->url }}" title="{{ $drug->title }}">
											<h2 class="drug__title">
												{{ $drug->title }}
											</h2>
										</a>
										<p class="drug__manufacturer">
											Производитель: {{ $drug->manufacturer->title }}
										</p>
										<hr class="drug__hr">
										<p class="drug__in_stock">
											В наличии
										</p>
									</div>
									<div class="cell small-12 medium-3 text-center">
										<div class="drug__price margin-bottom-1">
											{{ $drug->default_price }} руб.
										</div>
										<a href="{{ $drug->url }}" class="button expanded text-uppercase drug__in_bascket">
											В корзину
										</a>
									</div>
								</div>
							</div>
			    		@endforeach
			    	@endif
			    </div>
			</div>
        </div>
    </div>
@endsection