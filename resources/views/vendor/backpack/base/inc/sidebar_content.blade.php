<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}"><i class="fa fa-cog"></i> <span>Настройки сайта</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>Менеджер файлов</span></a></li>
<li><a href="{{ backpack_url('users') }}"><i class="fa fa-user-o"></i> <span>Пользователи</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-shopping-basket"></i> <span>Магазин</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('actuals') }}"><i class="fa fa-tag"></i> <span>Актуальное</span></a></li>
        <li><a href="{{ backpack_url('categories') }}"><i class="fa fa-tag"></i> <span>Категории</span></a></li>
        <li><a href="{{ backpack_url('drugs') }}"><i class="fa fa-tag"></i> <span>Товары</span></a></li>
        <li><a href="{{ backpack_url('manufacturers') }}"><i class="fa fa-tag"></i><span>Производители</span></a></li>
        <li><a href="{{ backpack_url('orders') }}"><i class="fa fa-tag"></i> <span>Заказы</span></a></li>
        <li><a href="{{ backpack_url('recommendation-categories') }}"><i class="fa fa-tag"></i><span>Категории рекомендуемых товары</span></a></li>
{{--        <li><a href="{{ backpack_url('promocodes') }}"><i class="fa fa-tag"></i><span>Промокоды</span></a></li>--}}
    </ul>
</li>

<li><a href="{{ backpack_url('partners') }}"><i class="fa fa-tag"></i> <span>Партнеры</span></a></li>
<li><a href="{{ backpack_url('slides') }}"><i class="fa fa-tag"></i> <span>Слайды</span></a></li>
<li><a href="{{ backpack_url('pages') }}"><i class="fa fa-tag"></i> <span>Страницы</span></a></li>


<li class="treeview">
    <a href="#"><i class="fa fa-plus-circle"></i> <span>Блог</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('articles') }}"><i class="fa fa-tag"></i> <span>Статьи</span></a></li>
        <li><a href="{{ backpack_url('article_categories') }}"><i class="fa fa-tag"></i> <span>Категории</span></a></li>
        <li><a href="{{ backpack_url('article_slides') }}"><i class="fa fa-tag"></i> <span>Слайдер</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-plus-circle"></i> <span>Аптеки</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('drugstores') }}"><i class="fa fa-tag"></i> <span>Аптеки</span></a></li>
        <li><a href="{{ backpack_url('freedays') }}"><i class="fa fa-tag"></i> <span>Нерабочие дни</span></a></li>
        <li><a href="{{ backpack_url('freeday-statuses') }}"><i class="fa fa-tag"></i> <span>Статусы нерабочих дней</span></a></li>
    </ul>
</li>

<li><a href="{{ backpack_url('regions') }}"><i class="fa fa-tag"></i> <span>Регионы</span></a></li>
<li><a href="{{ backpack_url('cities') }}"><i class="fa fa-tag"></i> <span>Города</span></a></li>

<li><a href="{{ backpack_url('vacancies') }}"><i class="fa fa-tag"></i> <span>Вакансии</span></a></li>
<li><a href="{{ backpack_url('resumes') }}"><i class="fa fa-tag"></i> <span>Резюме</span></a></li>

<li><a href="{{ backpack_url('feedbacks') }}"><i class="fa fa-tag"></i> <span>Обратная связь</span></a></li>
<li><a href="{{ backpack_url('drug-requests') }}"><i class="fa fa-tag"></i><span>Заявки на препараты</span></a></li>
<li><a href="{{ backpack_url('dosage-forms') }}"><i class="fa fa-tag"></i> <span>Формы выпуска препаратов</span></a></li>

<li><a href="{{ backpack_url('promotions') }}"><i class="fa fa-tag"></i> <span>Акции</span></a></li>
<li><a href="{{ backpack_url('discount-cards') }}"><i class="fa fa-tag"></i> <span>Дисконтные карты</span></a></li>
<li><a href="{{ backpack_url('search-queries') }}"><i class="fa fa-search"></i> <span>Поисковые запросы</span></a></li>