@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-bottom-1 margin-top-1">
                {!! Breadcrumbs::render('vacancies.index') !!}
            </div>
        </div>
        <vacancies-list :vacancies="{{ json_encode($vacancies) }}"></vacancies-list>
        <resume-modal action="{{  route('resume.store') }}"></resume-modal>
    </div>
@endsection