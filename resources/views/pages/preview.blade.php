@if ($page)
	<article class="margin-bottom-1">
		<h2>{{ $page->title }}</h2>
		{!! $page->content !!}
	</article>
@endif