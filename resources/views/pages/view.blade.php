@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('page.view', $page) !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-3 margin-vertical-1">
				@include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-9 margin-vertical-1 articles">
			    <article>
			    	<h1>{{ $page->title }}</h1>
			    	{!! $page->content !!}
			    </article>
				<div class="grid-x grid-margin-x">
					@foreach($page->drugs as $drug)
						<div class="cell medium-4 small-12">
							<drugs-item-block
									:key="{{$drug->id}}"
									:drug="{{$drug->toJson()}}">
							</drugs-item-block>
						</div>
					@endforeach
				</div>
            </div>
        </div>
	</div>
@endsection