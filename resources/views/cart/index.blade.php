@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x hide-for-small-only">
            <div class="cell small-12 margin-bottom-1 margin-top-1">
                {!! Breadcrumbs::render('cart') !!}
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 hide-for-medium-only large-3 margin-vertical-1">
                @include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-12 large-9 margin-vertical-1 cart-index">
                <cart-index :make-order="{{$makeOrder ? 'true' : 'false'}}" :cart-items="{{$cartItems->toJson()}}" :user="{{ Auth::check() ? Auth::user()->toJson() : 'false' }}"></cart-index>
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-vertical-1">
            @if ($recommendedDrugs->isNotEmpty())
                <individuals
                        :drugs="{{ $recommendedDrugs->toJson() }}"
                        :limit="4"
                        :css-class="'blue'"
                        :title="'Вас может заинтересовать'">
                </individuals>
            @endif
            @if ($cartItems->isNotEmpty())
                    <individuals
                            :is-viewed="true"
                            :limit="4"
                            :css-class="'blue'"
                            :title="'Вы недавно смотрели'">
                    </individuals>
            @else
                @include('individuals.cart')
            @endif
            </div>
        </div>
    </div>
@endsection