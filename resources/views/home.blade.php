@extends('layouts.app')

@section('content')
<div class="grid-container">
    <div class="grid-x grid-margin-x">
        <div class="cell small-12 medium-4 large-3">1</div>
        <div class="cell small-12 medium-4 large-3">2</div>
        <div class="cell small-12 medium-4 large-3">3</div>
        <div class="cell small-12 medium-4 large-3">4</div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
