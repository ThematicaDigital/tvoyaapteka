@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-vertical-1">
                {!! Breadcrumbs::render('promotions.index') !!}
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-3 margin-vertical-1">
                @include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-9 margin-vertical-1">
                <promotions :promotions="{{ $promotions }}"></promotions>
            </div>
        </div>
    </div>
@endsection