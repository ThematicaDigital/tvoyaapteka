@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-bottom-1 margin-top-1">
                {!! Breadcrumbs::render('promotions.index') !!}
            </div>
        </div>
        <div class="grid-x grid-margin-x catalog-category">
            <div class="cell small-12 medium-3 margin-bottom-1">
                @include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-9">
                    <catalog-items
                            class="margin-bottom-1"
                            :discount="true"
                            :items="{{ $items->toJson() }}"
                            :drugstores="{{ $drugstores->toJson() }}"
                            ></catalog-items>
            </div>
        </div>
    </div>
    {{--@if(!empty($category->seo_text))--}}
        {{--<div class="grid-container">--}}
            {{--{!! $category->seo_text !!}--}}
        {{--</div>--}}
    {{--@endif--}}
@endsection