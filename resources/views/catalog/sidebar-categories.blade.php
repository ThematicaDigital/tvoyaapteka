
@if ($sidebarCategories->isNotEmpty())
	<sidebar-categories :sidebar-categories="{{ $sidebarCategories->toJson() }}"></sidebar-categories>
@endif