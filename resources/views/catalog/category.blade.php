@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				@if (!empty($category))
					{!! Breadcrumbs::render('catalog.category', $category) !!}
				@elseif ($q)
					{!! Breadcrumbs::render('catalog.search', $q) !!}
				@endif
			</div>
		</div>
		<div class="grid-x grid-margin-x catalog-category">
			<div class="cell small-12 medium-4 large-3 margin-bottom-1">
				<catalog-filter
					class="margin-bottom-1"
					:q="'{{ !empty($q) ? $q : '' }}'"
					:category="{{ isset($category) ? $category->toJson() : 0 }}"
					:city-id="{{ session()->get('city_id') }}"></catalog-filter>
				@if(!empty($category))
					{!! \App\Http\Controllers\CatalogController::sidebarCategories($category) !!}
				@endif
				@include('articles.sidebar')
			</div>
			<div class="cell small-12 medium-8 large-9">
				@if (!empty($category))
					<catalog-items
						class="margin-bottom-1"
						{{--:current-page="{{ $page }}"--}}
						:category="{{ $category->toJson() }}"
						:items="{{ $items->toJson() }}"
						:drugstores="{{ $drugstores->toJson() }}"></catalog-items>
				@elseif ($q)
					<catalog-items
						class="margin-bottom-1"
						{{--:current-page="{{ $page }}"--}}
						:q="'{{ $q }}'"
						:dosage-forms="{{ $dosageForms }}"
						:drugstores="{{ $drugstores->toJson() }}"
						:suggest="'{{ $suggest }}'"></catalog-items>
				@endif

				@include('individuals.category')
			</div>
        </div>
	</div>
	@if(!empty($category->seoTextFull))
		<div class="grid-container">
			{!! $category->seoTextFull !!}
		</div>
	@endif
@endsection
