@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('catalog.view', $drug) !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x catalog-view">
			<div class="cell small-12 medium-6 drug margin-bottom-1 show-for-small-only">
				<h1 class="drug__title margin-bottom-1 ">
					{{ $drug->title }}
				</h1>
			</div>
			<div class="cell small-12 medium-6 margin-bottom-1 drug__images text-center">
				<div class="statuses">
				@if ($drug->type == \App\Models\Drug::PRESCRIPTION['NORMAL'])
					<div class="prescription padding-horizontal-1" title="Товар продается при наличии рецепта">
						<i class="icon-pharmacy-icon"></i> Рецептурный препарат
					</div>
				@elseif ($drug->type == \App\Models\Drug::PRESCRIPTION['HIGH'])
					<div class="super-prescription padding-horizontal-1" title="Товар продается только при наличии рецепта по форме 107-1/у">
						<i class="icon-pharmacy-icon"></i> Строгорецептурный препарат
					</div>
				@endif
				</div>
				<img src="{{ asset($drug->image) }}" title="{{ $drug->title }}" alt="{{ $drug->title }}"
					 class="drug__images__image"/>
			</div>
			<div class="cell small-12 medium-6 drug margin-bottom-1">
				<h1 class="drug__title margin-bottom-1 hide-for-small-only">
					{{ $drug->title }}
				</h1>
				@if ($drug->manufacturer->title)
				<div class="drug__manufacturer margin-bottom-1">
					Производитель: {{ $drug->manufacturer->title }}
					<favorite :drug-id="{{ $drug->id }}" :is-favorite="{{(int)$drug->is_favorite}}"></favorite>
				</div>
				@endif
				<div class="drug__in_stock margin-bottom-1">
					Есть в наличии на выбранной аптеке
				</div>
				<add-to-basket :drug="{{ $drug->toJson() }}" :current-drugstore="{{$currentDrugstore}}"></add-to-basket>
				<div class="drug__info">
					<div class="callout lightblue flex-container align-justify">
						<div class="align-self-middle margin-right-1">
							<i class="icon-receiving-time" style="font-size:1.7rem"></i>
						</div>
						<div class="align-self-middle">
							Все товары в заказе резирвируются на 24 часа, после этого заказ автоматически отменяется
						</div>
					</div>
				</div>
			</div>
			@if ($drug->text)
				<div class="drug__description margin-bottom-1">
					{!! htmlspecialchars_decode($drug->text) !!}
				</div>
			@endif
        </div>
        @if (!$drug->analogs->isEmpty())
            <drugs-hits :drugs="{{ $drug->analogs->toJson() }}" :title="'Аналоги'"></drugs-hits>
        @endif
        @if ($recommendedDrugs->isNotEmpty())
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 margin-bottom-1">
					<individuals :drugs="{{ $recommendedDrugs->toJson() }}"
						:limit="4"
						:css-class="'blue'"
						:title="'Вас может заинтересовать'">
					</individuals>
				</div>
			</div>
        @endif
	</div>

	@if(!empty($drug->seo_text))
		<div class="grid-container">
			{!! $drug->seo_text !!}
		</div>
	@endif
@endsection