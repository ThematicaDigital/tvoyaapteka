@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('catalog.index') !!}
			</div>
		</div>
		<catalog-categories :categories="'{{ $categories }}'"></catalog-categories>
	</div>
	@if(!empty($city->seo_text_catalog))
		<div class="grid-container">
			{!! $city->seo_text_catalog !!}
		</div>
	@endif
@endsection