<div class="cell small-12 medium-4 margin-bottom-1 articles">
	<div class="width-100 height-100 callout shadow-hover article padding-0 position-relative">
		<a href="{{ route('article.view', [$article->category->slug, $article->slug]) }}" title="{{ $article->title }}" class="article-absolute-link"></a>
		<a class="article-label padding-horizontal-1 position-absolute" href="{{ route('article.category', [$article->category->slug]) }}" title="{{ $article->category->title }}">
			{{ $article->category->title }}
		</a>
		<img src="{{ route('image', [370, 250, $article->previewPic]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="width-100">
		<div class="padding-1 padding-bottom-0">
			<h2>
				{{ $article->title }}
			</h2>
			<div class="article_date margin-bottom-1">
				{{ $article->created }} |
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="16" height="16" viewBox="0 0 24 24" style="fill:#9e9e9e;top:2px;position:relative"><path d="M12,9A3,3 0 0,1 15,12A3,3 0 0,1 12,15A3,3 0 0,1 9,12A3,3 0 0,1 12,9M12,4.5C17,4.5 21.27,7.61 23,12C21.27,16.39 17,19.5 12,19.5C7,19.5 2.73,16.39 1,12C2.73,7.61 7,4.5 12,4.5M3.18,12C4.83,15.36 8.24,17.5 12,17.5C15.76,17.5 19.17,15.36 20.82,12C19.17,8.64 15.76,6.5 12,6.5C8.24,6.5 4.83,8.64 3.18,12Z" /></svg> {{ $article->views }}
			</div>
		</div>
	</div>
</div>