@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('article.view', $article) !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x">
            <div class="cell small-12 large-3 hide-for-medium-only margin-vertical-1">
				@include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-12 large-9 margin-vertical-1 articles">
			    <article>
			    	<h1>{{ $article->title }}</h1>
				@if($article->images)
					<p class="text-center">
						<img src="/{{ current($article->images) }}">
					</p>
				@endif
			    	{!! $article->content !!}
			    </article>
				@if($article->drugs)
					<attached-drugs :drugs="{{ $article->drugs->toJson() }}"></attached-drugs>
				@endif
            </div>
        </div>
	</div>
@endsection