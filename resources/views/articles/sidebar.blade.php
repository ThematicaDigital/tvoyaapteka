<aside class="article-sidebar hide-for-small-only">
	<!-- <div class="callout text-center margin-bottom-1">
	    <h5 class="red">Не можете найти нужный товар?</h5>
	    <p>Грамотный специалист проконсультирует вас по бесплатному номеру</p>
	    <a class="red free-phone" href="tel:88001000003" title="8-800-100-000-3">8-800-100-000-3</a>
	    <p class="text-uppercase">Круглосуточно</p>
	</div> -->
	<div class="callout blue text-center">
		<p class="article-sidebar__title">В помощь покупателю</p>
		<ul class="menu vertical">
			<li><a href="{{ route('page.view', 'how-to-apply') }}" title="">Как оформить заказ</a></li>
			<li><a href="{{ route('page.view', 'terms-and-conditions') }}" title="">Сроки и условия получения заказа</a></li>
			@if (Auth::check())
				<li><a href="{{ route('profile') }}" title="">Личный кабинет</a></li>
			@endif
			{{--<li><a href="{{ route('page.view', 'information') }}" title="">Полезная информация</a></li>--}}
		</ul>
	</div>
</aside>