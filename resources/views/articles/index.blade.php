@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('article.index') !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x articles">
			<div class="cell small-12 margin-bottom-1">
				<h1>Блог о красоте и здоровье</h1>
			</div>
			{!! \App\Http\Controllers\ArticleController::slider() !!}
			<div class="cell small-12 medium-6 margin-bottom-1 text-right text-left-tablet">
				{!! $articles->links('pagination-left') !!}
			</div>
			<div class="cell small-12 medium-6 margin-bottom-1 text-right text-left-tablet">
				Тема
				<article-categories :categories="{{ $categories->toJson() }}"></article-categories>
			</div>
			@each('articles.item', $articles, 'article', 'articles.notfound')
		</div>
	</div>
@endsection