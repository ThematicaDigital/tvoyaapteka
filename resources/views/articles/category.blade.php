@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('article.category', $category) !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x articles">
			<div class="cell small-12 margin-bottom-1">
				<h1>{{ $category->title }}</h1>
			</div>
			<div class="cell small-12 medium-6 margin-bottom-1 text-right text-left-tablet">
				{!! $articles->links('pagination-left') !!}
			</div>
			<div class="cell small-12 medium-6 margin-bottom-1 text-right text-left-tablet">
				Тема
				<article-categories :categories="{{ $categories->toJson() }}"></article-categories>
			</div>
			@each('articles.item', $articles, 'article')
		</div>
	</div>
@endsection