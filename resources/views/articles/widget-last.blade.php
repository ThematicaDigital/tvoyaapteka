@if ($articles)
	<div class="grid-container blocks hide-for-small-only">
		<div class="grid-x grid-margin-x">
			<div class="cell small-8">
				<h2>Блог о красоте и здоровье</h2>
			</div>
			<div class="cell small-4 flex-container align-middle">
				<div class="text-right width-100">
					<a class="all blue" href="{{ route('article.index') }}" title="Все статьи">Все статьи</a>
				</div>
			</div>
			<div class="cell small-12 margin-bottom-1">
				<hr class="margin-0">
			</div>
		</div>
		<div class="grid-x grid-margin-x margin-bottom-1 align-stretch mainpage-articles">
			@foreach($articles as $article)
				<div class="cell small-12 large-6 medium-6 xlarge-3 flex-container">
					<div class="width-100 callout shadow-hover padding-0">
						<a href="{{ route('article.view', [$article->category->slug, $article->slug]) }}" title="{{ $article->title }}" class="drug-image-container">
							<img src="{{ route('image', [240, 183, $article->previewPic]) }}" title="{{ $article->title }}" alt="{{ $article->title }}" class="width-100">
						</a>
						<div class="padding-1">
							<h3>
								<a href="{{ route('article.view', [$article->category->slug, $article->slug]) }}" title="{{ $article->title }}">
									{{ $article->title }}
								</a>
							</h3>
							<div class="anonce">
								{{ $article->announce }}
							</div>
						</div>
						<hr class="margin-0">
						<div class="padding-1">
							<a class="more text-uppercase flex-container align-justify align-middle width-100" href="{{ route('article.view', [$article->category->slug, $article->slug]) }}" title="{{ $article->title }}">
								<span>Читать дальше</span>
								<i class="icon-external-link" style="font-size:1.5rem"></i>
							</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endif