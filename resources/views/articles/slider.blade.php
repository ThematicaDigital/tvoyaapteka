@if ($slides->count())
	<div class="cell small-12 margin-bottom-1">
		<div class="grid-container">
	        <div class="grid-x grid-margin-x margin-bottom-2">
				<div class="cell small-12">
					<article-slider class="hide-for-small-only" :slides="{{ $slides }}" :query="'{{ $query }}'"></article-slider>
					<article-slider-mobile class="show-for-small-only" :slides="{{ $slides }}" :query="'{{ $query }}'"></article-slider>
				</div>
			</div>
		</div>
	</div>
@endif