@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1 margin-top-1">
				{!! Breadcrumbs::render('contacts') !!}
			</div>
		</div>
		<contacs-body :regions="{{ $regions->toJson() }}" :drugstores="{{ $drugstores->toJson() }}"></contacs-body>
	</div>
@endsection