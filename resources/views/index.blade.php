@extends('layouts.main')
@section('content')

	{!! \App\Http\Controllers\SlideController::slider('top') !!}
	{{--<div class="grid-container">--}}
		{{--<div class="grid-x grid-margin-x">--}}
			{{--<div class="cell small-12">--}}
				{{--<h1 class="header__main-page">Семейная аптека</h1>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</div>--}}
	@include('drugs.actuals')
	@include('drugs.hits')
	@include('drugs.discounts')
	{!! \App\Http\Controllers\SlideController::slider('bottom') !!}
	@include('articles.widget-last')

	<!-- <div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-bottom-1">
				{!! \App\Http\Controllers\PageController::preview('about') !!}
				<a class="button" href="{{ route('page.view', ['about']) }}" title="Читать дальше">Читать дальше</a>
			</div>
		</div>
	</div> -->
	@if(!empty($city->seo_text))
		<div class="grid-container">
			{!! $city->seo_text !!}
		</div>
	@endif
@endsection