@push('scripts_after')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endpush
<div class="feedback-widget padding-vertical-2 hide-for-large-only hide-for-small-only">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-5">
                <h4 class="feedback-widget__title"></h4>
                <div class="grid-x grid-margin-x">
                    <div class="cell small-12 margin-bottom-1">
                        <h4 class="feedback-widget__title">Отзывы и предложения</h4>
                    </div>
                    <div class="cell small-2">
                        <i class="icon icon-speech-bubble feedback-widget__icon"></i>
                    </div>
                    <div class="cell small-10">
                        <p class="feedback-widget__description">
                            Оставленные Вами отзывы и предложения помогают нам развивать и совершенствовать сервис обслуживания сайта и аптек-партнеров.
                        </p>
                        <p class="feedback-widget__description">
                            Благодарим за участие!
                        </p>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-7">
                <form action="{{ route('feedback.send') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="grid-x grid-margin-x">
                        <div class="cell small-12 medium-6 ">
                            <label class="icon-label icon-user-outline">
                                <input type="text" placeholder="Введите Ваше имя" class="feedback-widget__name"
                                       name="name">
                            </label>
                        </div>
                        <div class="cell small-12 medium-6">
                            <label class="icon-label icon-flip-x icon-telephone-call-receiver">
                                <input type="text" placeholder="Контакт для связи" class="feedback-widget__phone"
                                       name="phone">
                            </label>
                        </div>
                        <div class="cell small-12">
                            <textarea class="feedback-widget__textarea" placeholder="Введите Ваш отзыв" name="text"></textarea>
                        </div>
                        <div class="cell small-12 medium-3">
                            <label for="feedback-widget-file" class="feedback-widget__file-label">
                                <i class="icon icon-cloud-storage-download"></i>
                                <span>Приложить файл</span>
                            </label>
                            <input type="file" id="feedback-widget-file" name="file" class="feedback-widget__file">
                        </div>
                        <div class="cell small-12 medium-6">
                            <div class="g-recaptcha" data-sitekey="{{config('recaptcha.public_key')}}"></div>
                        </div>
                        <div class="cell small-12 medium-3">
                            <button type="submit" class="alert button expanded feedback-widget__submit">Отправить отзыв</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>