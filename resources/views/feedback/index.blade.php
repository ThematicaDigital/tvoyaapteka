@extends('layouts.main')

@section('content')
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 margin-bottom-1 margin-top-1">
                {!! Breadcrumbs::render('feedback.index') !!}
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-3 margin-vertical-1">
                @include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-9 margin-vertical-1 feedback-widget feedback-widget-page">
                <h1 class="catalog-category__title">Отзывы и предложения</h1>
                <form action="{{ route('feedback.send') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="grid-x grid-margin-x">
                        <div class="cell small-12 medium-6 ">
                            <label class="icon-label icon-user-outline">
                                <input type="text" placeholder="Введите Ваше имя" class="feedback-widget__name"
                                       name="name">
                            </label>
                        </div>
                        <div class="cell small-12 medium-6">
                            <select name="drugstore_id">
                                <option>Все аптеки</option>
                                @foreach($drugstores as $drugstore)
                                    <option value="{{ $drugstore->id }}">{{ $drugstore->title }}, {{ $drugstore->address }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="cell small-12 medium-6">
                            <label class="icon-label icon-flip-x icon-telephone-call-receiver">
                                <masked-input
                                    type="tel"
                                    name="phone"
                                    id="phone"
                                    placeholder="Телефон"
                                    class="feedback-widget__phone"
                                    :mask="['+','7',' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]">
                                </masked-input>
                            </label>
                        </div>
                        <div class="cell small-12 medium-6">
                            <label class="icon-label icon-flip-x icon-email">
                                <input type="email" placeholder="E-mail" class="feedback-widget__email"
                                       name="email">
                            </label>
                        </div>
                        <div class="cell small-12">
                            <textarea class="feedback-widget__textarea" placeholder="Введите Ваш отзыв" name="text"></textarea>
                        </div>
                        <div class="cell small-12">
                            <label>
                                <input type="checkbox" id="need_feedback" name="need_feedback" value="1"> Я хочу, чтобы со мной связались по этому вопросу
                            </label>
                        </div>
                        <div class="cell small-12 medium-3">
                            <label for="feedback-widget-file" class="feedback-widget__file-label">
                                <i class="icon icon-cloud-storage-download"></i>
                                <span>Приложить файл</span>
                            </label>
                            <input type="file" id="feedback-widget-file" name="file" class="feedback-widget__file">
                        </div>
                        <div class="cell small-12 medium-6">
                            <div class="g-recaptcha" data-sitekey="{{config('recaptcha.public_key')}}"></div>
                        </div>
                        <div class="cell small-12 medium-3">
                            <button type="submit" class="alert button expanded feedback-widget__submit">Отправить отзыв</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection