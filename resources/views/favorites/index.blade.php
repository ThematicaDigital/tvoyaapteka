@extends('layouts.main')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 margin-top-1">
				{!! Breadcrumbs::render('favorites') !!}
			</div>
		</div>
		<div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-3 margin-bottom-1">
				@include('articles.sidebar')
            </div>
            <div class="cell small-12 medium-9 margin-bottom-1 articles">
            	<h1>Избранные товары</h1>
            	<favorites-items></favorites-items>
            </div>
        </div>
	</div>
@endsection