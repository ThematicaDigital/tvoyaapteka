<div class="width-100 callout shadow-hover padding-0">
	<img src="{{ route('image', [260, 150, $drug->image]) }}" title="{{ $drug->title }}" alt="{{ $drug->title }}" class="width-100">
	<div class="padding-1">
		<h3>
			<a href="{{ route('catalog.view', [$drug->category->slug, $drug->slug]) }}" title="{{ $drug->title }}">
				{{ $drug->title }}
			</a>
		</h3>
		<div class="properties">
			@if ($drug->manufacturer)
				<p class="margin-0">Пр-во: {{ $drug->manufacturer->title }}</p>
			@endif
			<p class="margin-0">В наличие</p>
		</div>
	</div>
	<hr class="margin-0">
	<div class="padding-1 padding-bottom-0">
		<div class="price">
			{{ $drug->default_price }} руб.
		</div>
		<a class="button text-uppercase expanded" href="{{ route('catalog.view', [$drug->category->slug, $drug->slug]) }}" title="В корзину">
			В корзину
		</a>
	</div>
</div>