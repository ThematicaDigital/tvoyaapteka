<footer class="padding-top-3 padding-bottom-3">
	<div class="grid-container">
		<div class="grid-x margin-bottom-2">
			<div class="cell small-12 medium-6 large-4 xlarge-3 xlarge-offset-2 margin-bottom-1 text-center">
				<p class="footer-title">Скачивайте мобильное приложение</p>
				<div class="cell grid-x small-12 medium-3 align-center app-images">
					<a href="https://itunes.apple.com/us/app/%D1%81%D0%B5%D0%BC%D0%B5%D0%B9%D0%BD%D0%B0%D1%8F%D0%B0%D0%BF%D1%82%D0%B5%D0%BA%D0%B0-%D1%80%D1%84/id1450786570#?platform=iphone" class="cell small-6 text-right"><img src="/images/appstore.png" class="margin-right-1"></a>
					<a href="https://play.google.com/store/apps/details?id=ru.semejnayaapteka.app" class="cell small-6 text-left"><img src="/images/googleplay.png" class="margin-right-1"></a>
				</div>
			</div>

			<div class="cell small-12 medium-6 large-4 xlarge-3 margin-bottom-1 padding-horizontal">
				<div class="social">
					<p class="footer-title">СемейнаяАптека.рф в социальных сетях</p>
					@if(!empty(\Config::get('settings.vk_link')))
						<a href="{{\Config::get('settings.vk_link')}}" title="ВКонтакте" class="circle-link circle-link__filled-blue" target="_blank">
							<i class="fab fa-vk"></i>
						</a>
					@endif
					@if(!empty(\Config::get('settings.ok_link')))
						<a href="{{\Config::get('settings.ok_link')}}" title="Одноклассники" class="circle-link circle-link__filled-blue"  target="_blank">
							<i class="fab fa-odnoklassniki"></i>
						</a>
					@endif
					@if(!empty(\Config::get('settings.facebook_link')))
						<a href="{{\Config::get('settings.facebook_link')}}" title="Facebook" class="circle-link circle-link__filled-blue"  target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
					@endif
					@if(!empty(\Config::get('settings.instagram_link')))
						<a href="{{\Config::get('settings.instagram_link')}}" title="Instagram" class="circle-link circle-link__filled-blue"  target="_blank">
							<i class="fab fa-instagram"></i>
						</a>
					@endif
				</div>
			</div>
			<div class="cell small-12 medium-12 large-4 xlarge-4 margin-bottom-1 inquire">
				<p class="footer-title">Аптечная справочная служба</p>
				<a class="free-phone" href="tel:88002012010" title="8-800-201-20-10">8-800-201-20-10</a>
				{{--<p class="free-phone-text">бесплатный звонок по России</p>--}}
			</div>
		</div>
	</div>
	<div class="grid-container copyright">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 medium-12 xlarge-7 margin-bottom-1">
				<small>Семейная аптека. {{now()->year}} &copy;</small>
			</div>
			<div class="cell small-12 medium-5 tablet-only-hide margin-bottom-1">
				<small>
					{{ $firstDrugstore->title }} {{ $firstDrugstore->address }}
					<a title="Адреса аптек" href="{{ route('contacts') }}" class=>
						Посмотреть все адреса {{ $city->title_seo_form ?? $city->title }}</a>

				</small>
			</div>
		</div>
	</div>
</footer>