<catalog-menu :categories="{{ $categories->toJson() }}"></catalog-menu>
<!-- <div class="catalog-menu width-100">
	<a href="{{ route('catalog.index') }}" title="Каталог товаров"
	   class="hollow button expanded text-uppercase menu-button">
        <i class="icon-menu"></i> Каталог товаров
    </a>
    <ul class="catalog-menu-categories">
    	@foreach ($categories as $category)
	        <li>
	            <a href="{{ route('catalog.category', [$category->slug]) }}" title="{{ $category->title }}" class="clearfix">
	            	@if($category->icon)
	            		<div class="float-left">
		            		<img src="{{ asset($category->icon) }}" alt="{{ $category->title }}" title="{{ $category->title }}" width="30">
		            	</div>
	            	@endif
	            	<div class="float-left margin-left-1" style="width:70%">
	                	{{ $category->title }}
	                </div>
	            </a>
	            <div class="catalog-menu-subcategories">
	                <div class="grid-x grid-margin-x">
	                	@foreach ($category->children as $subcategory)
		                    <div class="cell small-12 medium-4 padding-1">
		                        <a href="{{ route('catalog.category', [$subcategory->slug]) }}" title="{{ $subcategory->title }}">
		                            {{ $subcategory->title }}
		                        </a>
		                    </div>
	                    @endforeach
	                </div>
	            </div>
	        </li>
        @endforeach
    </ul>
</div> -->