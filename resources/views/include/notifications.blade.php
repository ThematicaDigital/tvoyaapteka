<notifications group="flash-notifications"></notifications>
@if (session()->has('flash_notification.message'))
    <flash-notifications
            message="{{ session('flash_notification.message') }}"
            level="{{ session('flash_notification.level') }}"
    >
    </flash-notifications>
@endif
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <flash-notifications message="{{ $error }}" level="error"></flash-notifications>
    @endforeach
@endif