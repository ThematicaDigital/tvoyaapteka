<header>
	<div class="grid-container header-menu hide-for-large-only hide-for-small-only">
		<div class="clearfix">
			<div class="float-left">
		        <nav>
	                <ul class="menu">
	                    <li class="flex-container align-middle {{ Request::is('page/about') ? 'active' : '' }}">
	                    	<a href="{{ route('page.view', ['about']) }}">О компании</a>
	                    </li>
	                    <li class="flex-container align-middle {{ Request::is('page/how-to-apply') ? 'active' : '' }}">
	                    	<a href="{{ route('page.view', ['how-to-apply']) }}">Как оформить</a>
	                    </li>
	                    <li class="flex-container align-middle {{ Request::is('contacts') ? 'active' : '' }}">
	                    	<a href="{{ route('contacts') }}">Адреса аптек</a>
	                    </li>
	                    <li class="flex-container align-middle {{ Request::is('articles') ? 'active' : '' }}">
	                    	<a href="{{ route('article.index') }}">Блог о красоте и здоровье</a>
	                    </li>
	                    <li class="flex-container align-middle {{ Request::is('promotions') ? 'active' : '' }}">
	                    	<a href="{{ route('promotions.index') }}">Акции</a>
	                    </li>
	                    <li class="flex-container align-middle {{ Request::is('vacancies') ? 'active' : '' }}">
	                    	<a href="{{ route('vacancies.index') }}">Вакансии</a>
						</li>
						<li class="flex-container align-middle {{ Request::is('feedback') ? 'active' : '' }}">
							<a href="{{ route('feedback.index') }}"></i>Оставить отзыв</a>
	                    </li>
	                    <!-- <li class="flex-container align-middle" style="visibility: hidden">
	                    	<a href="{{ route('main') }}" class="red">
	                    		Бонусная карта <i class="icon-credit-card red" style="font-size:1rem"></i>
	                    	</a>
	                    </li> -->
	                </ul>
		        </nav>
			</div>
			<div class="float-right">
                <ul class="menu">
                    <li class="flex-container align-middle">
                    	<a href="{{ route('favorites.index') }}">
                    		Избранное <i class="icon-heart-outline red" style="font-size:1rem"></i>
                    	</a>
                    </li>
					@auth
						@if (Auth::user()->is_admin)
							<li class="flex-container align-middle">
								<a href="{{ url('/admin') }}">
									Админ
								</a>
							</li>
						@endif
						<li class="flex-container align-middle">
							<a href="{{ route('profile') }}">
								Личный кабинет
							</a>
						</li>
						<li class="flex-container align-middle">
							<a href="{{ route('logout.get') }}">
								Выйти
							</a>
						</li>
					@endauth
					@guest
						<li class="flex-container align-middle">
							<signin-open-button></signin-open-button>
						</li>
					@endguest
                </ul>
			</div>
		</div>
	</div>
	<div class="background padding-vertical-1" id="sticky-header">
		<div class="small-header-wrap">
			<div class="grid-container">
				<div class="grid-x grid-margin-x scroll-flex-center">
					<div class="cell small-12 medium-4 xlarge-3">
						<div class="header-flex margin-bottom-1 margin-mobile">
							<a href="{{ route('main') }}" title="Семейная аптека" class="logo_wrap">
								<img src="{{ asset('images/logotype.svg') }}" title="Семейная аптека" alt="Семейная аптека">
							</a>
						</div>
						<div class="hide-for-large-only hide-for-small-only scroll-hide">
							@include('include.header-catalog')
						</div>
					</div>
					<div class="cell small-12 medium-8 xlarge-7">
						<div class="grid-x grid-margin-x header-flex margin-bottom-1 scroll-hide margin-mobile">
							<div class="chcity cell small-6 medium-6 xlarge-4">
								<choose-city
								:user-city="{{ !empty($userCity) ? 'true' : 'false' }}"
								:city="{{ $city->toJson() }}"
								:regions="{{ $regions->toJson() }}"></choose-city>
							</div>
							<div class="chstore cell small-6 medium-6 xlarge-4">
								<choose-drugstore :selected="{{ $drugstore }}"></choose-drugstore>
							</div>
						</div>
						<div class="search-drugs-wrap">
							<form method="GET" action="{{ route('drugs.search') }}">
								<div class="mobile-search">
									<div class="search-drugs">
										<autocomplete
        	    			                class="search-drugs__suggestions"
        	    			                :url="'{{ route('drugs.autocomplete') }}'"
        	    			                :placeholder="'Найдите нужный товар по названию'"
        	    			                :value="'{{ Request::get('q') }}'"
        	    			                :choosed-drugstore="{{ $drugstore }}">
										</autocomplete>
										<button class="button expanded text-uppercase" style="line-height:26px">
											Найти<!--<i class="icon-arrow-right" style="font-size:1.1rem"></i> -->
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="cell small-12 medium-2 mobile-heading-new">
						<div class="mobcell-3 show-for-large-only show-for-small-only">
						<mobheader :authorised="'{{ Auth::check() }}'"
                     		:vklink="'{{ \Config::get('settings.vk_link') }}'"
                     		:oklink="'{{ \Config::get('settings.ok_link') }}'"
                     		:fblink="'{{ \Config::get('settings.facebook_link') }}'"
							:inlink="'{{ \Config::get('settings.instagram_link') }}'">
						</mobheader>
						</div>
						<div class="mobcell-9 header-flex header-flex-right">
							<a href="/favorites" class="show-for-large-only show-for-small-only circle-link circle-link__outline-blue">
								<i class="fas fa-heart"></i>
							</a>
							<a href="/cart" class="show-for-large-only show-for-small-only circle-link circle-link__outline-blue">
								<i class="fas fa-shopping-cart"></i>
							</a>
							@auth
								<a class="show-for-large-only show-for-small-only circle-link circle-link__outline-blue" href="/profile">
									<i class="fas fa-user"></i>
								</a>
							@endauth
							@guest
								<div class="show-for-large-only show-for-small-only">
									<signin-open-button></signin-open-button>
								</div>
							@endguest
							<div class="flex-container align-middle hide-for-large-only hide-for-small-only">
								<cart-header ref="cartHeader"></cart-header>
							</div>
						</div>
						<a href="{{ route('promotions.index') }}" title="Акции" class="hide-for-small-only hide-for-large-only button scroll-hide alert margin-top-1 expanded text-uppercase shares" style="line-height:26px">
							<i class="icon-megaphone" style="font-size:1.4rem"></i> Акции
						</a>
					</div>
					<!-- <div class="header-menu-mobile show-for-small-only">
						<mobheader :authorised="'{{ Auth::check() }}'"></mobheader>
						<a href="/catalog" class="header-menu-mobile__item">
							<i class="icon-pharmacy-icon"></i> Каталог
						</a>
						<a href="/cart" class="header-menu-mobile__item">
							<i class="fi-shopping-cart"></i> Корзина
						</a>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<signin ref="authModal"></signin>
</header>
@push('scripts_after')
	<script>
        const header = document.getElementById('sticky-header');
        const headerTop = header.offsetTop;

        function stickyHeader() {
            if (window.scrollY >= headerTop) {
                document.body.classList.add('fixed-header');
            } else {
                document.body.classList.remove('fixed-header');
            }
        }
        window.addEventListener('scroll', stickyHeader);
	</script>
@endpush