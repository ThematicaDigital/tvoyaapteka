<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_user_have_a_phone()
    {
        $user = factory(User::class)->create([
                'phone' => '8-909-884-8294',
                'group_id' => 1
            ]);

        $this->assertEquals('9098848294', $user->phone);

        $user->phone = '(909)-884-82-94';
        $user->save();

        $this->assertEquals('9098848294', $user->phone);
    }
}
