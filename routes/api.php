<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/
Route::post('auth/login', 'AuthController@login');
Route::middleware('api.refresh:api')->post('auth/refresh-token', 'AuthController@refreshToken');

Route::middleware('api.auth:api')->prefix('auth')->group(function () {
    Route::post('logout', 'AuthController@logout');
    Route::get('me', 'AuthController@me');
});
Route::middleware('api.auth:api')->prefix('user')->group(function () {
    Route::get('', 'UserController@show');
    Route::patch('', 'UserController@update');
    Route::get('favourites', 'UserController@getFavourites');
    Route::post('favourites/{drug}', 'UserController@putFavourites');
    Route::delete('favourites/{drug}', 'UserController@detachFavourites');
    Route::get('bonus-card', 'BonusCardController@show');
    Route::post('bonus-card/{cardNumber}', 'BonusCardController@create');
});
Route::middleware('verify.sms:api')->group(function () {
    Route::post('user', 'UserController@store');
    Route::post('user/restore-password', 'UserController@changePassword');
});
Route::prefix('user')->group(function () {
    Route::post('validate', 'UserController@validateBeforeRegister');
});

Route::prefix('articles')->group(function () {
    Route::get('', 'ArticleController@index');
    Route::get('{article}', 'ArticleController@show');
});
Route::prefix('promotions')->group(function () {
    Route::get('', 'PromotionController@index')->name('api.promotion.index');
    Route::get('{promotion}', 'PromotionController@show')->name('api.promotion.show');
});
Route::prefix('catalog')->group(function () {
    Route::get('', 'CatalogController@index');
    Route::get('search', 'CatalogController@search');
    Route::get('{categoryId}', 'CatalogController@category');
});
Route::prefix('actuals')->group(function () {
    Route::get('', 'ActualController@index');
    Route::get('{actualId}', 'ActualController@actual');
});
Route::prefix('drugs')->group(function () {
    Route::get('{drugId}', 'CatalogController@getDrug')->name('api.drug.show');
});
Route::prefix('drugstores')->group(function () {
    Route::get('', 'DrugstoreController@index');
    Route::get('{drugstore}', 'DrugstoreController@show');
    Route::get('{drugstore}/drugs', 'DrugstoreController@drugs');
});
Route::get('regions', 'RegionController@index');
Route::get('filters', 'FilterController@index');
Route::post('feedback', 'FeedbackController@store');
Route::post('orders/unauthorized-user', 'OrderController@makeOrderByUnauthorizedUser');
Route::post('orders/{orderId}/unauthorized-cancel', 'OrderController@cancelOrderByUnauthorizedUser');

Route::middleware('api.auth:api')->prefix('orders')->group(function () {
    Route::get('', 'OrderController@index');
    Route::post('', 'OrderController@add');
    Route::get('{orderId}', 'OrderController@show');
    Route::post('{orderId}', 'OrderController@cancel');
});
Route::middleware('api.auth:api')->prefix('cart')->group(function () {
    Route::get('', 'CartController@index')->name('api.cart.index');
    Route::post('', 'CartController@add')->name('api.cart.add');
    Route::delete('', 'CartController@delete')->name('api.cart.delete');
});

Route::post('device-token', 'DeviceTokenController@store')->middleware('api.auth:api');