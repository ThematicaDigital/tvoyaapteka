<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', 'admin.auth'],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    Route::get('dashboard', 'DashboardController@index')->name('backpack.dashboard');
    Route::post('statistic', 'DashboardController@statistic')->name('backpack.dashboard');
    Route::get('/', '\Backpack\Base\app\Http\Controllers\AdminController@redirect')->name('backpack');
    CRUD::resource('drugs', 'DrugCrudController');
    CRUD::resource('pages', 'PageCrudController');
    CRUD::resource('articles', 'ArticleCrudController');
    CRUD::resource('article_categories', 'ArticleCategoryCrudController');
    CRUD::resource('article_slides', 'ArticleSlidesCrudController');
    CRUD::resource('slides', 'SlideCrudController');
    CRUD::resource('manufacturers', 'ManufacturerCrudController');
    CRUD::resource('orders', 'OrderCrudController');
    CRUD::resource('actuals', 'ActualCrudController');
    CRUD::resource('recommendation-categories', 'ForUseCrudController');
    CRUD::resource('drug-requests', 'DrugRequestCrudController');
    CRUD::resource('dosage-forms', 'DosageFormCrudController');
    CRUD::resource('partners', 'PartnerCrudController');
    CRUD::resource('regions', 'RegionCrudController');
    CRUD::resource('cities', 'CityCrudController');
    CRUD::resource('vacancies', 'VacancyCrudController');
    CRUD::resource('resumes', 'ResumeCrudController');
    CRUD::resource('feedbacks', 'FeedbackCrudController');
    CRUD::resource('categories', 'CategoryCrudController');
    CRUD::resource('drugstores', 'DrugstoreCrudController');
    CRUD::resource('orders', 'OrderCrudController');
    CRUD::resource('promotions', 'PromotionCrudController');
    CRUD::resource('freedays', 'FreedayCrudController');
    CRUD::resource('freeday-statuses', 'FreedayStatusCrudController');
    CRUD::resource('discount-cards', 'BonusCardCrudController');
    CRUD::resource('promocodes', 'PromocodeCrudController');
    CRUD::resource('users', 'UserCrudController');
    CRUD::resource('search-queries', 'SearchQueryCrudController');
    Route::get('vacancies/{id}/delete', 'VacancyCrudController@forceDelete')->name('crud.vacancies.force_delete');
}); // this should be the absolute last line of this file
