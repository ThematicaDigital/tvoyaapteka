<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Image resizer
 */
Route::get('/image/{h}/{w}/{img?}',function($w=200, $h=200, $img) {
	$image = Image::cache(function($image) use($img, $w, $h) {
	    $image->make(public_path("/$img"))->heighten($h);
	});
	return response($image, 200)->header('Content-Type', 'image/jpg');
})->name('image')->where('img', '(.*)');


/**
 * 1C exchange
 */
Route::middleware('auth.1c')->prefix('exchange')->group(function () {
    Route::any('', 'Exchange\ExchangeController@index');
    Route::post('promocodes', 'Exchange\PromocodeController@generate');
    Route::post('bonus-cards/create', 'Exchange\BonusCardController@create');
    Route::get('bonus-cards/{search}', 'Exchange\BonusCardController@show');
//    Route::post('bonus-cards/{search}', 'Exchange\BonusCardController@storeBonusLog');
    Route::get('orders/{id}', 'Exchange\OrderController@get');
    Route::post('orders/{order}/{status}', 'Exchange\OrderController@changeStatus');
});


/**
 * Auth
 */
Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');

Route::post('/login', 'Auth\LoginController@login');

Route::post('/register', 'Auth\RegisterController@create');

Route::post('/sms/send-code', 'SmsCodeController@sendCodeForExistingUser');

Route::post('/register/send-code', 'SmsCodeController@sendCodeForRegister');

Route::post('/forget/send-code', 'SmsCodeController@sendCodeForFoggotPassword');

Route::post('/forget/change', 'Auth\ResetPasswordController@forgetpassword');

Route::middleware('geoip')->group(function() {
    Route::get('sitemap.xml', 'SeoController@sitemap')->name('seo.sitemap');
    Route::get('robots.txt', 'SeoController@robots')->name('seo.robots');
	/**
	 * Catalog
	 */
	Route::group(['prefix' => 'catalog'], function () {
		Route::get('', 'CatalogController@index')->name('catalog.index');
		Route::get('{category}', 'CatalogController@category')->name('catalog.category');
		Route::get('{category}/{slug}', 'CatalogController@view')->name('catalog.view');
		Route::post('search', 'CatalogController@search')->name('catalog.search');
	});

	/**
	 * Contacts
	 */
	Route::group(['prefix' => 'contacts'], function () {
		Route::get('', 'DrugstoreController@contacts')->name('contacts');
		Route::post('/select', 'DrugstoreController@select')->name('contacts.city');
	});

	/**
	 * Main page
	 */
	Route::get('/', 'MainController@main')->name('main');
	Route::post('/choose-city', 'MainController@chooseCity')->name('choose.city');
	Route::post('/choose-drugstore', 'MainController@chooseDrugstore')->name('choose.drugstore');
	Route::get('/drugstores', 'MainController@drugstores')->name('drugstores');

	/**
	 * Pages
	 */
	Route::group(['prefix' => '/page'], function () {
	    Route::get('{slug}', 'PageController@view')->name('page.view');
	});

	/**
	 * Vacancies
	 */
	Route::group(['prefix' => '/vacancies'], function () {
		Route::get('', 'VacancyController@index')->name('vacancies.index');
	});
	/**
	 * Resume
	 */
	Route::resource('resume', 'ResumeController', ['only' => [
	    'store'
	]]);

	/**
	 * Feedback
	 */
	Route::group(['prefix' => 'feedback'], function () {
		Route::get('', 'FeedbackController@index')->name('feedback.index');
		Route::post('send', 'FeedbackController@send')->name('feedback.send');
	});
	/**
	 * drug request
	 */
    Route::post('drug-request', 'DrugRequestController@post')->name('drug-request.post');

	/**
	 * Promotions
	 */
	Route::group(['prefix' => '/promotions'], function () {
		Route::get('', 'CatalogController@promotions')->name('promotions.index');
//		Route::get('search', 'PromotionController@search')->name('promotions.search');
	});

	/**
	 * Drugs
	 */
	Route::group(['prefix' => '/drugs'], function () {
		Route::post('autocomplete', 'DrugController@autocomplete')->name('drugs.autocomplete');
		Route::get('search', 'CatalogController@searchDrugs')->name('drugs.search');
	    Route::get('for-select', 'DrugController@forSelect')->name('crud.drugs.for_select');
	    Route::get('recommended/{drug}', 'DrugController@getRecommendedDrugs')->name('drugs.recommended');
	});

	Route::middleware(['web', 'admin.auth'])->get('users/for-select', 'UserController@forSelect')->name('crud.users.for_select');

	/**
	 * Favorites
	 */
	Route::group(['prefix' => '/favorites'], function () {
		Route::get('', 'FavoriteController@index')->name('favorites.index');
		Route::post('add', 'FavoriteController@add')->name('favorites.add');
		Route::post('delete', 'FavoriteController@delete')->name('favorites.delete');
		Route::post('search', 'FavoriteController@search')->name('favorites.search');
	});

	/**
	 * Cart
	 */
	Route::group(['prefix' => '/cart'], function () {
		Route::get('', 'CartController@index')->name('cart');
		Route::delete('/clear', 'CartController@clear')->name('cart.clear');
		Route::delete('{drug_id}', 'CartController@delete')->name('cart.delete');
		Route::post('add', 'CartController@add')->name('cart.add');
		Route::get('get', 'CartController@get')->name('cart.get');
	});

	/**
	 * Order
	 */
	Route::group(['prefix' => '/orders'], function () {
	    Route::post('', 'Api\V1\OrderController@add')->name('order.add');
	    // Route::get('', 'OrderController@store')->name('order.add');
	});

	/**
	 * Articles
	 */
	Route::group(['prefix' => '/articles'], function () {
		Route::get('/', 'ArticleController@index')->name('article.index');
		Route::get('{category}', 'ArticleController@category')->name('article.category');
	    Route::get('{category}/{slug}', 'ArticleController@view')->name('article.view');
	});

	/**
	 * Profile
	 */
	Route::group(['prefix' => '/profile'], function () {
		Route::get('', 'ProfileController@index')->name('profile');
		Route::get('orders', 'ProfileController@orders')->name('profile.orders');
	    Route::get('getOrders', 'ProfileController@getOrders')->name('profile.orders.get');
	    Route::post('orders/{orderId}', 'Api\V1\OrderController@cancel')->name('profile.orders.cancel');
		Route::get('favorites', 'ProfileController@favorites')->name('profile.favorites');
		//Бонусная карта
		Route::get('bonus', 'ProfileController@bonus')->name('profile.bonus');
		Route::post('createcard', 'ProfileController@createcard')->name('profile.bonuscreate');
		//Смена пароля
		Route::get('password', 'ProfileController@password')->name('profile.password');
		Route::post('changepswd', 'ProfileController@changepswd')->name('profile.changepswd');
		//Изменение профиля
		Route::post('edit', 'ProfileController@edit')->name('profile.edit');
		//Изменение профиля
		Route::post('change-notification', 'ProfileController@changeNotification')->name('profile.change-notification');
	});
    Route::fallback(function () {
        return view('errors.404');
    });
});