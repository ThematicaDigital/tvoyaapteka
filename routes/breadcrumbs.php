<?php
/**
 * Home
 */
Breadcrumbs::register('main', function ($breadcrumbs) {
	$breadcrumbs->push('Главная', route('main'));
});

/**
 * Catalog
 */
Breadcrumbs::register('catalog.index', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Каталог', route('catalog.index'));
});
Breadcrumbs::register('catalog.category', function ($breadcrumbs, $category) {
	$breadcrumbs->parent('catalog.index');
	if ($category->ancestors->count()) {
		foreach ($category->ancestors as $ancestor) {
			$breadcrumbs->push($ancestor->title, route('catalog.category', [$ancestor->slug]));
		}
	}
	$breadcrumbs->push($category->title, route('catalog.category', [$category->slug]));
});
Breadcrumbs::register('catalog.view', function ($breadcrumbs, $drug) {
	$breadcrumbs->parent('catalog.index');
	foreach (App\Models\Category::ancestorsAndSelf($drug->category_id) as $ancestor) {
		$breadcrumbs->push($ancestor->title, route('catalog.category', [$ancestor->slug]));
	}
	$breadcrumbs->push($drug->title, route('catalog.view', [$drug->category->slug, $drug->slug]));
});
Breadcrumbs::register('catalog.search', function ($breadcrumbs, $q) {
	$breadcrumbs->parent('catalog.index');
	$breadcrumbs->push("Результаты поиска: {$q}", route('catalog.search'));
});

/**
 * Pages
 */
Breadcrumbs::register('page.view', function ($breadcrumbs, $page) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push($page->title, route('page.view', $page->slug));
});

/**
 * Feedback
 */
Breadcrumbs::register('feedback.index', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Оставить отзыв', route('feedback.index'));
});

/**
 * Promotions
 */
Breadcrumbs::register('promotions.index', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Акции', route('promotions.index'));
});

/**
 * vacancies
 */
Breadcrumbs::register('vacancies.index', function ($breadcrumbs) {
    $breadcrumbs->parent('main');
    $breadcrumbs->push('Вакансии', route('vacancies.index'));
});
/**
 * Articles
 */
Breadcrumbs::register('article.index', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Блог о красоте и здоровье', route('article.index'));
});
Breadcrumbs::register('article.category', function ($breadcrumbs, $category) {
	$breadcrumbs->parent('article.index');
	$breadcrumbs->push($category->title, route('article.category', [$category->slug]));
});
Breadcrumbs::register('article.view', function ($breadcrumbs, $article) {
	$breadcrumbs->parent('article.index');
	$breadcrumbs->push($article->category->title, route('article.category', [$article->category->slug]));
	$breadcrumbs->push($article->title, route('article.view', [$article->category->slug, $article->slug]));
});

/**
 * Contacts
 */
Breadcrumbs::register('contacts', function ($breadcrumbs, $slug = null) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Адреса аптек', route('contacts'));
	if ($slug) {
		$city = App\Models\City::where('slug', $slug)->first();
		$breadcrumbs->push($city->title, route('contacts.city', [$city->slug]));
	}
});

/**
 * Profile
 */
Breadcrumbs::register('profile', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Личный кабинет', route('profile'));
});

/**
 * Favorites
 */
Breadcrumbs::register('favorites', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Избранные товары', route('favorites.index'));
});

/**
 * Cart
 */
Breadcrumbs::register('cart', function ($breadcrumbs) {
	$breadcrumbs->parent('main');
	$breadcrumbs->push('Корзина товаров', route('cart'));
});